# import debugpy
# # Allow other computers to attach to debugpy at this IP address and port.
# debugpy.listen(('172.20.201.134', 5678))
# # Pause the program until a remote debugger is attached
# debugpy.wait_for_client()


import pandas as pd
import logging, pickle, os
import numpy as np
import sent2vec
import datetime,dgl,torch,time
from dgl.data.utils import save_graphs
import getTextToken

from concurrent import futures

def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

logging.basicConfig(level=logging.INFO)
# ------------------------utils-----------------------------------------------------------
# ----------------------------------------------------------------------------------------

def gen_graph(data_field, data, edge_indices, depos_dir):
    actor1_index = data_field.index('Actor1Name')
    actor2_index = data_field.index('Actor2Name')
    event_code_index = data_field.index('EventCode')

    # load the entity and evencode dict
    entity_dict = pd.read_csv(os.path.join(depos_dir, "loc_entity2id.txt"), \
        names=['id','entity_name'], sep='\t')
    entity_dict = dict(zip(list(entity_dict['entity_name']),list(entity_dict['id'])))
    code_dict = pd.read_csv("../data/support/cameo.txt",sep='\t',\
        names=['code'],dtype=str,index_col=[1])
    logging.debug( range(len(code_dict['code'])))
    code_dict = dict(zip(list(code_dict['code']), range(len(code_dict['code']))))

    # get index of actor1, eventcode, actor2
    index_data = []
    for i in range(len(data)):
        actor1 = entity_dict[data[i][actor1_index]]
        eventcode = code_dict[data[i][event_code_index]]
        actor2 = entity_dict[data[i][actor2_index]]
        index_data.append([actor1,eventcode,actor2])
    
    # use the index data to generate graph
    index_data = np.array(index_data)
    edge_indices = np.array(edge_indices)
    g = get_big_graph_w_idx(index_data, edge_indices)

    return g


def get_big_graph_w_idx(data, edge_indices):
    # from  GLEAN's 1_get_digraphs.py file
    # data:the data is one date's data, which composed as (subject, relation, object)
    # edge_indices: the every piece of 'data' in the total data's position(index).
    
    # separate the different composition of data
    src, rel, dst = data.transpose()
    # get unique entity, including subject and object, 
    # edges is the src and dst's position in the unique list. 
    # np.unique(***,return_inverse=True) :return the original data's index 
    # in the generated unique list if the return_inverse = True.
    uniq_v, edges = np.unique((src, dst), return_inverse=True)
    # now the 'src' and 'dst' is the index in the unique list.
    src, dst = np.reshape(edges, (2, -1))
    # use dgl for handling graph
    g = dgl.DGLGraph()
    g.add_nodes(len(uniq_v))
    g.add_edges(src, dst, {'eid': torch.from_numpy(edge_indices)}) # array list
    norm = comp_deg_norm(g)
    g.ndata.update({'id': torch.from_numpy(uniq_v).long().view(-1, 1), 'norm': norm.view(-1, 1)})
    g.edata['r'] = torch.LongTensor(rel)
    g.ids = {}
    idx = 0
    for id in uniq_v:
        g.ids[id] = idx
        idx += 1
    return g


def comp_deg_norm(g):
    in_deg = g.in_degrees(range(g.number_of_nodes())).float()
    # make in-degree 0 to 1 
    in_deg[torch.nonzero(in_deg == 0, as_tuple=False).view(-1)] = 1
    norm = 1.0 / in_deg
    return norm


# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------

# generate loc2id.txt
def loc2id(loc_list, depos_path):
    loc_data = pd.DataFrame(loc_list)
    loc_data.to_csv(depos_path,sep='\t',header=False)
    logging.info("loc2id.txt OK!")

def loc_entity2id(data_field, data, depos_path):
    entity = set()
    for loc_data in data:
        for one_data in loc_data:
            actor1name = one_data[data_field.index("Actor1Name")]
            actor2name = one_data[data_field.index("Actor2Name")]
            if len(actor1name)>0:
                entity.add(actor1name)
            if len(actor2name)>0:
                entity.add(actor2name)
    entity = list(entity)
    entity.sort()
    entity = pd.DataFrame(zip(range(len(entity)), entity))
    entity.to_csv(depos_path,sep='\t',index=False,header=False,doublequote=False)
    logging.info("loc_entity2id.txt OK!")

def data_count(data_field, data, depos_path):
    # get the count for every 292 event type for one day
    # we assume the data is permutating in the date order
    # get cameo data
    cameo_data = pd.read_csv("../data/support/cameo.txt",sep='\t',\
        names=['code', 'description'],dtype=str)
    logging.debug( range(len(cameo_data['code'])))
    cameo_dict = dict(zip(list(cameo_data['code']), range(len(cameo_data['code']))))
    
    # stat the every day's event type number
    count_data = []
    for loc_data in data:
        day_data = []
        event_date_index = data_field.index("EventDate")
        event_code_index = data_field.index('EventCode')
        date_flag = loc_data[0][event_date_index]
        i = 0 
        while i < len(loc_data) + 1:
            if i < len(loc_data) and loc_data[i][event_date_index] == date_flag:
                day_data.append(loc_data[i])
                i += 1
            else:
                day_count = np.zeros(len(cameo_data['code']), dtype= float)
                for one_data in day_data:
                    day_count[cameo_dict[one_data[event_code_index]]] += 1
                    # change for NumSources
                    # day_count[cameo_dict[one_data[2]]] += one_data[5]
                    # change for AvgTone
                    # day_count[cameo_dict[one_data[2]]] += one_data[6]

                count_data.append(day_count)
                
                day_data.clear()
                if i != len(loc_data):
                    date_flag = loc_data[i][event_date_index]
                else:
                    i += 1
    
    count_data = np.array(count_data)
    
    # save the data
    with open(depos_path,'wb') as f:
        pickle.dump(count_data, f)
    
    logging.info("data_count.pkl OK!")


def text_emb(data_field, data, depos_dir, model_path):
    # aquire allnames as text token
    allnames_index = data_field.index("AllNames")
    text_token_all = []
    for local_data in data:
        for one_data in local_data:
            text_token_all.append(one_data[allnames_index])
    
    # remove duplicated data
    # text_token, indices = np.unique(text_token,return_inverse=True)
    text_token = list(set(text_token_all))
    text_token = dict(zip(text_token, range(len(text_token))))
    indices = [text_token[one_text] for one_text in text_token_all]
    
    temp_text = list(range(len(text_token)))
    for one_text in text_token:
        temp_text[text_token[one_text]] = one_text
    
    text_token = temp_text
    del(temp_text)

    # remove stop words and get stem words
    text_token = getTextToken.process_texts(text_token)
    for i in range(len(text_token)):
        text_token[i] = ' '.join(text_token[i])

    # get embedding, model is trained in GLEAN process
    model = sent2vec.Sent2vecModel()
    model.load_model(model_path)
    embs = model.embed_sentences(text_token)
    # check
    logging.debug(embs)
    # storage data
    with open(os.path.join(depos_dir, "loc_text_emb.pkl"),'wb') as f:
        pickle.dump(embs, f)
    logging.info("loc_text_emb.pkl OK!")
    
    with open (os.path.join(depos_dir, "emb_idx.pkl"),'wb') as f:
        pickle.dump(indices, f)
    logging.info("emb_idx.pkl OK!")
    


def data_label(data_field, data, dataset_name, loc_list, depos_dir):

    event_date_index = data_field.index("EventDate")
    event_code_index = data_field.index("EventCode")
    is_root_index = data_field.index("IsRootEvent")
    # load text_token embedding index data
    with open (os.path.join(depos_dir, "emb_idx.pkl"),'rb') as f:
        indices = pickle.load(f)
    # traverse the data, handle once for one day
    # initialize data
    time_data = []
    loc_data = []
    label_dup = []
    label= []
    text_id = []
    is_root_14 = []

    day_data = []
    first_date = datetime.datetime.strptime(data[0][0][event_date_index], '%Y-%m-%d')

    # record all other location data before this location data, 
    # because data including more than 1 position's data
    pos_num_flag = 0
    # record one day's text embedding id, order required.
    day_text_id = []
    # i record loc, j record every piece of data
    for i in range(len(data)):
        date_flag = data[i][0][event_date_index]
        j = 0
        if i > 0:
            pos_num_flag += len(data[i-1])
        while j < len(data[i])+1:
            if j < len(data[i]) and data[i][j][event_date_index] == date_flag:
                day_data.append(data[i][j])
                # record oneday's text_id
                day_text_id.append(indices[pos_num_flag+j])
                j += 1
            else:
                # get date
                the_date = datetime.datetime.strptime(date_flag, '%Y-%m-%d')
                the_date = (the_date - first_date).days
                time_data.append(the_date)

                # get loc_data
                loc_data.append(i)

                # get label_dup and IsRootEvent
                day_dup = {}
                root_14_flag = False
                for one_data in day_data:
                    # label
                    root_code = int(one_data[event_code_index][0:2])-1
                    day_dup[root_code] = day_dup.setdefault(root_code, 0) + 1
                    
                    # IsRootEvent
                    if one_data[is_root_index] == 1 and root_code == 13:
                        root_14_flag = True
                

                label_dup.append(day_dup.copy())
                
                # get the label data
                label.append(list(day_dup.keys()))

                # get IsRootEvent
                is_root_14.append(1 if root_14_flag else 0 )
                
                # get the text_id data
                text_id.append(day_text_id.copy())
                print(f"\rloc: {i}, date: {date_flag} data has get",end='')
                # prepare for next day  
                day_data.clear()
                day_text_id.clear()
                if j != len(data[i]):
                    date_flag = data[i][j][event_date_index]
                else:
                    j += 1

    end_date =  first_date + datetime.timedelta(days=max(time_data))
    date_period = "{0}-{1}".format(first_date.strftime("%Y%m"), end_date.strftime("%Y%m"))
    data_label = {'time':time_data, 'loc':loc_data, 'label':label, 'label_dup':label_dup, \
                  'is_root_14':is_root_14, 'text_id':text_id, 'dataset_name':dataset_name, \
                    'loc_list': loc_list, 'date_period': date_period}

    with open(os.path.join(depos_dir, "data_label.pkl"), 'wb') as f:
        pickle.dump(data_label, f)
    logging.info("data_label.pkl OK!")
    

def data_graph(data_field, data, depos_dir):

    event_date_index = data_field.index("EventDate")
    # graph list for storaging graph data
    graph_list = []
    
    begin_index = 0
    for loc_data in data:
        # record for the first index of the same date data
        first_index = 0
        for i in range(len(loc_data)+1):
            if i == len(loc_data) or loc_data[i][event_date_index] != loc_data[first_index][event_date_index]:
                
                # generate a graph for the same date data
                g1 = gen_graph(data_field, loc_data[first_index:i], list(range(begin_index, begin_index +i-first_index)), depos_dir)

                # arrange to the graph list
                graph_list.append(g1)
                
                # set for next date
                begin_index += i - first_index
                first_index = i
        

    # check the graph list data
    logging.debug(graph_list)

    # save the graph data
    save_graphs(os.path.join(depos_dir, 'data_graph.bin'), graph_list)
    logging.info('data_graph.bin OK')
    
def cmfDataGenerator(data_field, dataset_name, loc_list, gdelt_dir, model_path, deposit_dir):

        time0 = time.time()
        print(f'handling {dataset_name}')
        """ 1, get loc2id.txt """
        loc2id(loc_list, os.path.join(deposit_dir, 'loc2id.txt'))
        
        # get the GDELT data
        data = []
        for one_loc in loc_list:
            one_path = os.path.join(gdelt_dir, one_loc+'.pkl')
            with open (one_path,'rb') as f:
                data.append(pickle.load(f))
        
        # # remedy for Getting Gdelt not filtered EventCode = '---' 
        # event_codes= set(['010', '011', '012', '013', '014', '015', '016', '017', '018', '019', '020', '021', '0211', '0212', '0213', '0214', '022', '023', '0231', '0232', '0233', '0234', '024', '0241', '0242', '0243', '0244', '025', '0251', '0252', '0253', '0254', '0255', '0256', '026', '027', '028', '030', '031', '0311', '0312', '0313', '0314', '032', '033', '0331', '0332', '0333', '0334', '034', '0341', '0342', '0343', '0344', '035', '0351', '0352', '0353', '0354', '0355', '0356', '036', '037', '038', '039', '040', '041', '042', '043', '044', '045', '046', '050', '051', '052', '053', '054', '055', '056', '057', '060', '061', '062', '063', '064', '070', '071', '072', '073', '074', '075', '080', '081', '0811', '0812', '0813', '0814', '082', '083', '0831', '0832', '0833', '0834', '084', '0841', '0842', '085', '086', '0861', '0862', '0863', '087', '0871', '0872', '0873', '0874', '090', '091', '092', '093', '094', '100', '101', '1011', '1012', '1013', '1014', '102', '103', '1031', '1032', '1033', '1034', '104', '1041', '1042', '1043', '1044', '105', '1051', '1052', '1053', '1054', '1055', '1056', '106', '107', '108', '110', '111', '112', '1121', '1122', '1123', '1124', '1125', '113', '114', '115', '116', '120', '121', '1211', '1212', '1213', '122', '1221', '1222', '1223', '1224', '123', '1231', '1232', '1233', '1234', '124', '1241', '1242', '1243', '1244', '1245', '1246', '125', '126', '127', '128', '129', '130', '131', '1311', '1312', '1313', '132', '1321', '1322', '1323', '1324', '133', '134', '135', '136', '137', '138', '1381', '1382', '1383', '1384', '1385', '139', '140', '141', '1411', '1412', '1413', '1414', '142', '1421', '1422', '1423', '1424', '143', '1431', '1432', '1433', '1434', '144', '1441', '1442', '1443', '1444', '145', '1451', '1452', '1453', '1454', '150', '151', '152', '153', '154', '160', '161', '162', '1621', '1622', '1623', '163', '164', '165', '166', '1661', '1662', '1663', '170', '171', '1711', '1712', '172', '1721', '1722', '1723', '1724', '173', '174', '175', '180', '181', '182', '1821', '1822', '1823', '183', '1831', '1832', '1833', '184', '185', '186', '190', '191', '192', '193', '194', '195', '196', '200', '201', '202', '203', '204', '2041', '2042'])
        # filter_data = []
        # for loc_data in data:
        #     flter_loc_data = []
        #     for one_data in loc_data:
        #         if one_data[data_field.index("EventCode")] in event_codes:
        #             flter_loc_data.append(one_data)
        #     filter_data.append(flter_loc_data)
        # data = filter_data

        """ 2, get loc_entity2id.txt """
        loc_entity2id(data_field, data, os.path.join(deposit_dir, 'loc_entity2id.txt'))
        
        """ 3, get data_count.pkl """
        data_count(data_field, data, os.path.join(deposit_dir, 'data_count.pkl'))

        """ 4, get loc_text_emb.pkl """
        text_emb(data_field, data, deposit_dir, model_path)
        
        """ 5, get data_label.pkl """
        data_label(data_field, data, dataset_name, loc_list, deposit_dir)

        """ 6, data_graph.bin """
        data_graph(data_field, data, deposit_dir)  

        handling_time = (time.time() - time0)/60
        return handling_time 


if __name__ == "__main__":
    
    time0 = time.time()
    data_field = ['EventID', 'Actor1Name', 'EventCode', 'Actor2Name', 'AllNames', 'NumArticles', 'IsRootEvent', 'EventDate']
    dataset_list = {
        'Egypt':['Abuja', 'Alexandria', 'Cairo', 'Lagos'],
        'Thailand': ['Bangkok'],
        "Russia": ['Moscow'],
        'India': ['New Delhi', 'Chennai'],
        'Japan':['Tokyo'],
        'America':['Chicago', 'Los Angeles', 'New York', 'San Francisco', 'Washington'],
        'UK':['London', 'Edinburgh', 'Manchester'],
    }

    with futures.ProcessPoolExecutor(max_workers=5) as excutor:
        # future list
        to_do_list = {}
        for one_dataset in dataset_list:
            # where to deposit all generated data file
            deposit_dir = "../data/training data/{0}".format(one_dataset)
            # GDELT data dir
            gdelt_dir = "/nfs/home/fzx/project/EPredict2023/data/model_comparing/{0}/2018-2022".format(one_dataset)
            # sen2vec model path
            model_path = '../data/support/America/s2v_300.bin'

            # solve path problems
            if not os.path.exists(gdelt_dir):
                logging.info(f'{gdelt_dir} is not exist!')
                exit()
            if not os.path.exists(deposit_dir):
                os.mkdir(deposit_dir)
            loc_list = dataset_list[one_dataset]

            # submit functions that need process concurrent to excutor, excutor wrap it as Future class
            future = excutor.submit(cmfDataGenerator, data_field, one_dataset, loc_list, gdelt_dir, model_path, deposit_dir)
            to_do_list[future] = one_dataset
        
        done_iter = futures.as_completed(to_do_list)
        done_map = {}
        for future in done_iter:
            handling_time = future.result()
            done_map[to_do_list[future]] = handling_time
        
        excutor.shutdown()
    
    print(done_map)
    total_time = (time.time()- time0)/60
    print(f"total time {total_time} min")


    # deposit_dir = "/nfs/home/fzx/project/EPredict2023/text/code/CMF/data/training data/{}".format("Thailand")
    # #one_dataset GDELT data dir
    # gdelt_dir = "/nfs/home/fzx/project/EPredict2023/data/model_comparing/{0}/2018-2022".format("Thailand")
    # # sen2vec model path
    # model_path = '/nfs/home/fzx/project/EPredict2023/text/code/CMF/data/support/America/s2v_300.bin'

    # one_dataset = 'Thailand'
    # loc_list = dataset_list[one_dataset]
    # # submit functions that need process concurrent to excutor, excutor wrap it as Future class
    # cmfDataGenerator(data_field, one_dataset, loc_list, gdelt_dir, model_path, deposit_dir)
            
    