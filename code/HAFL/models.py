from layers import *
import torch.nn as nn


class TAGS_modularize(nn.Module):
    def __init__(self, entity_emb, relation_emb, n_convs, n_encoders, n_heads, n_feedforward, n_output, dropout, explain=False):
        super(TAGS_modularize, self).__init__()
        self.num_relations = get_minimum_multiples(relation_emb.size(0), n_heads)
        self.num_nodes = get_minimum_multiples(entity_emb.size(0), n_heads)
        self.n_input = entity_emb.size(1)

        self.graph_processor = Graph_Processor(entity_emb, relation_emb, n_output, n_convs, n_heads, dropout)
        self.encoder1_list, self.encoder2_list = nn.ModuleList(), nn.ModuleList()
        for i in range(n_encoders):
            self.encoder1_list.append(Encoder_Layer(self.num_nodes, n_heads, n_feedforward, dropout))
            self.encoder2_list.append(Encoder_Layer(self.num_relations, n_heads, n_feedforward, dropout))
        self.predict = Predictor(self.num_nodes, self.num_relations, self.num_nodes+self.num_relations, n_output, dropout)
        self.explain = explain

    def forward(self, edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
        seq_emb1, seq_emb2 = self.graph_processor(edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq)
        for encoder1, encoder2 in zip(self.encoder1_list, self.encoder2_list):
            enc_output1, a1 = encoder1(seq_emb1)
            enc_output2, a2 = encoder2(seq_emb2)
        pred_emb = torch.cat([enc_output1, enc_output2], dim=1)
        output = self.predict(pred_emb)
        if self.explain:
            actor_contribution = self.predict.explain(pred_emb, node_idx_seq, edge_idx_seq)
            return output, actor_contribution
        else:
            return output


class TAGS2_modularize(nn.Module):
    def __init__(self, entity_emb, relation_emb, n_convs, n_encoders, n_heads, n_feedforward, n_output, dropout):
        super(TAGS2_modularize, self).__init__()
        self.num_relations = get_minimum_multiples(relation_emb.size(0), n_heads)
        self.num_nodes = get_minimum_multiples(entity_emb.size(0), n_heads)
        self.n_input = entity_emb.size(1)

        self.graph_processor = Graph_Processor2(entity_emb, relation_emb, n_output, n_convs, n_heads, dropout)
        self.encoder1_list, self.encoder2_list = nn.ModuleList(), nn.ModuleList()
        for i in range(n_encoders):
            self.encoder1_list.append(Encoder_Layer(self.n_input, n_heads, n_feedforward, dropout))
            self.encoder2_list.append(Encoder_Layer(self.n_input, n_heads, n_feedforward, dropout))
        self.predict = Predictor(self.n_input*2, n_output, dropout)

    def forward(self, edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
        seq_emb1, seq_emb2 = self.graph_processor(edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq)
        for encoder1, encoder2 in zip(self.encoder1_list, self.encoder2_list):
            enc_output1, a1 = encoder1(seq_emb1)
            enc_output2, a2 = encoder2(seq_emb2)
        output = self.predict(torch.cat([enc_output1, enc_output2], dim=1))

        return output


class TAGS_online(nn.Module):
    def __init__(self, entity_size, relation_size, n_feature, n_convs, n_encoders, n_heads, n_feedforward, n_output, dropout):
        super(TAGS_online, self).__init__()
        self.num_nodes = get_minimum_multiples(entity_size, n_heads)
        self.num_relations = get_minimum_multiples(relation_size, n_heads)

        self.graph_processor = Graph_Processor_online(entity_size, relation_size, n_feature, n_output, n_convs, n_heads, dropout)
        self.encoder1_list, self.encoder2_list = nn.ModuleList(), nn.ModuleList()
        for i in range(n_encoders):
            self.encoder1_list.append(Encoder_Layer(self.num_nodes, n_heads, n_feedforward, dropout))
            self.encoder2_list.append(Encoder_Layer(self.num_relations, n_heads, n_feedforward, dropout))
        self.predict = Predictor(self.num_nodes, self.num_relations, self.num_nodes+self.num_relations, n_output, dropout)

    def forward(self, edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
        seq_emb1, seq_emb2 = self.graph_processor(edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq)
        for encoder1, encoder2 in zip(self.encoder1_list, self.encoder2_list):
            enc_output1, a1 = encoder1(seq_emb1)
            enc_output2, a2 = encoder2(seq_emb2)
        output = self.predict(torch.cat([enc_output1, enc_output2], dim=1))

        return output


# class CNN(nn.Module):
#     def __init__(self, entity_size, relation_size, n_feature, n_convs, num_filter, filter_sizes, n_output, dropout):
#         super(CNN, self).__init__()
#         self.graph_processor = Graph_Processor_online(entity_size, relation_size, n_feature, n_output, n_convs, 8, dropout)
#         self.convs = nn.ModuleList([
#             nn.Conv2d(in_channels=1, out_channels=num_filter, kernel_size=(fs, n_feature))
#             for fs in filter_sizes])
#         self.fc = nn.Linear(len(filter_sizes) * num_filter, n_output)
#         self.dropout = torch.nn.Dropout(dropout)
#
#     def forward(self, edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
#         seq_emb1, seq_emb2 = self.graph_processor(edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq)
#         seq_emb = torch.cat([seq_emb1, seq_emb2], dim=1).unsqueeze(0).unsqueeze(1)
#         conved = [F.relu(conv(seq_emb)).squeeze(3) for conv in self.convs]
#         pooled = [F.max_pool1d(conv, conv.shape[2]).squeeze(2) for conv in conved]  # [batch,num_filter]
#         x_cat = self.dropout(torch.cat(pooled, dim=1))
#         y = torch.sigmoid(self.fc(x_cat)).squeeze(0)
#
#         return y


class CNN(nn.Module):
    def __init__(self, entity_size, relation_size, n_feature, n_convs, num_filter, filter_sizes, n_output, dropout):
        super().__init__()
        self.entity_emb = get_param((entity_size, n_feature))
        self.relation_emb = get_param((relation_size, n_feature))

        self.dropout = dropout
        self.num_nodes = entity_size
        self.num_relations = relation_size
        self.n_feature = self.num_nodes + self.num_relations

        self.compconv = CompGCNConv(n_feature, n_output, self.num_relations, opn='sub',
                                     dropout=self.dropout, bias=True, act=torch.tanh)

        self.convs = nn.ModuleList([
            nn.Conv2d(in_channels=1, out_channels=num_filter, kernel_size=(fs, self.n_feature))
            for fs in filter_sizes])

        self.fc = nn.Linear(len(filter_sizes) * num_filter, n_output)

    def forward(self, edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
        seq_feature = []
        for edge_index, edge_type, node_idx, edge_idx in zip(edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
            x_p, r_p = torch.zeros(self.num_nodes).cuda(), torch.zeros(self.num_relations).cuda()
            if len(node_idx) > 0:
                x, r = self.compconv(self.entity_emb, edge_index, edge_type, self.relation_emb)
                x_p[node_idx], r_p[edge_idx] = x.squeeze(1)[node_idx], r.squeeze(1)[edge_idx]
            seq_feature.append(torch.cat([x_p, r_p], dim=0))
        feature = torch.stack(seq_feature).unsqueeze(0).unsqueeze(1)
        conved = [F.relu(conv(feature)).squeeze(3) for conv in self.convs]
        pooled = [F.max_pool1d(conv, conv.shape[2]).squeeze(2) for conv in conved]  # [batch,num_filter]
        x_cat = torch.cat(pooled, dim=1)
        cat = F.dropout(x_cat, self.dropout, training=self.training)
        y = torch.sigmoid(self.fc(cat)).squeeze(0)

        return y


class GRU(nn.Module):
    def __init__(self, entity_size, relation_size, n_feature, n_convs, n_encoders, n_heads, n_feedforward, n_output, dropout):
        super(GRU, self).__init__()
        self.num_nodes = get_minimum_multiples(entity_size, n_heads)
        self.num_relations = get_minimum_multiples(relation_size, n_heads)

        self.graph_processor = Graph_Processor_online(entity_size, relation_size, n_feature, n_output, n_convs, n_heads, dropout)
        self.sequence_encoder1 = nn.GRU(self.num_nodes, self.num_nodes, batch_first=True)
        self.sequence_encoder2 = nn.GRU(self.num_relations, self.num_relations, batch_first=True)
        self.predict = nn.Linear(self.num_nodes+self.num_relations, n_output)
        self.dropout = torch.nn.Dropout(dropout)

    def forward(self, edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
        seq_emb1, seq_emb2 = self.graph_processor(edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq)
        _, seq_emb1 = self.sequence_encoder1(seq_emb1.unsqueeze(0))
        _, seq_emb2 = self.sequence_encoder2(seq_emb2.unsqueeze(0))
        logit = self.dropout(self.predict(torch.cat([seq_emb1[0,0], seq_emb2[0,0]], dim=0)))
        output = torch.sigmoid(logit)

        return output


class BiLSTM(nn.Module):
    def __init__(self, entity_size, relation_size, n_feature, n_convs, n_encoders, n_heads, n_feedforward, n_output, dropout):
        super(BiLSTM, self).__init__()
        self.num_nodes = get_minimum_multiples(entity_size, n_heads)
        self.num_relations = get_minimum_multiples(relation_size, n_heads)

        self.graph_processor = Graph_Processor_online(entity_size, relation_size, n_feature, n_output, n_convs, n_heads, dropout)
        self.sequence_encoder1 = nn.LSTM(self.num_nodes, self.num_nodes, batch_first=True, bidirectional=True)
        self.sequence_encoder2 = nn.LSTM(self.num_relations, self.num_relations, batch_first=True, bidirectional=True)
        self.predict = nn.Linear((self.num_nodes+self.num_relations)*2, n_output)
        self.dropout = torch.nn.Dropout(dropout)

    def forward(self, edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
        seq_emb1, seq_emb2 = self.graph_processor(edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq)
        seq_emb1, _ = self.sequence_encoder1(seq_emb1.unsqueeze(0))
        seq_emb2, _ = self.sequence_encoder2(seq_emb2.unsqueeze(0))
        logit = self.dropout(self.predict(torch.cat([seq_emb1[0, -1, :], seq_emb2[0, -1, :]], dim=0)))
        output = torch.sigmoid(logit)

        return output