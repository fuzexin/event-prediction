from layers import *
from baselines_layers import *


class CNN(nn.Module):
    def __init__(self, entity_emb, relation_emb, num_filter, filter_sizes, n_output, dropout):
        super().__init__()
        self.entity_emb = entity_emb
        self.relation_emb = relation_emb

        self.dropout = dropout
        self.entity_emb = entity_emb
        self.num_nodes = entity_emb.size(0)
        self.n_input = entity_emb.size(1)
        self.relation_emb = relation_emb
        self.num_relations = relation_emb.size(0)
        self.n_feature = self.num_nodes + self.num_relations

        # self.compconv1 = CompGCNConv(self.n_feature, self.n_feature, self.num_relations, opn='sub',
        #                              dropout=0.1, bias=True, act=torch.tanh)
        self.compconv2 = CompGCNConv(self.n_input, n_output, self.num_relations, opn='sub',
                                     dropout=self.dropout, bias=True, act=torch.tanh)

        self.convs = nn.ModuleList([
            nn.Conv2d(in_channels=1, out_channels=num_filter, kernel_size=(fs, self.n_feature))
            for fs in filter_sizes])

        self.fc = nn.Linear(len(filter_sizes) * num_filter, n_output)

    def forward(self, edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
        seq_feature = []
        for edge_index, edge_type, node_idx, edge_idx in zip(edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
            x_p, r_p = torch.zeros(self.num_nodes).cuda(), torch.zeros(self.num_relations).cuda()
            if len(node_idx) > 0:
                x, r = self.compconv2(self.entity_emb, edge_index, edge_type, self.relation_emb)
                x_p[node_idx], r_p[edge_idx] = x.squeeze(1)[node_idx], r.squeeze(1)[edge_idx]
            seq_feature.append(torch.cat([x_p, r_p], dim=0))
        feature = torch.stack(seq_feature).unsqueeze(0).unsqueeze(1)
        conved = [F.relu(conv(feature)).squeeze(3) for conv in self.convs]
        pooled = [F.max_pool1d(conv, conv.shape[2]).squeeze(2) for conv in conved]  # [batch,num_filter]
        x_cat = torch.cat(pooled, dim=1)
        cat = F.dropout(x_cat, self.dropout, training=self.training)

        return torch.sigmoid(self.fc(cat)).squeeze(0)


class RNN(nn.Module):
    def __init__(self, entity_emb, relation_emb, n_output, dropout, rnn_model='gru'):
        super(RNN, self).__init__()
        self.entity_emb = entity_emb
        self.relation_emb = relation_emb

        self.dropout = dropout
        self.entity_emb = entity_emb
        self.num_nodes = entity_emb.size(0)
        self.n_input = entity_emb.size(1)
        self.relation_emb = relation_emb
        self.num_relations = relation_emb.size(0)
        self.n_feature = self.num_nodes + self.num_relations

        # self.compconv1 = CompGCNConv(self.n_feature, self.n_feature, self.num_relations, opn='sub',
        #                              dropout=0.1, bias=True, act=torch.tanh)
        self.compconv2 = CompGCNConv(self.n_input, n_output, self.num_relations, opn='sub',
                                     dropout=self.dropout, bias=True, act=torch.tanh)
        if rnn_model == 'rnn':
            self.rnn = nn.RNN(self.n_feature, self.n_feature, batch_first=True)
        elif rnn_model == 'lstm':
            self.rnn = nn.LSTM(self.n_feature, self.n_feature, batch_first=True)
        else:
            self.rnn = nn.GRU(self.n_feature, self.n_feature, batch_first=True)
        self.linear = nn.Linear(self.n_feature, n_output)

    def forward(self, edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
        seq_feature = []
        for edge_index, edge_type, node_idx, edge_idx in zip(edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
            x_p, r_p = torch.zeros(self.num_nodes).cuda(), torch.zeros(self.num_relations).cuda()
            if len(node_idx) > 0:
                x, r = self.compconv2(self.entity_emb, edge_index, edge_type, self.relation_emb)
                x_p[node_idx], r_p[edge_idx] = x.squeeze(1)[node_idx], r.squeeze(1)[edge_idx]
            seq_feature.append(torch.cat([x_p, r_p], dim=0))
        seq_feature = torch.stack(seq_feature).unsqueeze(0)
        output, _ = self.rnn(seq_feature)
        output = F.dropout(output, self.dropout, training=self.training)
        output = output[0, -1, :]
        output = torch.sigmoid(self.linear(output))

        return output


class BiLSTM(nn.Module):
    def __init__(self, entity_emb, relation_emb, n_output, dropout):
        super(BiLSTM, self).__init__()
        self.entity_emb = entity_emb
        self.relation_emb = relation_emb

        self.dropout = dropout
        self.entity_emb = entity_emb
        self.num_nodes = entity_emb.size(0)
        self.n_input = entity_emb.size(1)
        self.relation_emb = relation_emb
        self.num_relations = relation_emb.size(0)
        self.n_feature = self.num_nodes + self.num_relations

        self.compconv2 = CompGCNConv(self.n_input, n_output, self.num_relations, opn='sub',
                                     dropout=self.dropout, bias=True, act=torch.tanh)
        self.rnn = nn.LSTM(self.n_feature, self.n_feature, batch_first=True, bidirectional=True)
        self.linear = nn.Linear(self.n_feature*2, n_output)

    def forward(self, edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
        seq_feature = []
        for edge_index, edge_type, node_idx, edge_idx in zip(edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
            x_p, r_p = torch.zeros(self.num_nodes).cuda(), torch.zeros(self.num_relations).cuda()
            if len(node_idx) > 0:
                x, r = self.compconv2(self.entity_emb, edge_index, edge_type, self.relation_emb)
                x_p[node_idx], r_p[edge_idx] = x.squeeze(1)[node_idx], r.squeeze(1)[edge_idx]
            seq_feature.append(torch.cat([x_p, r_p], dim=0))
        seq_feature = torch.stack(seq_feature).unsqueeze(0)
        output, _ = self.rnn(seq_feature)
        output = F.dropout(output, self.dropout, training=self.training)
        output = output[0, -1, :]
        output = torch.sigmoid(self.linear(output))

        return output


class DNN(nn.Module):
    def __init__(self, pretrained_emb, n_output, dropout):
        super(DNN, self).__init__()
        self.n_feature = pretrained_emb.size(1)
        self.n_input = pretrained_emb.size(0)
        self.dropout = dropout

        self.embedding = nn.Embedding(pretrained_emb.size(0), pretrained_emb.size(1))
        self.embedding.weight = nn.Parameter(pretrained_emb)
        self.embedding.weight.requires_grad = False

        self.fc = nn.Sequential(
            nn.Linear(self.n_feature, self.n_feature, bias=True),
            nn.ReLU(),
            nn.Linear(self.n_feature, self.n_feature, bias=True),
            nn.ReLU(),
            nn.Linear(self.n_feature, self.n_feature, bias=True),
            nn.ReLU(),
            nn.Linear(self.n_feature, self.n_feature, bias=True),
            nn.ReLU(),
            nn.Linear(self.n_feature, self.n_feature, bias=True),
            nn.ReLU(),
            nn.Linear(self.n_feature, n_output, bias=True))
        self.mask = MaskLinear(self.n_input, n_output)

    def forward(self, adj, vertice):
        x = self.embedding(vertice)
        x = self.fc(x)
        x = F.dropout(x, self.dropout, training=self.training)
        x = self.mask(x, vertice)
        x = torch.sigmoid(x)

        return x


class GCN(nn.Module):
    def __init__(self, pretrained_emb, n_output, dropout):
        super(GCN, self).__init__()
        self.n_feature = pretrained_emb.size(1)
        self.n_input = pretrained_emb.size(0)
        self.dropout = dropout
        self.embedding = nn.Embedding(pretrained_emb.size(0), pretrained_emb.size(1))
        self.embedding.weight = nn.Parameter(pretrained_emb)
        self.embedding.weight.requires_grad = False

        self.gcn1 = SpGraphConvLayer(self.n_feature, self.n_feature)
        self.bn1 = nn.BatchNorm1d(self.n_feature)
        self.gcn2 = SpGraphConvLayer(self.n_feature, 1)
        self.bn2 = nn.BatchNorm1d(1)
        self.mask = MaskLinear(self.n_input, n_output)

    def forward(self, adj, vertice):
        emb = self.embedding(vertice)
        x = emb
        x = self.bn1(self.gcn1(x, adj))
        # x = F.relu(x)
        x = self.bn2(self.gcn2(x, adj))
        # x = F.relu(x)
        x = F.dropout(x, self.dropout, training=self.training)
        x = self.mask(x, vertice)
        x = torch.sigmoid(x)
        return x


class CompGCN(nn.Module):
    def __init__(self, entity_emb, relation_emb, n_output, dropout):
        super(CompGCN, self).__init__()
        self.entity_emb = entity_emb
        self.relation_emb = relation_emb

        self.dropout = dropout
        self.n_input = entity_emb.size(0)
        self.n_feature = entity_emb.size(1)
        self.num_relations = relation_emb.size(0)

        self.compconv1 = CompGCNConv(self.n_feature, self.n_feature, self.num_relations, opn='sub',
                                     dropout=self.dropout, bias=True, act=torch.tanh)
        self.compconv2 = CompGCNConv(self.n_feature, n_output, self.num_relations, opn='sub',
                                     dropout=self.dropout, bias=True, act=torch.tanh)
        self.linear = nn.Linear(self.n_input+self.num_relations, n_output)

    def forward(self, edge_index, edge_type, node_idx, edge_idx):
        mask_x, mask_r = torch.zeros(self.n_input).cuda(), torch.zeros(self.num_relations).cuda()
        x, r = self.compconv1(self.entity_emb, edge_index, edge_type, self.relation_emb)
        x, r = self.compconv2(x, edge_index, edge_type, r)
        x, r = F.dropout(x, self.dropout, training=self.training).squeeze(), F.dropout(r, self.dropout, training=self.training).squeeze()
        mask_x[node_idx], mask_r[edge_idx] = x[node_idx], r[edge_idx]
        output = torch.sigmoid(self.linear(torch.cat([mask_x, mask_r], dim=0)))

        return output


class EvolveGCN(nn.Module):
    def __init__(self, entity_size, n_feature, n_output, dropout, rnn_model):
        super(EvolveGCN, self).__init__()
        self.dropout = dropout

        self.embedding = nn.Embedding(entity_size, n_feature)
        self.embedding.weight = nn.Parameter(torch.Tensor(entity_size, n_feature))
        xavier_normal_(self.embedding.weight.data)
        self.n_input = entity_size

        self.layer_stack = nn.ModuleList()
        self.bn_stack = nn.ModuleList()
        for i in range(14):
            self.layer_stack.append(SpGraphConvLayer(n_feature, n_output))
            self.bn_stack.append(nn.BatchNorm1d(n_output))

        if rnn_model == 'rnn':
            self.rnn = nn.RNN(entity_size, entity_size, batch_first=True)
        elif rnn_model == 'lstm':
            self.rnn = nn.LSTM(entity_size, entity_size, batch_first=True)
        else:
            self.rnn = nn.GRU(entity_size, entity_size, batch_first=True)

        self.linear = nn.Linear(entity_size, n_output)

    def forward(self, adjs, vertices):
        emb = self.embedding(vertices)
        seq = []
        for i, adj in enumerate(adjs):
            graph_feature = torch.zeros(self.n_input).cuda() if torch.cuda.is_available() else torch.zeros(self.n_input)
            if adjs[i].size(0) > 1:
                last_x = self.bn_stack[i](self.layer_stack[i](emb, adjs[i]))
                graph_feature[vertices] = last_x.squeeze()
            seq.append(graph_feature)
        seq_emb = torch.stack(seq).unsqueeze(0)
        seq_emb, _ = self.rnn(seq_emb)
        # _, seq_emb = self.rnn(seq_emb) # gru
        output = torch.sigmoid(self.linear(seq_emb.squeeze(0)[-1]))

        return output


class DynamicGCN(nn.Module):
    def __init__(self, entity_size, n_feature, n_output, n_hidden, dropout, instance_norm=False):
        super(DynamicGCN, self).__init__()
        self.dropout = dropout
        self.instance_norm = instance_norm
        if self.instance_norm:
            self.norm = nn.InstanceNorm1d(n_feature, momentum=0.0, affine=True)

        self.embedding = nn.Embedding(entity_size, n_feature)
        self.embedding.weight = nn.Parameter(torch.Tensor(entity_size, n_feature))
        xavier_normal_(self.embedding.weight.data)

        self.layer_stack = nn.ModuleList()  # TODO class initiate
        self.bn_stack = nn.ModuleList()
        self.temporal_cells = nn.ModuleList()
        for i in range(n_hidden - 1):
            self.layer_stack.append(SpGraphConvLayer(n_feature, n_feature))
            self.bn_stack.append(nn.BatchNorm1d(n_feature))
            self.temporal_cells.append(TemporalEncoding(n_feature, n_feature))
        self.layer_stack.append(SpGraphConvLayer(n_feature, n_feature))
        self.layer_stack.append(SpGraphConvLayer(n_feature, n_output))
        self.bn_stack.append(nn.BatchNorm1d(n_feature))
        self.bn_stack.append(nn.BatchNorm1d(n_output))
        self.mask = MaskLinear(entity_size, n_output)

    def forward(self, adjs, vertices):
        emb = self.embedding(vertices)
        if self.instance_norm:
            emb = self.norm(emb)
        x = emb
        for i, gcn_layer in enumerate(self.layer_stack):
            if i < len(adjs):
                last_x = x
                if adjs[i].size(0) > 1:
                    x = self.bn_stack[i](gcn_layer(emb, adjs[i]))
                x = F.relu(x)
                x = F.dropout(x, self.dropout, training=self.training)
                if i > 0:
                    x = self.temporal_cells[i - 1](last_x, x)
                if i == len(adjs)-1:
                    x = self.bn_stack[-1](self.layer_stack[-1](x, adjs[i]))
        x = F.relu(x)
        x = self.mask(x, vertices)
        x = torch.sigmoid(x)
        return x


class TAGS_gcn(nn.Module):
    def __init__(self, entity_size, n_feature, len_seq, n_heads, n_feedforward, n_output, dropout):
        super(TAGS_gcn, self).__init__()
        self.n_input = get_minimum_multiples(entity_size, n_heads)
        self.n_feature = n_feature
        self.graph_processor = GCN_Layer(entity_size, n_feature, len_seq, n_heads, n_output, dropout)
        self.encoder = Encoder(self.n_input, n_heads, n_feedforward, dropout)
        self.decoder = Decoder(self.n_input, len_seq, n_output)

    def forward(self, adjs, vertices):
        enc_input = self.graph_processor(adjs, vertices)
        enc_output = self.encoder(enc_input)
        output = self.decoder(enc_output)

        return output