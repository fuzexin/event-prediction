import argparse
import os
import time

os.environ["CUDA_VISIBLE_DEVICES"] = "0"
from sklearn.metrics import *
from models import *


def test():
    model.eval()
    y_true, y_pred = [], []
    contribution = []
    t = time.time()
    for i in range(len(y_list)):
        edge_index_seq = edge_index_data[i]
        edge_type_seq = edge_type_data[i]
        node_idx_seq = node_idx_data[i]
        edge_idx_seq = edge_idx_data[i]
        y = y_list[i]
        if args.cuda:
            for j in range(len(edge_index_seq)):
                edge_index_seq[j] = edge_index_seq[j].cuda()
                edge_type_seq[j] = edge_type_seq[j].cuda()
                node_idx_seq[j] = node_idx_seq[j].cuda()
            y = y.cuda()
        output, actor_contribution = model(edge_index_seq[14 - args.len_seq:], edge_type_seq[14 - args.len_seq:], node_idx_seq[14 - args.len_seq:], edge_idx_seq[14 - args.len_seq:])
        y_true += y.data.tolist()
        bi_val = np.where(output.data.cpu().numpy() > 0.5, 1, 0)
        y_pred += torch.from_numpy(bi_val).tolist()
        if output.item() > 0.5 and y.item() == 1.:
            contribution.append(actor_contribution)
            print(args.location, i, output.item(), y.item())
    tt = time.time() - t
    prec, rec, f1, _ = precision_recall_fscore_support(y_true, y_pred, average="binary")
    acc = accuracy_score(y_true, y_pred)
    with open('contribution/{}.pkl'.format(args.location), 'wb') as f:
        pkl.dump(contribution, f)
    return acc, prec, rec, f1, round(tt, 3)


def test_one_sample(idx):
    model.eval()
    edge_index_seq = edge_index_data[idx]
    edge_type_seq = edge_type_data[idx]
    node_idx_seq = node_idx_data[idx]
    edge_idx_seq = edge_idx_data[idx]
    y = y_list[idx]
    if args.cuda:
        for j in range(len(edge_index_seq)):
            edge_index_seq[j] = edge_index_seq[j].cuda()
            edge_type_seq[j] = edge_type_seq[j].cuda()
            node_idx_seq[j] = node_idx_seq[j].cuda()
        y = y.cuda()
    output, actor_contribution = model(edge_index_seq[:args.len_seq], edge_type_seq[:args.len_seq], node_idx_seq[:args.len_seq], edge_idx_seq[:args.len_seq])

    return output, y.item(), actor_contribution


def get_topk(value_list, k):
    max_idx = []
    sorted_list = process_list(value_list)
    sorted_list.reverse()
    topk = sorted_list[:k]
    for top in topk:
        max_idx.append(value_list.index(top))
    return max_idx


parser = argparse.ArgumentParser()
parser.add_argument('--location', type=str, default='USW', choices=['USW', 'USE', 'IND', 'UK'])
parser.add_argument('--datatype', type=str, default='correlation')
parser.add_argument('--len_seq', type=int, default=14)
parser.add_argument('--n_convs', type=int, default=1, choices=[1, 2, 3])
parser.add_argument('--n_encoders', type=int, default=1, choices=[1, 2, 3, 6])
parser.add_argument('--n_heads', type=int, default=8, choices=[1, 2, 4, 8, 16])
parser.add_argument('--n_feedforward', type=int, default=64)
parser.add_argument('--dropout', type=float, default=0.5)
parser.add_argument('--n_output', type=int, default=1)
args = parser.parse_args()
args.cuda = torch.cuda.is_available()

location_list = ['USW', 'USE', 'UK', 'RU', 'IND', 'INDN']
for location in location_list[:]:
    args.location = location
    entity_emb, relation_emb, edge_index_data, edge_type_data, node_idx_data, y_list = load_data(args.location, args.datatype)
    edge_idx_data = get_edge_idx_data(edge_type_data)

    model = TAGS_modularize(entity_emb=entity_emb,
                            relation_emb=relation_emb,
                            n_convs=args.n_convs,
                            n_encoders=args.n_encoders,
                            n_heads=args.n_heads,
                            n_feedforward=args.n_feedforward,
                            n_output=args.n_output,
                            dropout=args.dropout,
                            explain=True)
    model.load_state_dict(torch.load('models/{}_correlation.pkl'.format(args.location), map_location='cpu'))

    # output = test_one_sample(905)
    # print(output[0])
    # print(output[1])
    # print(output[2])

    # indicators = test()
    # print(args.location, indicators[1:-1])

    actor_list, event_list = [], []
    with open('data/{}/actor_vocab.txt'.format(location), 'r', encoding='utf-8') as f:
        lines = f.readlines()
    for line in lines:
        actor_list.append(line.strip())
    with open('data/{}/event_vocab.txt'.format(location), 'r', encoding='utf-8') as f:
        lines = f.readlines()
    for line in lines:
        event_list.append(line.strip().split("\t")[-1])

    with open('contribution/{}.pkl'.format(location), 'rb') as f:
        contribution = pkl.load(f)
    num_nodes = get_minimum_multiples(entity_emb.size(0), 8)
    new_contribution = []
    for i in range(len(contribution[0])):
        sum, count = 0., 0
        for sample_contribution in contribution:
            score = sample_contribution[i]
            if score > 0.:
                sum += score
                count += 1
        if count > 0:
            new_contribution.append(sum / count)
        else:
            new_contribution.append(0.)

    actor_contribution, event_contribution = new_contribution[:num_nodes], new_contribution[num_nodes:]
    actor_idx, event_idx = get_topk(actor_contribution, 20), get_topk(event_contribution, 20)
    top_actors, top_events = [actor_list[i] for i in actor_idx], [event_list[i] for i in event_idx]
    print(top_actors)
    print(top_events)
    with open('contribution/{}_precursors.pkl'.format(location), 'wb') as f:
        pkl.dump([top_actors, top_events], f)