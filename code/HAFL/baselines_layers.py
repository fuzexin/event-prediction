from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from math import sqrt

import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init
from torch.nn.modules.module import Module

from layers import *
from utils import *


class SpGraphConvLayer(Module):
    def __init__(self, in_features, out_features, bias=True):
        super(SpGraphConvLayer, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(in_features, out_features))
        init.xavier_uniform_(self.weight)

        if bias:
            self.bias = Parameter(torch.Tensor(out_features))
            stdv = 1. / sqrt(self.bias.size(0))
            self.bias.data.uniform_(-stdv, stdv)
        else:
            self.register_parameter('bias', None)

    def forward(self, feature, adj):
        support = torch.mm(feature, self.weight)  # sparse
        output = torch.spmm(adj, support)
        if self.bias is not None:
            return output + self.bias
        else:
            return output

    def __repr__(self):
        return self.__class__.__name__ + ' (' + str(self.in_features) + ' -> ' + str(self.out_features) + ')'


class GraphAttentionLayer(nn.Module):
    def __init__(self, in_features, out_features, dropout=0., alpha=0.2, concat=False):
        super(GraphAttentionLayer, self).__init__()
        self.dropout = dropout
        self.in_features = in_features
        self.out_features = out_features
        self.alpha = alpha
        self.concat = concat

        self.W = nn.Parameter(torch.empty(size=(in_features, out_features)))
        nn.init.xavier_uniform_(self.W.data, gain=1.414)
        self.a = nn.Parameter(torch.empty(size=(2 * out_features, 1)))
        nn.init.xavier_uniform_(self.a.data, gain=1.414)
        self.leakyrelu = nn.LeakyReLU(self.alpha)

    def forward(self, h, adj):
        Wh = torch.mm(h, self.W)
        Wh1 = torch.matmul(Wh, self.a[:self.out_features, :])
        Wh2 = torch.matmul(Wh, self.a[self.out_features:, :])
        e = self.leakyrelu(Wh1 + Wh2.T)
        zero_vec = -9e15 * torch.ones_like(e)
        attention = torch.where(adj > 0, e, zero_vec)
        attention = F.softmax(attention, dim=1)
        attention = F.dropout(attention, self.dropout, training=self.training)
        h_prime = torch.matmul(attention, Wh)
        if self.concat:
            return F.elu(h_prime)
        else:
            return h_prime

    def __repr__(self):
        return self.__class__.__name__ + ' (' + str(self.in_features) + ' -> ' + str(self.out_features) + ')'


class TemporalEncoding(Module):
    def __init__(self, in_features, out_features, bias=True):
        super(TemporalEncoding, self).__init__()
        out_features = int(in_features / 2) # not useful now
        out_o = out_c = int(in_features / 2)
        self.weight_o = Parameter(torch.Tensor(in_features, out_o))
        self.weight_c = Parameter(torch.Tensor(in_features, out_c))
        nn.init.xavier_uniform_(self.weight_o.data, gain=1.667)
        nn.init.xavier_uniform_(self.weight_c.data, gain=1.667)
        if bias:
            self.bias = Parameter(torch.Tensor(in_features))
            stdv = 1. / sqrt(self.bias.size(0))
            self.bias.data.uniform_(-stdv, stdv)
        else:
            self.register_parameter('bias', None)

    def forward(self, h_o, h_c):
        trans_ho = torch.mm(h_o, self.weight_o)
        trans_hc = torch.mm(h_c, self.weight_c)
        output =torch.tanh( (torch.cat((trans_ho, trans_hc), dim=1))) # dim=1

        if self.bias is not None:
            return output + self.bias
        else:
            return output


class MaskLinear(Module):
    def __init__(self, in_features, out_features=1, bias=True):
        super(MaskLinear, self).__init__()
        self.in_features = in_features
        self.out_features = out_features

        self.weight = Parameter(torch.Tensor(in_features))
        if bias:
            self.bias = Parameter(torch.Tensor(out_features))
        else:
            self.register_parameter('bias', None)

        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / sqrt(self.weight.size(0))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, x, idx):  # idx is a list
        if torch.cuda.is_available():
            mask = torch.zeros(self.in_features).cuda()
        else:
            mask = torch.zeros(self.in_features)
        mask[idx] = x.squeeze()
        output = torch.matmul(self.weight, mask)
        if self.bias is not None:
            return output + self.bias
        else:
            return output

    def __repr__(self):
        return self.__class__.__name__ + ' (' + str(self.in_features) + ' => ' + str(self.out_features) + ')'\


class GCN_Layer(nn.Module):
    def __init__(self, entity_size, n_feature, len_seq, n_heads, n_output, dropout):
        super(GCN_Layer, self).__init__()
        self.len_seq = len_seq
        self.n_output = n_output
        self.n_input = get_minimum_multiples(entity_size, n_heads)
        self.n_feature = n_feature
        self.pe = self.get_positional_encoding(self.len_seq, self.n_input)

        self.embedding = nn.Embedding(entity_size, n_feature)
        self.embedding.weight = nn.Parameter(torch.Tensor(entity_size, n_feature))
        xavier_normal_(self.embedding.weight.data)

        # self.gcn1 = GraphAttentionLayer(self.n_feature, self.n_feature)
        # self.bn1 = nn.BatchNorm1d(self.n_feature)
        self.gcn2 = SpGraphConvLayer(self.n_feature, self.n_output)
        self.bn2 = nn.BatchNorm1d(self.n_output)

        self.dropout = torch.nn.Dropout(dropout)

    def get_positional_encoding(self, len_seq, embed_dim):
        positional_encoding = np.array([
            [pos / np.power(10000, 2 * i / embed_dim) for i in range(embed_dim)]
            if pos != 0 else np.zeros(embed_dim) for pos in range(len_seq)])
        positional_encoding[1:, 0::2] = np.sin(positional_encoding[1:, 0::2])
        positional_encoding[1:, 1::2] = np.cos(positional_encoding[1:, 1::2])
        return torch.tensor(positional_encoding).cuda() if torch.cuda.is_available() else torch.tensor(positional_encoding)

    def forward(self, adjs, vertices):
        self.device = adjs[0].device
        feature_list = []
        emb = self.embedding(vertices)
        for i, adj in enumerate(adjs):
            graph_feature = torch.zeros(self.n_input).cuda() if torch.cuda.is_available() else torch.zeros(self.n_input)
            if adj.size(0) > 1:
                x = F.relu(self.bn2(self.gcn2(emb, adj)))
                x = self.dropout(x)
                # x = F.relu(self.bn2(self.gcn2(x, adj)))
                graph_feature[vertices] = x.squeeze()
            feature_list.append(graph_feature)
        feature = torch.stack(feature_list)
        enc_input = (self.pe + feature).type_as(feature).to(self.device)

        return enc_input


class Encoder(nn.Module):
    def __init__(self, n_input, n_heads, n_feedforward, dropout):
        super(Encoder, self).__init__()
        self.n_input = n_input
        self.mh_att = MultiHead_Attention(n_input, n_input, n_input, n_heads, dropout)
        # self.mh_att = Self_Attention(n_input, n_input, n_input)
        self.ff = FeedForward(n_input, n_feedforward)
        self.ln1 = nn.LayerNorm(self.n_input)
        self.ln2 = nn.LayerNorm(self.n_input)

    def forward(self, enc_input):
        atten_output, _ = self.mh_att(enc_input)
        add_norm = self.ln1(atten_output + enc_input)
        enc_output = self.ln2(self.ff(add_norm) + add_norm)

        return enc_output


class Decoder(nn.Module):
    def __init__(self, n_input, len_seq, n_output):
        super(Decoder, self).__init__()
        self.linear1 = nn.Linear(n_input, n_output)
        self.linear2 = nn.Linear(len_seq, n_output)

    def forward(self, enc_output):
        x = self.linear1(enc_output)
        x = self.linear2(x.T).squeeze(-1)
        return torch.sigmoid(x)
