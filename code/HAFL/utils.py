import numpy as np
import pickle as pkl
import random
import os

import torch
from torch.nn import Parameter, init
from torch.nn.init import *
import scipy.sparse as sp


def process_list(lis):
    return sorted(list(set(lis)))


def get_minimum_multiples(n_nodes, n_heads):
    while n_nodes % n_heads != 0:
        n_nodes += 1
    return n_nodes


def shuffle(rand_seed, edge_index_data, edge_type_data, node_idx_data, y_list):
    random.seed(rand_seed)
    zip_list = list(zip(edge_index_data, edge_type_data, node_idx_data, y_list))
    random.shuffle(zip_list)
    edge_index_data, edge_type_data, node_idx_data, y_list = zip(*zip_list)
    edge_index_data, edge_type_data, node_idx_data, y_list = list(edge_index_data), list(edge_type_data), list(
        node_idx_data), list(y_list)

    return edge_index_data, edge_type_data, node_idx_data, y_list


def cut_dataset(k, old_list):
    length = len(old_list)
    test_list = old_list[int(0.1 * k * length): int(0.1 * (k + 1) * length)]
    del old_list[int(0.1 * k * length): int(0.1 * (k + 1) * length)]
    new_list = old_list + test_list

    return new_list


def cross_cut(k, edge_index_data, edge_type_data, node_idx_data, y_list):
    edge_index_data = cut_dataset(k, edge_index_data)
    edge_type_data = cut_dataset(k, edge_type_data)
    node_idx_data = cut_dataset(k, node_idx_data)
    y_list = cut_dataset(k, y_list)

    return edge_index_data, edge_type_data, node_idx_data, y_list


################
def get_edge_idx(edge_type):
    idx = []
    for edge_idx in edge_type:
        idx.append(edge_idx.item())
    return torch.tensor(process_list(idx))


###############
def get_edge_idx_data(edge_type_data):
    # deduplicate and sort day's event-type  
    edge_idx_data = []
    for seq_edge_type in edge_type_data:
        seq_edge_idx = []
        for edge_type in seq_edge_type:
            seq_edge_idx.append(get_edge_idx(edge_type))
        edge_idx_data.append(seq_edge_idx)
    return edge_idx_data


def load_data(location, datatype):
    with open('data/{}/{}_{}.pkl'.format(location, location, datatype), 'rb') as f:
        entity_emb, relation_emb, edge_index_data, edge_type_data, node_idx_data, y_list = pkl.load(f)
    return entity_emb, relation_emb, edge_index_data, edge_type_data, node_idx_data, y_list


def get_param(shape):
    param = Parameter(torch.Tensor(*shape))
    xavier_normal_(param.data)  # 正态分布
    # xavier_uniform_(param.data)  # 均匀分布
    # kaiming_uniform_(param.data, a=math.sqrt(5))
    return param


def get_positional_encoding(len_seq, embed_dim):
    positional_encoding = np.array([
        [pos / np.power(10000, 2 * i / embed_dim) for i in range(embed_dim)]
        if pos != 0 else np.zeros(embed_dim) for pos in range(len_seq)])
    positional_encoding[1:, 0::2] = np.sin(positional_encoding[1:, 0::2])
    positional_encoding[1:, 1::2] = np.cos(positional_encoding[1:, 1::2])
    return torch.tensor(positional_encoding)


def com_mult(a, b):
    r1, i1 = a[..., 0], a[..., 1]
    r2, i2 = b[..., 0], b[..., 1]
    return torch.stack([r1 * r2 - i1 * i2, r1 * i2 + i1 * r2], dim=-1)


def conj(a):
    a[..., 1] = -a[..., 1]
    return a


def cconv(a, b):
    return torch.irfft(com_mult(torch.rfft(a, 1), torch.rfft(b, 1)), 1, signal_sizes=(a.shape[-1],))


def ccorr(a, b):
    return torch.irfft(com_mult(conj(torch.rfft(a, 1)), torch.rfft(b, 1)), 1, signal_sizes=(a.shape[-1],))


def find_file(filepath, match_string):
    file_list = []
    names = os.listdir(filepath)
    for name in names:
        if match_string in name:
            file_list.append(name)
    file_list = process_list(file_list)
    return file_list


def Get_EventMapping():
    event_mapping = {}
    with open('support/CAMEO.eventcodes.txt', 'r', encoding='utf-8') as f:
        lines = f.readlines()
    for i, line in enumerate(lines):
        if i > 0:
            split_line = line.strip().split('\t')
            event_code = split_line[0]
            event_type = split_line[1]
            event_mapping[event_code] = event_type

    return event_mapping


def count(y_list):
    pos_count, neg_count = 0, 0
    for y in y_list:
        if y.item() > 0.:
            pos_count += 1
        else:
            neg_count += 1
    return pos_count, neg_count


def simulate(pos_rate, sample_count):
    prec_list, rec_list, f1_list = [], [], []
    for i in range(100):
        pred_list, label_list = [], []
        for j in range(sample_count):
            if random.random() < pos_rate:
                pred_list.append(1.)
            else:
                pred_list.append(0.)
            if j < int(sample_count*pos_rate):
                label_list.append(1.)
            else:
                label_list.append(0.)
        prec, rec, f1, _ = precision_recall_fscore_support(label_list, pred_list, average="binary")
        prec_list.append(prec)
        rec_list.append(rec)
        f1_list.append(f1)
    return np.mean(pred_list), np.mean(rec_list), np.mean(f1_list)


# 邻接矩阵相关1
def normalize(mx):
    """Row-normalize sparse matrix"""
    rowsum = np.array(mx.sum(1))
    r_inv = np.power(rowsum, -1).flatten()
    r_inv[np.isinf(r_inv)] = 0.
    r_mat_inv = sp.diags(r_inv)
    mx = r_mat_inv.dot(mx)
    return mx


# 邻接矩阵相关2
def sparse_mx_to_torch_sparse_tensor(sparse_mx):
    """Convert a scipy sparse matrix to a torch sparse tensor."""
    sparse_mx = sparse_mx.tocoo().astype(np.float32)
    indices = torch.from_numpy(
        np.vstack((sparse_mx.row, sparse_mx.col)).astype(np.int64))
    values = torch.from_numpy(sparse_mx.data)
    shape = torch.Size(sparse_mx.shape)
    return torch.sparse.FloatTensor(indices, values, shape)


def shuffle_dgcn(rand_seed, adj_list, idx_list, y_list):
    random.seed(rand_seed)
    zip_list = list(zip(adj_list, idx_list, y_list))
    random.shuffle(zip_list)
    adj_list, idx_list, y_list = zip(*zip_list)
    adj_list, idx_list, y_list = list(adj_list), list(idx_list), list(y_list)

    return adj_list, idx_list, y_list


def cross_cut_dgcn(k, adj_list, idx_list, y_list):
    adj_list = cut_dataset(k, adj_list)
    idx_list = cut_dataset(k, idx_list)
    y_list = cut_dataset(k, y_list)

    return adj_list, idx_list, y_list