from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from math import sqrt

import torch
import torch.nn as nn
import torch.nn.functional as F

from compgcn_conv import *
from utils import *


class Graph_Processor_online(nn.Module):
    def __init__(self, entity_size, relation_size, n_feature, n_output, n_convs, n_heads, dropout):
        super(Graph_Processor_online, self).__init__()
        self.num_nodes = get_minimum_multiples(entity_size, n_heads)
        self.num_relations = get_minimum_multiples(relation_size, n_heads)
        self.entity_emb = get_param((entity_size, n_feature))
        self.relation_emb = get_param((relation_size, n_feature))

        self.conv_list = nn.ModuleList()
        for i in range(n_convs):
            self.conv_list.append(CompGCNConv(n_feature, n_feature, self.num_relations, opn='sub', dropout=dropout, bias=True, act=torch.tanh))
        self.att_ent = get_param((n_feature, n_output))
        self.att_rel = get_param((n_feature, n_output))
        self.w_ent = get_param((n_feature, n_output))
        self.w_rel = get_param((n_feature, n_output))
        self.dropout = torch.nn.Dropout(dropout)

    def forward(self, edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
        self.device = edge_index_seq[0].device
        seq_emb1, seq_emb2 = [], []
        for edge_index, edge_type, node_idx, edge_idx in zip(edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
            x_p, r_p = torch.zeros(self.num_nodes).to(self.device), torch.zeros(self.num_relations).to(self.device)
            if len(node_idx) > 0:
                for i, compconv in enumerate(self.conv_list):
                    if i == 0:
                        x, r = compconv(self.entity_emb, edge_index, edge_type, self.relation_emb)
                    else:
                        x, r = compconv(x, edge_index, edge_type, r)
                node_att, edge_att = torch.sigmoid(torch.matmul(x, self.att_ent)), torch.sigmoid(torch.matmul(r, self.att_rel))
                x, r = torch.matmul(x*node_att, self.w_ent), torch.matmul(r, self.w_rel)
                x_p[node_idx], r_p[edge_idx] = x.squeeze(1)[node_idx], r.squeeze(1)[edge_idx]
            seq_emb1.append(x_p), seq_emb2.append(r_p)
        seq_emb1, seq_emb2 = torch.stack(seq_emb1), torch.stack(seq_emb2)

        return self.dropout(seq_emb1), self.dropout(seq_emb2)


class Graph_Processor(nn.Module):
    def __init__(self, entity_emb, relation_emb, n_output, n_convs, n_heads, dropout):
        super(Graph_Processor, self).__init__()
        self.entity_emb = entity_emb
        self.relation_emb = relation_emb
        # self.entity_emb = get_param((entity_emb.size(0), 100))
        # self.relation_emb = get_param((relation_emb.size(0), 100))
        self.num_nodes = get_minimum_multiples(entity_emb.size(0), n_heads)
        self.num_relations = get_minimum_multiples(relation_emb.size(0), n_heads)
        self.n_input = self.entity_emb.size(1)

        self.conv_list = nn.ModuleList()
        for i in range(n_convs):
            self.conv_list.append(CompGCNConv(self.n_input, self.n_input, self.num_relations, opn='sub', dropout=dropout, bias=True, act=torch.tanh))
        self.att_ent = get_param((self.n_input, n_output))
        self.att_rel = get_param((self.n_input, n_output))
        self.w_ent = get_param((self.n_input, n_output))
        self.w_rel = get_param((self.n_input, n_output))
        self.dropout = torch.nn.Dropout(dropout)

    def forward(self, edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
        self.device = edge_index_seq[0].device
        seq_emb1, seq_emb2 = [], []
        for edge_index, edge_type, node_idx, edge_idx in zip(edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
            x_p, r_p = torch.zeros(self.num_nodes).to(self.device), torch.zeros(self.num_relations).to(self.device)
            if len(node_idx) > 0:
                for i, compconv in enumerate(self.conv_list):
                    if i == 0:
                        x, r = compconv(self.entity_emb, edge_index, edge_type, self.relation_emb)
                    else:
                        x, r = compconv(x, edge_index, edge_type, r)
                node_att, edge_att = torch.sigmoid(torch.matmul(x, self.att_ent)), torch.sigmoid(torch.matmul(r, self.att_rel))
                x, r = torch.matmul(x*node_att, self.w_ent), torch.matmul(r, self.w_rel)
                x_p[node_idx], r_p[edge_idx] = x.squeeze(1)[node_idx], r.squeeze(1)[edge_idx]
            seq_emb1.append(x_p), seq_emb2.append(r_p)
        seq_emb1, seq_emb2 = torch.stack(seq_emb1), torch.stack(seq_emb2)

        return self.dropout(seq_emb1), self.dropout(seq_emb2)


class Graph_Processor2(nn.Module):
    def __init__(self, entity_emb, relation_emb, n_output, n_convs, n_heads, dropout):
        super(Graph_Processor2, self).__init__()
        self.entity_emb = entity_emb
        self.relation_emb = relation_emb
        self.dropout = dropout

        self.num_nodes = entity_emb.size(0)
        self.num_relations = relation_emb.size(0)
        self.n_input = entity_emb.size(1)
        self.device = entity_emb.device

        self.conv_list = nn.ModuleList()
        for i in range(n_convs):
            self.conv_list.append(CompGCNConv(self.n_input, self.n_input, self.num_relations, opn='sub', dropout=self.dropout, bias=True, act=torch.tanh))
        # self.att_ent = get_param((self.n_input, n_output))
        # self.att_rel = get_param((self.n_input, n_output))
        self.dropout = torch.nn.Dropout(dropout)

    def forward(self, edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
        seq_feature1, seq_feature2 = [], []
        for edge_index, edge_type, node_idx in zip(edge_index_seq, edge_type_seq, node_idx_seq):
            x, r = torch.zeros(self.n_input).to(self.device), torch.zeros(self.n_input).to(self.device)
            if len(node_idx) > 0:
                for i, compconv in enumerate(self.conv_list):
                    if i == 0:
                        x, r = compconv(self.entity_emb, edge_index, edge_type, self.relation_emb)
                    else:
                        x, r = compconv(x, edge_index, edge_type, r)
                # node_att, edge_att = torch.softmax(torch.matmul(x, self.att_ent), dim=0), torch.softmax(torch.matmul(r, self.att_rel), dim=0)
                # x, r = torch.matmul(node_att.T, x).squeeze(0), torch.matmul(edge_att.T, r).squeeze(0)
                x, r = torch.mean(x, dim=0), torch.mean(r, dim=0)
            seq_feature1.append(x)
            seq_feature2.append(r)
        seq_emb1, seq_emb2 = torch.stack(seq_feature1), torch.stack(seq_feature2)

        return self.dropout(seq_emb1), self.dropout(seq_emb2)


class MultiHead_Attention(nn.Module):
    def __init__(self, input_dim, dim_k, dim_v, n_heads, dropout):
        super(MultiHead_Attention, self).__init__()
        self.n_heads = n_heads
        self.dim_k = dim_k
        self.dim_v = dim_v
        self._norm_fact = 1 / sqrt(dim_k)

        self.wq = nn.Linear(input_dim, dim_k)
        self.wk = nn.Linear(input_dim, dim_k)
        self.wv = nn.Linear(input_dim, dim_v)
        self.dropout = torch.nn.Dropout(dropout)

    def forward(self, x):
        Q = self.wq(x).reshape(-1, x.shape[0], self.dim_k // self.n_heads)
        K = self.wk(x).reshape(-1, x.shape[0], self.dim_k // self.n_heads)
        V = self.wv(x).reshape(-1, x.shape[0], self.dim_v // self.n_heads)
        atten = nn.Softmax(dim=-1)(torch.matmul(Q, K.permute(0, 2, 1)) * self._norm_fact)  # n_heads, len_seq, len_seq
        atten = self.dropout(atten)
        output = torch.matmul(atten, V).reshape(x.shape[0], x.shape[1])

        return output, atten


class FeedForward(nn.Module):
    def __init__(self, n_input, n_feedforward):
        super(FeedForward, self).__init__()
        self.ff = nn.Sequential(
            nn.Linear(n_input, n_feedforward, bias=False),
            nn.ReLU(),
            nn.Linear(n_feedforward, n_input, bias=False))

    def forward(self, input):
        return self.ff(input)


class Encoder_Layer(nn.Module):
    def __init__(self, n_feature, n_heads, n_feedforward, dropout):
        super(Encoder_Layer, self).__init__()
        self.mh_att = MultiHead_Attention(n_feature, n_feature, n_feature, n_heads, dropout)
        self.pe = get_positional_encoding(14, n_feature)
        self.ff = FeedForward(n_feature, n_feedforward)
        self.ln1 = nn.LayerNorm(n_feature)
        self.ln2 = nn.LayerNorm(n_feature)

    def forward(self, enc_input):
        self.device = enc_input.device
        enc_input = (enc_input + self.pe[:enc_input.size(0)].to(self.device)).type_as(enc_input)
        atten_output, atten = self.mh_att(enc_input)
        add_norm = self.ln1(atten_output + enc_input)
        enc_output = self.ln2(self.ff(add_norm) + add_norm)

        return enc_output, atten


class Predictor(nn.Module):
    def __init__(self, num_nodes, num_relations, n_feature, n_output, dropout):
        super(Predictor, self).__init__()
        self.num_nodes = num_nodes
        self.num_relations = num_relations
        self.seq_w = get_param((n_feature, n_output))
        self.weight = get_param((n_feature, n_output))
        self.dropout = torch.nn.Dropout(dropout)

    def forward(self, enc_output):
        seq_att = torch.softmax(torch.matmul(enc_output, self.seq_w), dim=0).T
        seq_emb = torch.mm(seq_att, enc_output)
        logit = torch.matmul(seq_emb, self.weight).squeeze(0)
        pred = torch.sigmoid(logit)

        # indicators = seq_att.T*enc_output*self.weight.T
        # indicators_actors = torch.sum(indicators, dim=0)
        # indicators_dates = torch.sum(indicators, dim=1)

        return pred

    def explain(self, enc_output, node_idx_seq, edge_idx_seq):
        seq_att = torch.softmax(torch.matmul(enc_output, self.seq_w), dim=0).T
        seq_emb = torch.mm(seq_att, enc_output)

        node_edge_idx = []
        for node_idx, edge_idx in zip(node_idx_seq, edge_idx_seq):
            for node in node_idx.tolist():
                node_edge_idx.append(node)
            for edge in edge_idx.tolist():
                node_edge_idx.append(edge+self.num_nodes)
        node_edge_idx = torch.tensor(process_list(node_edge_idx))
        actor_contribution = torch.zeros(self.num_nodes+self.num_relations)
        temp = (seq_emb*self.weight.T).squeeze(0)
        actor_contribution[node_edge_idx] = temp[node_edge_idx]

        # day_contribution = (torch.mm(enc_output, self.weight)*seq_att.T).squeeze(1).tolist()
        actor_contribution = actor_contribution.tolist()

        return actor_contribution


class Multi_Predictor(nn.Module):
    def __init__(self, num_nodes, num_relations, n_feature, n_output, dropout):
        super(Multi_Predictor, self).__init__()
        self.num_nodes = num_nodes
        self.num_relations = num_relations
        self.weight = get_param((n_feature, 1))
        self.dropout = torch.nn.Dropout(dropout)

    def forward(self, enc_output):
        logit = torch.matmul(enc_output, self.weight).squeeze(0)
        pred = torch.sigmoid(logit).squeeze(1)

        return pred