from __future__ import division
from __future__ import print_function

import argparse
import os,json
import time
from datetime import datetime


# # DEBUG
# import debugpy
# # Allow other computers to attach to debugpy at this IP address and port.
# debugpy.listen(('172.20.201.133', 5678))
# # Pause the program until a remote debugger is attached
# debugpy.wait_for_client()

parser = argparse.ArgumentParser()
parser.add_argument('--gpu', type=int, default=1)
parser.add_argument('--seed', type=int, default=42)
parser.add_argument('--n_feature', type=int, default=100)
parser.add_argument('--n_convs', type=int, default=1, choices=[1, 2, 3])
parser.add_argument('--n_encoders', type=int, default=1, choices=[1, 2, 3, 6])
parser.add_argument('--seq_len', type=int, default=7)
parser.add_argument('--n_heads', type=int, default=8, choices=[1, 2, 4, 8, 16])
parser.add_argument('--n_feedforward', type=int, default=64)
parser.add_argument('--batch_size', type=int, default=32)
parser.add_argument('--dropout', type=float, default=0.5)
parser.add_argument('--n_output', type=int, default=1)
parser.add_argument('--lr', type=float, default=5e-4, choices=[1e-3, 5e-4, 1e-4])
parser.add_argument('--weight_decay', type=float, default=5e-3, choices=[5e-3, 5e-4])
parser.add_argument("--chs_root", action="store_true", help="choose using IsRootEvent")
parser.add_argument("-d", "--dataset", type=str, help="dataset to use")
parser.add_argument("--date_period",default="2018-2022", type=str, help="dataset to use")
parser.add_argument("--train", type=float, default=0.6, help='training dataset rate')
parser.add_argument("--val", type=float, default=0.2, help='validating dataset rate')
parser.add_argument("-w","--weight_loss", action="store_false", help='')
parser.add_argument("-pa","--patience", type=int, default=20)
parser.add_argument("-s","--shuffle", action="store_false", help='')
parser.add_argument("-l","--loop", type=int, default=10, help='')
parser.add_argument("--epochs", type=int, default=20, help="maximum epochs")
parser.add_argument("-m","--model", type=str, default='HAFL', help='')
parser.add_argument("--cri", type=str, default='f1',choices=['f1', 'loss'], help='validating criteria')
args = parser.parse_args()


os.environ["CUDA_VISIBLE_DEVICES"] = str(args.gpu)
from models import *
use_cuda = torch.cuda.is_available()
import torch.optim as optim
from sklearn.metrics import accuracy_score, recall_score
from sklearn.metrics import precision_recall_fscore_support
import logging

from split_online_data import *

logging.basicConfig(level=logging.INFO)

def evaluate(mode = 'val'):
    model.eval()
    y_true, y_pred = [], []
    # DEBUG for checking begin_index and end_index
    if mode == 'val':
        begin_index = int(len(y_list)*args.train)
        end_index = int(len(y_list)*(args.train + args.val))
    elif mode == 'test':
        begin_index = int(len(y_list)*(args.train + args.val))
        end_index = int(len(y_list))
    else:
        raise ValueError("mode value must be val or test")

    for i in range(begin_index, end_index):
        output = model(edge_index_data[i], edge_type_data[i],
                       node_idx_data[i], edge_idx_data[i])
        y_true += y_list[i].data.tolist()
        bi_val = np.where(output.data.cpu().numpy() > 0.5, 1, 0)
        y_pred += torch.from_numpy(bi_val).tolist()
    
    y_pred = torch.FloatTensor(y_pred)
    y_true = torch.FloatTensor(y_true)
    prec, rec, f1, _ = precision_recall_fscore_support(y_true, y_pred, average="binary")
    acc = accuracy_score(y_true, y_pred)
    neg_rec =  recall_score(y_true, y_pred, pos_label=0)
    if args.weight_loss:
        weight = torch.FloatTensor([1/(2*class_weights[1-int(i)]) for i in y_true])
        val_loss = F.binary_cross_entropy(y_pred, y_true, weight)
    else:
        val_loss = F.binary_cross_entropy(y_pred, y_true)
    return acc, prec, rec, f1, neg_rec, val_loss


def train(epoch, batch_size):
    count = 0
    model.train()
    iteration = int(len(y_list) * args.train) // batch_size
    # t1 = time.time()
    for it in tqdm(range(iteration), desc='train iteration'):
        loss_train = 0
        optimizer.zero_grad()
        for i in range(it * batch_size, (it + 1) * batch_size):
            output = model(edge_index_data[i], edge_type_data[i],
                           node_idx_data[i], edge_idx_data[i])
            # has modified.
            # DEBUG: comparing the training results between modified loos setting and wang's setting
            if args.weight_loss:
                loss_train += F.binary_cross_entropy(output, y_list[i], weight=1/(2*class_weights[1-int(y_list[i].item())]))
                # Wang Yensin's original code, 
                # loss_train += F.binary_cross_entropy(output, y_list[i], weight=class_weights[int(y_list[i].item())])
            else:
                loss_train += F.binary_cross_entropy(output, y_list[i])

            count += 1
        loss_train.backward()
        optimizer.step()
        # loss += loss_train
    acc, prec, rec, f1, neg_rec, val_loss = evaluate(mode='val')
    # print('Epoch: {:03d}'.format(epoch + 1),
    #       'time: {:.4f}s'.format(time.time() - t1),
    #       'loss_train: {:.4f}'.format(loss / count),
    #       'precision_test: {:.4f}'.format(prec),
    #       'recall_test: {:.4f}'.format(rec),
    #       'f1_test: {:.4f}'.format(f1))

    return acc, prec, rec, f1, neg_rec, val_loss


def sampling(y_list):
    pos_count, neg_count = 0, 0
    for y in y_list:
        if y.item() > 0.:
            pos_count += 1
        else:
            neg_count += 1
    return pos_count, neg_count


def load_result(filename):
    try:
        with open('online/results/{}.pkl'.format(filename), 'rb') as f:
            result_list = pkl.load(f)
        acc = [result[0] for result in result_list]
        prec = [result[1] for result in result_list]
        rec = [result[2] for result in result_list]
        f1 = [result[3] for result in result_list]
        print(filename, [round(np.average(acc), 3), round(np.std(acc), 3)], [round(np.average(prec), 3), round(np.std(prec), 3)], [round(np.average(rec), 3), round(np.std(rec), 3)], [round(np.average(f1), 3), round(np.std(f1), 3)], len(result_list))
    except Exception:
        print(filename, 'file not found')


# data_dir = "/nfs/home/fzx/project/EPredict2023/data"
data_dir = "/data/fuzexin/project/event_pred_research2024/data/south_east_asia"
# deposit_train_data = '/nfs/home/fzx/project/EPredict2023/data'
deposit_train_data = "/data/fuzexin/project/event_pred_research2024/code/HAFL/data"

data_field = ['EventID', 'Actor1Name', 'EventCode', 'Actor2Name', 'AllNames', 'NumArticles', 'IsRootEvent', 'EventDate']

# output parameters
train_time = datetime.strftime(datetime.now(), "%Y%m%d-%H%M")
token = "HAFL"+train_time
train_info = {}
train_info['train time'] = train_time
train_info['dataset name'] = args.dataset
train_info['date_period'] = args.date_period
train_info['training device'] = torch.cuda.get_device_name(0) if use_cuda else "cpu"
train_info['train rate'] = args.train
train_info['validate rate'] = args.val
train_info['test rate'] = 1 - args.train - args.val

train_info['sequence length'] = args.seq_len
train_info['weight loss'] = args.weight_loss
train_info['batch size'] = args.batch_size
train_info['patience'] = args.patience
train_info['shuffle'] = args.shuffle
train_info['loop number'] = args.loop
train_info['epochs'] = args.epochs
train_info['use IsRootEvent'] = args.chs_root
train_info['model name'] = args.model

country = args.dataset 
result_list = []
for i in range(args.loop):
    # ---handle data
    print(args)
    # conjecture edge_index_data is (enode1, enode2),  edge_type_data is 141 etc., node_idx_data is 1, the entity index in all entity list, y_list is 0 or 1. 
    deposit_dir = '{0}/{1}'.format(deposit_train_data, country+'_'+ args.date_period)
    if not os.path.exists(deposit_dir):
        os.mkdir(deposit_dir)
    entity_size, relation_size, edge_index_data, edge_type_data, node_idx_data, y_list, recall_info = preprocess(country, args.date_period, args.seq_len, data_dir, deposit_dir, data_field, args.chs_root)
    # aquire output info
    train_info.update(recall_info)
    # ---- DEBUG
    # for i in range(len(edge_index_data)):
    #     if len(edge_index_data[i])==0:
    #         print(i)
    if args.shuffle:
        edge_index_data, edge_type_data, node_idx_data, y_list = shuffle(args.seed, edge_index_data, edge_type_data, node_idx_data, y_list)
    # deduplicate edge_type_data
    edge_idx_data = get_edge_idx_data(edge_type_data)
    print(sampling(y_list))

    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if use_cuda:
        print('GPU: {} {}'.format(torch.cuda.get_device_name(), args.gpu))
        torch.cuda.manual_seed(args.seed)
        for sample in range(len(y_list)):
            y_list[sample] = y_list[sample].cuda()
            for day in range(args.seq_len):
                try:
                    edge_index_data[sample][day] = edge_index_data[sample][day].cuda()
                    edge_type_data[sample][day] = edge_type_data[sample][day].cuda()
                    node_idx_data[sample][day] = node_idx_data[sample][day].cuda()
                    edge_idx_data[sample][day] = edge_idx_data[sample][day].cuda()
                except Exception:
                    pass

    # train model
    model = TAGS_online(entity_size=entity_size,
                        relation_size=relation_size,
                        n_feature=args.n_feature,
                        n_convs=args.n_convs,
                        n_encoders=args.n_encoders,
                        n_heads=args.n_heads,
                        n_feedforward=args.n_feedforward,
                        n_output=args.n_output,
                        dropout=args.dropout)
    optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
    parameters_num = sum(p.numel() for p in model.parameters() if p.requires_grad)
    train_info['model total parameters'] = parameters_num
    pos, neg = count(y_list)
    class_weights = torch.FloatTensor([pos/(pos+neg), neg/(pos+neg)])
    train_info['class weight'] = class_weights.tolist()
    if use_cuda:
        model.cuda()
        class_weights = class_weights.cuda()

    print('Location/Samples: {}/{}'.format(country, len(y_list)))
    print('Model Parameters: {:.4f}M'.format(parameters_num / 1000000.0))
    if not os.path.exists('model/{}/'.format(country)):
        os.makedirs('model/{}/'.format(country))
    model_state_file = 'model/{}/{}_{}.pth'.format(country, token, i)
    print("----------------Training-----------------")
    best_loss, max_epoch, bad_count = float('inf'), 0, 0
    t1 = time.time()
    for epoch in range(args.epochs):
        acc, prec, rec, f1, neg_rec, val_loss = train(epoch, args.batch_size)
        if args.cri == 'f1':
            eval_metric = 1-f1
        else:
            eval_metric = val_loss
        if eval_metric < best_loss:
            best_loss, max_epoch, bad_count = eval_metric, epoch, 0
            # indicators = [acc, prec, rec, f1, neg_rec]

            torch.save(model.state_dict(), model_state_file)
        else:
            bad_count += 1
        if (epoch + 1) % 10 == 0 and epoch > 0:
            print('Best epoch: {:03d}'.format(max_epoch + 1),
                    'Best loss: {:.4f}'.format(best_loss))
        if bad_count >= args.patience:
            print("------------Early Stop------------")
            break
    print("Training time: {:.4f}s".format(time.time()-t1))
    print("Best epoch: {:03d}".format(max_epoch + 1),
            "Best loss: {:.4f}".format(best_loss))
    # test
    # DEBUG: check model_state load successful.
    model_state = torch.load(model_state_file, map_location=lambda storage, location: storage)
    model.load_state_dict(model_state)
    acc, prec, rec, f1, neg_rec, _ = evaluate(mode='test')
    test_info = {
        'acc':acc, 'prec':prec, 'recall':rec, 'f1':f1, 'neg-recall':neg_rec 
    }
    train_info['loop{} result'.format(i)] = test_info

result_file = deposit_dir +'/{}.json'.format(token)
print(train_info)
f = open(result_file, 'w')
json.dump(train_info, f, indent=1)
f.close()