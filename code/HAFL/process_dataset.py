from utils import *
from pytorch_transformers import BertTokenizer, BertModel


def get_actor_event(location):
    actor_list, eventcode_list, event_list = [], [], []
    file_list = find_file('sequence/{}/'.format(location), '.pkl')
    for file in file_list:
        with open('sequence/{}/{}'.format(location, file), 'rb') as f:
            seq = pkl.load(f)
        for day in seq:
            for event in day:
                actor_list.append(event[1])
                eventcode_list.append(event[2])
                actor_list.append(event[3])
    event_mapping = Get_EventMapping()
    for eventcode in process_list(eventcode_list):
        event_list.append(event_mapping[eventcode])
    return process_list(actor_list), process_list(eventcode_list), event_list


def get_word_vec(word):
    input_ids = torch.tensor(tokenizer.encode(word)).unsqueeze(0)  # Batch size 1
    outputs = model(input_ids)
    return outputs[1]


def process_feature(word_list):
    feature_tensor = torch.zeros([len(word_list), 768])
    for i, tensor in enumerate(feature_tensor):
        try:
            feature_tensor[i] = torch.tensor(get_word_vec(word_list[i]).detach().numpy())
        except Exception:
            feature_tensor[i] = torch.zeros([1, 768])
    return feature_tensor


def get_node_idx(edge_index):
    idx = []
    for node_list in edge_index:
        for node in node_list:
            idx.append(node.item())
    return torch.tensor(process_list(idx))


def get_edge_idx(edge_type):
    idx = []
    for edge in edge_type:
        idx.append(edge.item())
    return torch.tensor(process_list(idx))


def get_relational_graph(location, actor_list, event_list, eventcode_list):
    edge_index_data, edge_type_data, node_idx_data, edge_idx_data, y_list, pos_count = [], [], [], [], [], 0
    file_list = find_file('sequence/{}/'.format(location), '.pkl')
    for i, file in enumerate(file_list):
        with open('sequence/{}/{}'.format(location, file), 'rb') as f:
            seq = pkl.load(f)
        seq_edge_index, seq_edge_type, seq_node_idx, seq_edge_idx = [], [], [], []
        for j, day in enumerate(seq):
            edge_index, edge_type = [], []
            for event in day:
                if (actor_list.index(event[1]), actor_list.index(event[3])) not in edge_index:
                    edge_index.append((actor_list.index(event[1]), actor_list.index(event[3])))
                    edge_type.append(eventcode_list.index(event[2]))
            edge_index = torch.LongTensor(edge_index).t()
            edge_type = torch.LongTensor(edge_type)
            seq_edge_index.append(edge_index)
            seq_edge_type.append(edge_type)
            seq_node_idx.append(get_node_idx(edge_index))
            seq_edge_idx.append(get_edge_idx(edge_type))
        edge_index_data.append(seq_edge_index)
        edge_type_data.append(seq_edge_type)
        node_idx_data.append(seq_node_idx)
        edge_idx_data.append(seq_edge_idx)
        if 'pos' in file:
            y_list.append(torch.tensor([1.]))
            pos_count += 1
        else:
            y_list.append(torch.tensor([0.]))
        print('sample {} processed'.format(i+1))
    print('processing initial embeddings......')
    ent_emb = process_feature(actor_list)
    rel_emb = process_feature(event_list)
    print('location: {} pos/neg: {}/{} entity size: ({},{}) relation size: ({},{})'.format(location, pos_count, len(file_list)-pos_count, ent_emb.size(0), ent_emb.size(1), rel_emb.size(0), rel_emb.size(1)))
    if not os.path.exists('data/{}'.format(location)):
        os.makedirs('data/{}'.format(location))
    with open('data/{}/{}.pkl'.format(location, location), 'wb') as f:
        pkl.dump((ent_emb, rel_emb, edge_index_data, edge_type_data, node_idx_data, y_list), f)
    return ent_emb, rel_emb, edge_index_data, edge_type_data, node_idx_data, edge_idx_data, y_list


if __name__ == "__main__":
    tokenizer = BertTokenizer.from_pretrained('../research2/data/bert_models/cased_L-12_H-768_A-12')
    model = BertModel.from_pretrained('../research2/data/bert_models/cased_L-12_H-768_A-12')

    location_list = ['test']
    for location in location_list:
        actor_list, eventcode_list, event_list = get_actor_event(location)
        ent_emb, rel_emb, edge_index_data, edge_type_data, node_idx_data, edge_idx_data, y_list = get_relational_graph(location, actor_list, event_list, eventcode_list)