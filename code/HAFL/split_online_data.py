from utils import *
from tqdm import tqdm
from pytorch_transformers import BertTokenizer, BertModel
import logging, os

# def get_word_vec(word):
#     input_ids = torch.tensor(tokenizer.encode(word)).unsqueeze(0)  # Batch size 1
#     outputs = model(input_ids)
#     return outputs[1]


# def process_feature(word_list):
#     feature_tensor = torch.zeros([len(word_list), 768])
#     for i, tensor in enumerate(feature_tensor):
#         try:
#             feature_tensor[i] = torch.tensor(get_word_vec(word_list[i]).detach().numpy())
#         except Exception:
#             feature_tensor[i] = torch.zeros([1, 768])
#     return feature_tensor


def process_original_data(country, year, data_dir, depos_dir, data_field):
    """ read data file and store it in [[pos1], [pos2], ...], pos1 = [[day0's data], [day1's data], ...] """
    data_path = os.path.join(depos_dir, "all_data.pkl")
    if os.path.exists(data_path):
        with open(data_path,'rb') as f:
            all_data, recall_info = pkl.load(f)
    else:    
        event_date_index = data_field.index("EventDate")
        all_data = []
        file_list = find_file('{}/{}/{}/'.format(data_dir, country, year), '.pkl')
        # recall info for results output
        recall_info = {}
        recall_info['loc_list'] = [x[:-4] for x in file_list]
        # record dataset's sample quantity
        sample_quantity = 0
        for file in file_list:
            data, date_list = [], []
            with open('{}/{}/{}/{}'.format(data_dir, country, year, file), 'rb') as f:
                orginal_data = pkl.load(f)
            for event in orginal_data:
                date_list.append(event[event_date_index])
            date_list = process_list(date_list)
            for date in date_list:
                day_list = []
                for event in orginal_data:
                    if event[event_date_index] == date:
                        day_list.append(event)
                data.append(day_list)
                # check if data length is increase
                if len(data) % 10 == 0:
                    logging.debug(len(data))
            sample_quantity += len(data)
            all_data.append(data)
        recall_info['sample quantity'] = sample_quantity
        with open(data_path,'wb') as f:
            pkl.dump((all_data, recall_info), f)
    return all_data, recall_info

def get_actor_event(data, data_field):
    """ get actor name, event code and event description text. """
    # DEBUG: check if indexes are correct
    event_code_index = data_field.index( 'EventCode' )
    actor1_index = data_field.index( 'Actor1Name' )
    actor2_index =  data_field.index( 'Actor2Name' )
    actor_list, eventcode_list, event_list = [], [], []
    event_mapping = Get_EventMapping()
    for idx, day_list in enumerate(data):
        for event in day_list:
            if event[event_code_index] != '---':
                actor_list.append(event[actor1_index])
                actor_list.append(event[actor2_index])
                eventcode_list.append(event[event_code_index])
    eventcode_list = process_list(eventcode_list)
    for eventcode in eventcode_list:
        event_list.append(event_mapping[eventcode])
    return process_list(actor_list), eventcode_list, event_list


def get_labels(data, len_seq, data_field, chs_root):
    event_code_index = data_field.index("EventCode")
    # DEBUG: check
    is_root_protest_index = data_field.index('IsRootEvent')
    label_list = []
    for idx, day_list in enumerate(data):
        if idx >= len_seq:
            protest_count = 0
            root_protest_count = 0
            for event in day_list:
                if event[event_code_index].startswith('14'):
                    protest_count += 1
                    if event[is_root_protest_index] == 1:
                        root_protest_count += 1
            if chs_root == False:    
                if protest_count > 0:
                    label_list.append(torch.tensor([1.]))
                else:
                    label_list.append(torch.tensor([0.]))
            else:
                if root_protest_count > 0:
                    label_list.append(torch.tensor([1.]))
                else:
                    label_list.append(torch.tensor([0.]))

    return label_list


def get_node_idx(edge_index):
    idx = []
    for node_list in edge_index:
        for node in node_list:
            idx.append(node.item())
    return torch.tensor(process_list(idx))


def get_edge_idx(edge_type):
    idx = []
    for edge in edge_type:
        idx.append(edge.item())
    return torch.tensor(process_list(idx))


def get_graphs(data, actor_list, eventcode_list, label_list, len_seq, data_field):
    # edge_index_data is list of many len_seq length day edge_indexs [e0, e1, ..., ], one is like (1, 2) , 
    # edge_type_data ie like up,  one edge is like 141, 
    # node_idx_data is also like up, one is like 1.   

    # DEBUG: check
    actor1_index = data_field.index("Actor1Name")
    actor2_index = data_field.index("Actor2Name")
    event_code_index = data_field.index("EventCode")

    edge_index_data, edge_type_data, node_idx_data = [], [], []
    edge_indexs, edge_types, node_idxs = [], [], []
    for i in tqdm(range(len(data)), desc='processing graphs......'):
        edge_index, edge_type = [], []
        for event in data[i]:
            if (actor_list.index(event[actor1_index]), actor_list.index(event[actor2_index])) not in edge_index and event[event_code_index] != '---':
                edge_index.append((actor_list.index(event[actor1_index]), actor_list.index(event[actor2_index])))
                edge_type.append(eventcode_list.index(event[event_code_index]))
        edge_index = torch.LongTensor(edge_index).t()
        edge_type = torch.LongTensor(edge_type)
        node_idx = get_node_idx(edge_index)
        edge_indexs.append(edge_index)
        edge_types.append(edge_type)
        node_idxs.append(node_idx)
    for i in range(len(label_list)):
        edge_index_data.append(edge_indexs[i:i + len_seq])
        edge_type_data.append(edge_types[i:i + len_seq])
        node_idx_data.append(node_idxs[i:i + len_seq])

    return len(actor_list), len(eventcode_list), edge_index_data, edge_type_data, node_idx_data


def preprocess_dataset(location, len_seq):
    with open('data/online/{}_count.pkl'.format(location), 'rb') as f:
        count_date = pkl.load(f)
    with open('data/online/{}_events.pkl'.format(location), 'rb') as f:
        events = pkl.load(f)
    avg_count = round(np.mean(count_date[0]), 0)
    label_list, pos_count = [], 0
    for count, date in zip(count_date[0][len_seq:], count_date[1][len_seq:]):
        if count > avg_count:
            label_list.append(torch.tensor([1.]))
        else:
            label_list.append(torch.tensor([0.]))
    actor_list, eventcode_list, event_list = get_actor_event(events)
    entity_size, relation_size, edge_index_data, edge_type_data, node_idx_data = get_graphs(events, actor_list, eventcode_list, label_list, len_seq)
    return entity_size, relation_size, edge_index_data, edge_type_data, node_idx_data, label_list


def preprocess_dataset2(location, len_seq):
    if not os.path.exists('data/online/{}_tags_{}.pkl'.format(location, len_seq)):
        with open('data/online/{}_count.pkl'.format(location), 'rb') as f:
            count_date = pkl.load(f)
        with open('data/online/{}_events.pkl'.format(location), 'rb') as f:
            events = pkl.load(f)
        avg_count = round(np.mean(count_date[0]), 0)
        label_list, pos_count = [], 0
        for count, date in zip(count_date[0][len_seq:], count_date[1][len_seq:]):
            if count > avg_count:
                label_list.append(torch.tensor([1.]))
            else:
                label_list.append(torch.tensor([0.]))
        actor_list, eventcode_list, event_list = get_actor_event(events)
        edge_index_data, edge_type_data, node_idx_data = [], [], []
        for i in tqdm(range(len(label_list)), desc='processing seqs......'):
            seq = events[i:i+len_seq]
            seq_edge_index, seq_edge_type, seq_node_idx = [], [], []
            for j, day in enumerate(seq):
                edge_index, edge_type = [], []
                for event in day:
                    if (actor_list.index(event[1]), actor_list.index(event[3])) not in edge_index and event[2] != '---':
                        edge_index.append((actor_list.index(event[1]), actor_list.index(event[3])))
                        edge_type.append(eventcode_list.index(event[2]))
                edge_index = torch.LongTensor(edge_index).t()
                edge_type = torch.LongTensor(edge_type)
                seq_edge_index.append(edge_index)
                seq_edge_type.append(edge_type)
                seq_node_idx.append(get_node_idx(edge_index))
            edge_index_data.append(seq_edge_index)
            edge_type_data.append(seq_edge_type)
            node_idx_data.append(seq_node_idx)
        entity_size, relation_size = len(actor_list), len(eventcode_list)
        with open('data/online/{}_tags_{}.pkl'.format(location, len_seq), 'wb') as f:
            pkl.dump((entity_size, relation_size, edge_index_data, edge_type_data, node_idx_data, label_list), f)
    else:
        with open('data/online/{}_tags_{}.pkl'.format(location, len_seq), 'rb') as f:
            entity_size, relation_size, edge_index_data, edge_type_data, node_idx_data, label_list = pkl.load(f)
    return entity_size, relation_size, edge_index_data, edge_type_data, node_idx_data, label_list


def preprocess(country, year, len_seq, data_dir, deposit_dir, data_field, chs_root):
    all_data, recall_info = process_original_data(country, year, data_dir, deposit_dir, data_field)
    all_actor_list, all_eventcode_list, all_event_list, all_label_list = [], [], [], []
    for data in all_data:
        actor_list, eventcode_list, event_list = get_actor_event(data, data_field)
        label_list = get_labels(data, len_seq, data_field, chs_root)
        all_actor_list += actor_list
        all_eventcode_list += eventcode_list
        all_event_list += event_list
        all_label_list += label_list
    all_actor_list, all_eventcode_list, all_event_list = process_list(all_actor_list), process_list(all_eventcode_list), process_list(all_event_list)
    all_edge_index_data, all_edge_type_data, all_node_idx_data = [], [], []
    for data in all_data:
        label_list = get_labels(data, len_seq, data_field, chs_root)
        entity_size, relation_size, edge_index_data, edge_type_data, node_idx_data = get_graphs(data, all_actor_list, all_eventcode_list, label_list, len_seq, data_field)
        all_edge_index_data += edge_index_data
        all_edge_type_data += edge_type_data
        all_node_idx_data += node_idx_data
    
    # DEBUG: finished FLAG

    with open('{}/actor_event.pkl'.format(deposit_dir), 'wb') as f:
        pkl.dump([all_actor_list, all_eventcode_list], f)
    return len(all_actor_list), len(all_event_list), all_edge_index_data, all_edge_type_data, all_node_idx_data, all_label_list, recall_info


def preprocess_dataset_undirected(location, seq_len, lead_day):
    if not os.path.exists('data/online/{}_dgcn_{}.pkl'.format(location, seq_len)):
        with open('data/online/{}_count.pkl'.format(location), 'rb') as f:
            count_date = pkl.load(f)
        avg_count = round(np.mean(count_date[0]), 0)
        label_list, pos_count = [], 0
        for count, date in zip(count_date[0][seq_len:], count_date[1][seq_len:]):
            if count > avg_count:
                label_list.append(torch.tensor([1.]))
                pos_count += 1
            else:
                label_list.append(torch.tensor([0.]))
        with open('data/online/{}_events.pkl'.format(location), 'rb') as f:
            events = pkl.load(f)
        actor_list, eventcode_list, event_list = get_actor_event(events)
        adj_list, vertice_list = [], []
        for i in tqdm(range(len(label_list)), desc='processing graphs......'):
            seq = events[i:i+seq_len]
            adjs, node_list, edge_list = [], [], []
            for j, day in enumerate(seq):
                for event in day:
                    node_list.append(actor_list.index(event[1]))
                    node_list.append(actor_list.index(event[3]))
            node_list = process_list(node_list)
            idx_map = {j: i for i, j in enumerate(node_list)}
            for day in seq:
                for event in day:
                    edge_list.append([actor_list.index(event[1]), actor_list.index(event[3])])
                edges = np.array(edge_list)
                edges = np.array(list(map(idx_map.get, edges.flatten())), dtype=np.int32).reshape(edges.shape)
                try:
                    adj = sp.coo_matrix((np.ones(edges.shape[0]), (edges[:, 0], edges[:, 1])),
                                        shape=(len(node_list), len(node_list)), dtype=np.float32)
                    adj = adj + adj.T.multiply(adj.T > adj) - adj.multiply(adj.T > adj)
                    adj = normalize(adj + sp.eye(adj.shape[0]))
                    adj = sparse_mx_to_torch_sparse_tensor(adj)
                    adjs.append(adj)
                except Exception:
                    adjs.append(torch.tensor([0]))
            adj_list.append(adjs)
            vertice_list.append(torch.tensor(node_list))
        with open('data/online/{}_dgcn_{}.pkl'.format(location, seq_len), 'wb') as f:
            pkl.dump((len(actor_list), len(event_list), adj_list, vertice_list, label_list), f)
        entity_size, relation_size = len(actor_list), len(event_list)
    else:
        with open('data/online/{}_dgcn_frequentevents_{}.pkl'.format(location, seq_len), 'rb') as f:
            entity_size, relation_size, adj_list, vertice_list, label_list = pkl.load(f)
    return entity_size, relation_size, adj_list, vertice_list, label_list


# def preprocess_dataset_undirected(location, len_seq, lead_day):
#     if not os.path.exists('data/online/{}_dgcn_unsteady_{}.pkl'.format(location, len_seq)):
#         with open('data/online/{}_count.pkl'.format(location), 'rb') as f:
#             count_date = pkl.load(f)
#         avg_count = round(np.mean(count_date[0]), 0)
#         label_list, pos_count = [], 0
#         for count, date in zip(count_date[0][len_seq:], count_date[1][len_seq:]):
#             if count > avg_count:
#                 label_list.append(torch.tensor([1.]))
#                 pos_count += 1
#             else:
#                 label_list.append(torch.tensor([0.]))
#         with open('data/online/{}_events.pkl'.format(location), 'rb') as f:
#             events = pkl.load(f)
#         actor_list, eventcode_list, event_list = get_actor_event(events)
#         adj_list, vertice_list = [], []
#         for i in tqdm(range(len(label_list)), desc='processing graphs......'):
#             seq = events[i:i+len_seq]
#             cut_len = random.randint(0,7)
#             for idx in range(cut_len):
#                 seq[idx] = []
#             adjs, node_list, edge_list = [], [], []
#             for j, day in enumerate(seq):
#                 for event in day:
#                     node_list.append(actor_list.index(event[1]))
#                     node_list.append(actor_list.index(event[3]))
#             node_list = process_list(node_list)
#             idx_map = {j: i for i, j in enumerate(node_list)}
#             for day in seq:
#                 for event in day:
#                     edge_list.append([actor_list.index(event[1]), actor_list.index(event[3])])
#                 edges = np.array(edge_list)
#                 edges = np.array(list(map(idx_map.get, edges.flatten())), dtype=np.int32).reshape(edges.shape)
#                 try:
#                     adj = sp.coo_matrix((np.ones(edges.shape[0]), (edges[:, 0], edges[:, 1])),
#                                         shape=(len(node_list), len(node_list)), dtype=np.float32)
#                     adj = adj + adj.T.multiply(adj.T > adj) - adj.multiply(adj.T > adj)
#                     adj = normalize(adj + sp.eye(adj.shape[0]))
#                     adj = sparse_mx_to_torch_sparse_tensor(adj)
#                     adjs.append(adj)
#                 except Exception:
#                     adjs.append(torch.tensor([0]))
#             adj_list.append(adjs)
#             vertice_list.append(torch.tensor(node_list))
#         with open('data/online/{}_dgcn_unsteady_{}.pkl'.format(location, len_seq), 'wb') as f:
#             pkl.dump((len(actor_list), len(event_list), adj_list, vertice_list, label_list), f)
#         entity_size, relation_size = len(actor_list), len(event_list)
#     else:
#         with open('data/online/{}_dgcn_unsteady_{}.pkl'.format(location, len_seq), 'rb') as f:
#             entity_size, relation_size, adj_list, vertice_list, label_list = pkl.load(f)
#     return entity_size, relation_size, adj_list, vertice_list, label_list