from __future__ import division
from __future__ import print_function

import argparse
import os
import time
import pickle as pkl
import matplotlib.pyplot as plt

with open('data/online/New York_count.pkl', 'rb') as f:
    count, date = pkl.load(f)

# def predict_sample(idx):
#     model.eval()
#     output = model(edge_index_data[idx][14 - args.len_seq:], edge_type_data[idx][14 - args.len_seq:],
#                    node_idx_data[idx][14 - args.len_seq:], edge_idx_data[idx][14 - args.len_seq:])
#     return round(output.item(), 2), y_list[idx].item()
#
#
# parser = argparse.ArgumentParser()
# parser.add_argument('--location', type=str, default='USW', choices=['USW', 'USE', 'IND', 'UK'])
# parser.add_argument('--datatype', type=str, default='correlation', choices=['correlation'])
# parser.add_argument('--gpu_idx', type=int, default=0)
# parser.add_argument('--seed', type=int, default=42)
# parser.add_argument('--n_convs', type=int, default=1, choices=[1, 2])
# parser.add_argument('--n_encoders', type=int, default=1, choices=[1, 2, 3, 6])
# parser.add_argument('--len_seq', type=int, default=14)
# parser.add_argument('--n_heads', type=int, default=8)
# parser.add_argument('--n_feedforward', type=int, default=64)
# parser.add_argument('--batch_size', type=int, default=8)
# parser.add_argument('--epochs', type=int, default=50)
# parser.add_argument('--dropout', type=float, default=0.5)
# parser.add_argument('--n_output', type=int, default=1)
# parser.add_argument('--lr', type=float, default=5e-4, choices=[1e-3, 5e-4, 1e-4])
# parser.add_argument('--weight_decay', type=float, default=5e-3, choices=[5e-3, 5e-4])
# args = parser.parse_args()
#
# os.environ["CUDA_VISIBLE_DEVICES"] = str(args.gpu_idx)
# from models import *
# args.cuda = torch.cuda.is_available()
#
# entity_emb, relation_emb, edge_index_data, edge_type_data, node_idx_data, y_list = load_data(args.location, args.datatype)
# edge_idx_data = get_edge_idx_data(edge_type_data)
# model_list = find_file('models', '.pkl')
# print(model_list)
#
# np.random.seed(args.seed)
# torch.manual_seed(args.seed)
# if args.cuda:
#     torch.cuda.manual_seed(args.seed)
#     print('GPU: {} {}'.format(torch.cuda.get_device_name(), args.gpu_idx))
#     entity_emb, relation_emb = entity_emb.cuda(), relation_emb.cuda()
#     for sample in range(len(y_list)):
#         y_list[sample] = y_list[sample].cuda()
#         for day in range(14):
#             edge_index_data[sample][day] = edge_index_data[sample][day].cuda()
#             edge_type_data[sample][day] = edge_type_data[sample][day].cuda()
#             node_idx_data[sample][day] = node_idx_data[sample][day].cuda()
#             edge_idx_data[sample][day] = edge_idx_data[sample][day].cuda()
#
# model = TAGS_modularize(entity_emb=entity_emb, relation_emb=relation_emb, len_seq=args.len_seq,
#                         n_convs=args.n_convs, n_encoders=args.n_encoders, n_heads=args.n_heads,
#                         n_feedforward=args.n_feedforward, n_output=args.n_output, dropout=args.dropout)
#
# if args.cuda:
#     model.cuda()
# model.load_state_dict(torch.load('models/{}'.format('USW_1fold_0.0005.pkl')))
# parameters_num = sum(p.numel() for p in model.parameters() if p.requires_grad)
# print('Model Parameters: {:.4f}M'.format(parameters_num / 1000000.0))
#
# for idx in range(len(edge_index_data)):
#     y_pred, y_true = predict_sample(idx)
#     print(idx, y_pred, y_true)