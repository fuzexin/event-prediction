from __future__ import division
from __future__ import print_function

import argparse
import os
import time
import torch.optim as optim
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support


def evaluate():
    model.eval()
    y_true, y_pred = [], []
    for i in range(int(len(y_list) * 0.9), len(y_list)):
        # output = model(edge_index_data[i][:args.len_seq], edge_type_data[i][:args.len_seq],
        #                node_idx_data[i][:args.len_seq], edge_idx_data[i][:args.len_seq])
        output = model(edge_index_data[i][14-args.len_seq:], edge_type_data[i][14-args.len_seq:],
                       node_idx_data[i][14-args.len_seq:], edge_idx_data[i][14-args.len_seq:])
        y_true += y_list[i].data.tolist()
        bi_val = np.where(output.data.cpu().numpy() > 0.5, 1, 0)
        y_pred += torch.from_numpy(bi_val).tolist()
    prec, rec, f1, _ = precision_recall_fscore_support(y_true, y_pred, average="binary")
    acc = accuracy_score(y_true, y_pred)
    return acc, prec, rec, f1


def train(epoch, batch_size):
    loss, count = 0., 0
    model.train()
    iteration = int(len(y_list) * 0.9) // batch_size
    t1 = time.time()
    for it in range(iteration):
        loss_train = 0
        optimizer.zero_grad()
        for i in range(it * batch_size, (it + 1) * batch_size):
            # output = model(edge_index_data[i][:args.len_seq], edge_type_data[i][:args.len_seq],
            #                node_idx_data[i][:args.len_seq], edge_idx_data[i][:args.len_seq])
            output = model(edge_index_data[i][14-args.len_seq:], edge_type_data[i][14-args.len_seq:],
                           node_idx_data[i][14-args.len_seq:], edge_idx_data[i][14-args.len_seq:])
            loss_train += F.binary_cross_entropy(output, y_list[i]) #, weight=class_weights[int(y_list[i].item())]
            count += 1
        loss_train.backward()
        optimizer.step()
        loss += loss_train

    acc, prec, rec, f1 = evaluate()
    print('Epoch: {:03d}'.format(epoch + 1),
          'time: {:.4f}s'.format(time.time() - t1),
          'loss_train: {:.4f}'.format(loss / count),
          'precision_test: {:.4f}'.format(prec),
          'recall_test: {:.4f}'.format(rec),
          'f1_test: {:.4f}'.format(f1))

    return acc, prec, rec, f1


parser = argparse.ArgumentParser()
parser.add_argument('--location', type=str, default='USW', choices=['USW', 'USE', 'IND', 'UK'])
parser.add_argument('--datatype', type=str, default='correlation', choices=['correlation'])
parser.add_argument('--gpu_idx', type=int, default=0)
parser.add_argument('--seed', type=int, default=42)
parser.add_argument('--n_convs', type=int, default=1, choices=[1, 2, 3])
parser.add_argument('--n_encoders', type=int, default=1, choices=[1, 2, 3, 6])
parser.add_argument('--len_seq', type=int, default=14)
parser.add_argument('--n_heads', type=int, default=8, choices=[1, 2, 4, 8, 16])
parser.add_argument('--n_feedforward', type=int, default=64)
parser.add_argument('--batch_size', type=int, default=8)
parser.add_argument('--epochs', type=int, default=100)
parser.add_argument('--dropout', type=float, default=0.5)
parser.add_argument('--n_output', type=int, default=1)
parser.add_argument('--lr', type=float, default=5e-4, choices=[1e-3, 5e-4, 1e-4])
parser.add_argument('--weight_decay', type=float, default=5e-3, choices=[5e-3, 5e-4])
args = parser.parse_args()

os.environ["CUDA_VISIBLE_DEVICES"] = str(args.gpu_idx)
from models import *
args.cuda = torch.cuda.is_available()

location_list = ['USW', 'USE', 'UK', 'RU', 'IND', 'INDN']
for location in location_list:
    args.location = location
    result_list = []
    for fold in range(10):
        entity_emb, relation_emb, edge_index_data, edge_type_data, node_idx_data, y_list = load_data(args.location,
                                                                                                     args.datatype)
        edge_index_data, edge_type_data, node_idx_data, y_list = shuffle(args.seed, edge_index_data, edge_type_data,
                                                                         node_idx_data, y_list)
        edge_index_data, edge_type_data, node_idx_data, y_list = cross_cut(fold, edge_index_data, edge_type_data,
                                                                           node_idx_data, y_list)
        edge_idx_data = get_edge_idx_data(edge_type_data)

        np.random.seed(args.seed)
        torch.manual_seed(args.seed)
        if args.cuda:
            print('GPU: {} {}'.format(torch.cuda.get_device_name(), args.gpu_idx))
            torch.cuda.manual_seed(args.seed)
            entity_emb, relation_emb = entity_emb.cuda(), relation_emb.cuda()
            for sample in range(len(y_list)):
                y_list[sample] = y_list[sample].cuda()
                for day in range(14):
                    edge_index_data[sample][day] = edge_index_data[sample][day].cuda()
                    edge_type_data[sample][day] = edge_type_data[sample][day].cuda()
                    node_idx_data[sample][day] = node_idx_data[sample][day].cuda()
                    edge_idx_data[sample][day] = edge_idx_data[sample][day].cuda()

        model = TAGS_modularize(entity_emb=entity_emb,
                                relation_emb=relation_emb,
                                n_convs=args.n_convs,
                                n_encoders=args.n_encoders,
                                n_heads=args.n_heads,
                                n_feedforward=args.n_feedforward,
                                n_output=args.n_output,
                                dropout=args.dropout,
                                explain=False)
        optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)

        parameters_num = sum(p.numel() for p in model.parameters() if p.requires_grad)
        class_weights = torch.FloatTensor([0.5, 0.5])
        if args.cuda:
            model.cuda()
            class_weights = class_weights.cuda()

        print(args)
        print('Location/Samples: {}/{}'.format(args.location, len(y_list)))
        print('Node Embedding: ({},{})'.format(entity_emb.size(0), entity_emb.size(1)))
        print('Edge Embedding: ({},{})'.format(relation_emb.size(0), relation_emb.size(1)))
        print('Model Parameters: {:.4f}M'.format(parameters_num / 1000000.0))
        print("----------------Training-----------------")
        max_f1, max_epoch, bad_count = 0., 0, 0
        t1 = time.time()
        for epoch in range(args.epochs):
            acc, prec, rec, f1 = train(epoch, args.batch_size)
            if f1 >= max_f1 and rec < 0.95:
                max_f1, max_epoch, bad_count = f1, epoch, 0
                indicators = [acc, prec, rec, f1]
                # torch.save(model.state_dict(), 'models/{}_{}_{}.pkl'.format(args.location, args.datatype, fold+1))
            else:
                bad_count += 1
            if (epoch + 1) % 10 == 0 and epoch > 0:
                print('Fold: {}'.format(fold + 1),
                      'Best epoch: {:03d}'.format(max_epoch + 1),
                      'Best F1: {:.4f}'.format(max_f1))
            if epoch >= 75 and bad_count >= 25:
                print("------------Early Stop------------")
                break
        print("Training accomplished, elapsed: {:.4f}s".format(time.time()-t1))
        print("Best epoch: {:03d}".format(max_epoch + 1),
              "Best F1: {:.4f}".format(max_f1))
        result_list.append(indicators)
        with open('results/{}_{}_new.pkl'.format(args.location, args.datatype), 'wb') as f:
            pkl.dump(result_list, f)