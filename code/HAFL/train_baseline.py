from __future__ import division
from __future__ import print_function


def evaluate():
    model.eval()
    y_true, y_pred, y_score = [], [], []
    for i in range(int(len(adj_list)*0.9), len(adj_list)):
        adj = adj_list[i]
        idx = idx_list[i]
        y = y_list[i]
        if args.cuda:
            for j in range(len(adj)):
                adj[j] = adj[j].cuda()
            idx = idx.cuda()
            y = y.cuda()

        output = model(adj, idx)

        y_true += y.data.tolist()
        bi_val = np.where(output.data.cpu().numpy() > 0.5, 1, 0)
        y_pred += torch.from_numpy(bi_val).tolist()

    prec, rec, f1, _ = precision_recall_fscore_support(y_true, y_pred, average="binary")
    acc = accuracy_score(y_true, y_pred)
    return acc, prec, rec, f1


def train(epoch, adj_list, idx_list, y_list, batch_size):
    # loss = 0
    # count = 0
    # model.train()
    # iteration = int(len(adj_list)*0.9)//batch_size # 24
    #
    # t1 = time.time()
    # for it in tqdm(range(iteration), desc='train iteration'):
    #     loss_train = 0
    #     optimizer.zero_grad()
    #     for i in range(it*batch_size, (it+1)*batch_size):
    #         adj = adj_list[i]
    #         idx = idx_list[i]
    #         y = y_list[i]
    #         if args.cuda:
    #             for j in range(len(adj)):
    #                 adj[j] = adj[j].cuda()
    #             idx = idx.cuda()
    #             y = y.cuda()
    #         output = model(adj, idx)
    #         loss_train += F.binary_cross_entropy(output, y, weight=class_weights[int(y.item())])
    #         count += 1
    #     loss_train.backward()
    #     optimizer.step()
    #     loss += loss_train

    acc, prec, rec, f1 = evaluate()

    # print('Epoch: {:03d}'.format(epoch + 1),
    #       'time: {:.4f}s'.format(time.time() - t1),
    #       'loss_train: {:.4f}'.format(loss / count),
    #       'precision_val: {:.4f}'.format(prec),
    #       'recall_val: {:.4f}'.format(rec),
    #       'f1_val: {:.4f}'.format(f1))

    return acc, prec, rec, f1


import time
import argparse
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import accuracy_score
import torch.optim as optim
from baselines import *
from split_online_data import *

# Training settings
parser = argparse.ArgumentParser()
parser.add_argument('--location', type=str, default='USW', choices=['USW', 'USE', 'IND', 'UK'])
parser.add_argument('--model', type=str, default='tagsgcn', choices=['dynamicgcn', 'evolvegcn'])
parser.add_argument('--seed', type=int, default=42)
parser.add_argument('--len_seq', type=int, default=7)
parser.add_argument('--n_heads', type=int, default=8, choices=[1, 2, 4, 8, 16])
parser.add_argument('--n_feedforward', type=int, default=64)
parser.add_argument('--batch_size', type=int, default=8)
parser.add_argument('--epochs', type=int, default=10)
parser.add_argument('--lr', type=float, default=5e-4)
parser.add_argument('--weight_decay', type=float, default=5e-3)
parser.add_argument('--dropout', type=float, default=0.5)
parser.add_argument('--n_output', type=int, default=1)
args = parser.parse_args()
args.cuda = torch.cuda.is_available()


location_list = ['New York', 'San Francisco', 'London', 'Moskva', 'New Delhi', 'Mumbai']
# len_list = [8,9,10,11,12,13,14]
ld_list = [0]
for lead_day in ld_list:
    # args.len_seq = 14-lead_day
    for location in location_list[:]:
        args.location = location
        print(args)
        result_list = []
        for i in range(1):
            entity_size, relation_size, adj_list, idx_list, y_list = preprocess_dataset_undirected(args.location, args.len_seq, lead_day)
            adj_list, idx_list, y_list = shuffle_dgcn(args.seed, adj_list, idx_list, y_list)
            adj_list, idx_list, y_list = cross_cut_dgcn(i, adj_list, idx_list, y_list)

            # train_idx = int(len(adj_list)*0.9)
            # adj_train, idx_train, y_train = adj_list[:train_idx], idx_list[:train_idx], y_list[:train_idx]
            # adj_train, idx_train, y_train = shuffle_dgcn(args.seed, adj_train, idx_train, y_train)
            # adj_test, idx_test, y_test = adj_list[train_idx:], idx_list[train_idx:], y_list[train_idx:]

            pos, neg = count(y_list)
            class_weights = torch.FloatTensor([pos / (pos + neg), neg / (pos + neg)])
            print('class weights: {}'.format(class_weights))
            print(entity_size, relation_size)

            np.random.seed(args.seed)
            torch.manual_seed(args.seed)
            if args.cuda:
                torch.cuda.manual_seed(args.seed)
                print('GPU: ' + torch.cuda.get_device_name())

            if args.model == 'dynamicgcn':
                model = DynamicGCN(entity_size=entity_size, n_feature=100, n_output=args.n_output, n_hidden=args.len_seq, dropout=args.dropout)
            if args.model == 'evolvegcn':
                model = EvolveGCN(entity_size=entity_size, n_feature=100, n_output=args.n_output, dropout=args.dropout, rnn_model='gru')
            if args.model == 'tagsgcn':
                model = TAGS_gcn(entity_size=entity_size, n_feature=100, len_seq=args.len_seq, n_heads=args.n_heads, n_feedforward=args.n_feedforward, n_output=args.n_output, dropout=args.dropout)

            optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
            if args.cuda:
                model.cuda()
                class_weights = class_weights.cuda()
            parameters_num = sum(p.numel() for p in model.parameters() if p.requires_grad)

            print("----------------Training-----------------")
            print('Location: {}--{}'.format(args.location, len(y_list)))
            print('Pretrained embedding size: {}'.format(entity_size))
            print('Model Parameters: {:.4f}M'.format(parameters_num / 1000000.0))

            max_f1, max_epoch, bad_count = 0., 0, 0
            t = time.time()
            for epoch in range(args.epochs):
                acc, prec, rec, f1 = train(epoch, adj_list, idx_list, y_list, args.batch_size)
            print(time.time()-t)
            #     if f1 >= max_f1 and rec < 0.95:
            #         max_f1, max_epoch, bad_count = f1, epoch, 0
            #         indicators = [acc, prec, rec, f1]
            #         torch.save(model.state_dict(),
            #                    'online_models/{}_{}_{}_{}_true.pkl'.format(args.location, args.len_seq, i + 1, args.model, lead_day))
            #     else:
            #         if epoch >= 50:
            #             bad_count += 1
            #     if (epoch + 1) % 10 == 0 and epoch > 0:
            #         print('Fold: {}'.format(i + 1),
            #               'Best epoch: {:03d}'.format(max_epoch + 1),
            #               'Best F1: {:.4f}'.format(max_f1))
            #     if bad_count >= 25 and epoch >= 50:
            #         print("------------Early Stop------------")
            #         break
            # result_list.append(indicators)
            # with open('online_results/{}_{}_{}_true.pkl'.format(args.location, args.len_seq, args.model, lead_day), 'wb') as f:
            #     pkl.dump(result_list, f)
            #
            # print("Optimization Finished!")
            # print("Training time: {:.4f}s".format(time.time() - t))
            # print("F1: {:.4f}".format(max_f1),
            #       "Epoch: {:03d}".format(max_epoch + 1))