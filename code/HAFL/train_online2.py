from __future__ import division
from __future__ import print_function

import argparse
import os
import time
import torch.optim as optim
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support


def train(epoch, batch_size):
    loss, count = 0., 0
    model.train()
    y_true, y_pred = [], []
    iteration = len(train_idx) // batch_size
    t1 = time.time()
    for it in tqdm(range(iteration), desc='train iteration'):
        loss_train = 0
        optimizer.zero_grad()
        for i in range(it * batch_size, (it + 1) * batch_size):
            output = model(edge_index_train[i], edge_type_train[i], node_idx_train[i], edge_idx_train[i])
            loss_train += F.binary_cross_entropy(output, y_train[i], weight=class_weights[int(y_train[i].item())])
            count += 1
            y_true += y_train[i].data.tolist()
            bi_val = np.where(output.data.cpu().numpy() > 0.5, 1, 0)
            y_pred += torch.from_numpy(bi_val).tolist()
        loss_train.backward()
        optimizer.step()
        loss += loss_train

    _, __, f1_train, ___ = precision_recall_fscore_support(y_true, y_pred, average="binary")
    acc, prec, rec, f1 = evaluate()
    print('Epoch: {:03d}'.format(epoch + 1),
          'time: {:.4f}s'.format(time.time() - t1),
          'loss_train: {:.4f}'.format(loss / count),
          'f1_train: {:.4f}'.format(f1_train),
          'precision_test: {:.4f}'.format(prec),
          'recall_test: {:.4f}'.format(rec),
          'f1_test: {:.4f}'.format(f1))

    return acc, prec, rec, f1


def evaluate():
    model.eval()
    y_true, y_pred = [], []
    for i in test_idx:
        output = model(edge_index_test[i], edge_type_test[i], node_idx_test[i], edge_idx_test[i])
        y_true += y_test[i].data.tolist()
        bi_val = np.where(output.data.cpu().numpy() > 0.5, 1, 0)
        y_pred += torch.from_numpy(bi_val).tolist()
    prec, rec, f1, _ = precision_recall_fscore_support(y_true, y_pred, average="binary")
    acc = accuracy_score(y_true, y_pred)
    return acc, prec, rec, f1


def load_result(filename):
    try:
        with open('online_results/{}'.format(filename), 'rb') as f:
            result_list = pkl.load(f)
        acc = [result[0] for result in result_list]
        prec = [result[1] for result in result_list]
        rec = [result[2] for result in result_list]
        f1 = [result[3] for result in result_list]
        print(filename, [round(np.average(acc), 3), round(np.std(acc), 3)], [round(np.average(prec), 3), round(np.std(prec), 3)], [round(np.average(rec), 3), round(np.std(rec), 3)], [round(np.average(f1), 3), round(np.std(f1), 3)], len(result_list))
    except Exception:
        print(filename, 'file not found')


def count(y_list):
    pos_count, neg_count = 0, 0
    for y in y_list:
        if y.item() > 0.:
            pos_count += 1
        else:
            neg_count += 1
    return pos_count, neg_count


def sampling_testdata(y_test):
    pos_count, neg_count, idx = 0, 0, []
    for i, y in enumerate(y_test):
        if y.item() > 0.:
            pos_count += 1
            idx.append(i)
    for i, y in enumerate(y_test):
        if y.item() == 0. and neg_count <= pos_count:
            neg_count += 1
            idx.append(i)
    return process_list(idx)


def simulate(pos_rate, sample_count):
    prec_list, rec_list, f1_list = [], [], []
    for i in range(100):
        pred_list, label_list = [], []
        for j in range(sample_count):
            if random.random() < pos_rate:
                pred_list.append(1.)
            else:
                pred_list.append(0.)
            if j < int(sample_count*pos_rate):
                label_list.append(1.)
            else:
                label_list.append(0.)
        prec, rec, f1, _ = precision_recall_fscore_support(label_list, pred_list, average="binary")
        prec_list.append(prec)
        rec_list.append(rec)
        f1_list.append(f1)
    return np.mean(pred_list), np.mean(rec_list), np.mean(f1_list)


parser = argparse.ArgumentParser()
parser.add_argument('--gpu_idx', type=int, default=3)
parser.add_argument('--shuffle', default=False)
parser.add_argument('--seed', type=int, default=42)
parser.add_argument('--n_feature', type=int, default=100)
parser.add_argument('--n_convs', type=int, default=1, choices=[1, 2, 3])
parser.add_argument('--n_encoders', type=int, default=1, choices=[1, 2, 3, 6])
parser.add_argument('--len_seq', type=int, default=7)
parser.add_argument('--n_heads', type=int, default=8, choices=[1, 2, 4, 8, 16])
parser.add_argument('--n_feedforward', type=int, default=64)
parser.add_argument('--batch_size', type=int, default=8)
parser.add_argument('--epochs', type=int, default=100)
parser.add_argument('--dropout', type=float, default=0.5)
parser.add_argument('--n_output', type=int, default=1)
parser.add_argument('--lr', type=float, default=5e-4, choices=[1e-3, 5e-4, 1e-4])
parser.add_argument('--weight_decay', type=float, default=5e-3, choices=[5e-3, 5e-4])
args = parser.parse_args()

os.environ["CUDA_VISIBLE_DEVICES"] = str(args.gpu_idx)
from models import *
args.cuda = torch.cuda.is_available()
from split_online_data import *


file_list = find_file('online_results/', 'pkl')
for file in file_list:
    load_result(file)

# tokenizer = BertTokenizer.from_pretrained('../local_code/data/bert_models/cased_L-12_H-768_A-12')
# model = BertModel.from_pretrained('../local_code/data/bert_models/cased_L-12_H-768_A-12')
location_list = ['New York', 'San Francisco', 'London', 'Moskva', 'New Delhi', 'Mumbai']
for location in location_list[3:]:
    result_list = []
    for n in range(10):
        entity_size, relation_size, edge_index_data, edge_type_data, node_idx_data, y_list = preprocess_dataset2(location, args.len_seq)
        edge_index_data, edge_type_data, node_idx_data, y_list = shuffle(args.seed, edge_index_data, edge_type_data, node_idx_data, y_list)

        pos, neg = count(y_list)
        print('pos:neg = {}:{}'.format(pos, neg))
        # prec, rec, f1 = simulate(pos/(pos+neg), pos+neg)
        # print('prec, rec, f1: {}, {}, {}'.format(prec, rec, f1))

        np.random.seed(args.seed)
        torch.manual_seed(args.seed)
        if args.cuda:
            print('GPU: {} {}'.format(torch.cuda.get_device_name(), args.gpu_idx))
            torch.cuda.manual_seed(args.seed)
            for sample in range(len(y_list)):
                y_list[sample] = y_list[sample].cuda()
                for day in range(args.len_seq):
                    edge_index_data[sample][day] = edge_index_data[sample][day].cuda()
                    edge_type_data[sample][day] = edge_type_data[sample][day].cuda()
                    node_idx_data[sample][day] = node_idx_data[sample][day].cuda()

        train_num = int(len(y_list) * 0.9)
        edge_index_train, edge_type_train, node_idx_train, y_train = edge_index_data[:train_num], edge_type_data[:train_num], node_idx_data[:train_num], y_list[:train_num]
        edge_index_test, edge_type_test, node_idx_test, y_test = edge_index_data[train_num:], edge_type_data[train_num:], node_idx_data[train_num:], y_list[train_num:]
        edge_idx_train, edge_idx_test = get_edge_idx_data(edge_type_train), get_edge_idx_data(edge_type_test)
        # train_idx = sampling_dataset(y_train)
        train_idx = [i for i in range(train_num)]
        # test_idx = sampling_dataset(y_test)
        test_idx = [i for i in range(len(y_test))]

        # model = GRU(entity_size=entity_size,
        #             relation_size=relation_size,
        #             n_feature=args.n_feature,
        #             n_convs=args.n_convs,
        #             n_encoders=args.n_encoders,
        #             n_heads=args.n_heads,
        #             n_feedforward=args.n_feedforward,
        #             n_output=args.n_output,
        #             dropout=args.dropout)
        model = CNN(entity_size=entity_size,
                    relation_size=relation_size,
                    n_feature=args.n_feature,
                    n_convs=args.n_convs,
                    num_filter=1,
                    filter_sizes=[3,5,7],
                    n_output=args.n_output,
                    dropout=args.dropout)
        optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
        parameters_num = sum(p.numel() for p in model.parameters() if p.requires_grad)
        class_weights = torch.FloatTensor([pos/(pos+neg), neg/(pos+neg)])
        if args.cuda:
            model.cuda()
            class_weights = class_weights.cuda()

        print(args)
        print('Location: {}'.format(location))
        print('Train Samples: {}'.format(len(train_idx)))
        print('Model Parameters: {:.4f}M'.format(parameters_num / 1000000.0))
        print("----------------Training-----------------")

        max_f1, max_epoch, bad_count = 0., 0, 0
        t1 = time.time()
        for epoch in range(args.epochs):
            acc, prec, rec, f1 = train(epoch, args.batch_size)
            if f1 >= max_f1 and rec < 0.95:
                max_f1, max_epoch, bad_count = f1, epoch, 0
                indicators = [acc, prec, rec, f1]
                torch.save(model.state_dict(), 'online_models/{}_{}_cnn_{}.pkl'.format(location, args.len_seq, n))
            else:
                bad_count += 1
            if (epoch + 1) % 10 == 0 and epoch > 0:
                print('Best epoch: {:03d}'.format(max_epoch + 1),
                      'Best F1: {:.4f}'.format(max_f1))
            if epoch >= 75 and bad_count >= 25:
                print("------------Early Stop------------")
                break
        print("Training accomplished, elapsed: {:.4f}s".format(time.time()-t1))
        print("Best epoch: {:03d}".format(max_epoch + 1),
              "Best F1: {:.4f}".format(max_f1))
        result_list.append(indicators)
        with open('online_results/{}_{}_cnn.pkl'.format(location, args.len_seq), 'wb') as f:
            pkl.dump(result_list, f)