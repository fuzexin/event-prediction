"""
    使用线性模型, target number数据实现事件预测
"""
from random import shuffle
import torch, pickle, logging, datetime
import torch.utils, torch.utils.data
import torch.nn.functional as F
from torch.utils.tensorboard import SummaryWriter

# some global settings
logging.basicConfig(level=logging.DEBUG)


# 构建数据类
class TargetNumberSequence(torch.utils.data.Dataset):
    def __init__(self, seq_len=7, lead_len=3, target=14, training_rate=0.8, batch_size=64) -> None:
        # seq_len: 序列长度, lead_len: 预测间隔, 
        # target: 目标事件cameo.
        self.seq_len = seq_len
        self.lead_len = lead_len
        self.data_field = ["GlobalEventID", "Actor1Name", "EventCode", \
                           "Actor2Name", "NumArticles", "IsRootEvent", "SOURCEURL", "EventDate"]
        self.target = target
        self.training_rate = 0.8
        self.batch_size = batch_size
        self.data = []
        self.pos_len, self.neg_len = 0, 0

    
    def addData(self, data_path):
        # data_path: GDELT原始数据文件路径
        with open(data_path, 'rb') as f:
            gdelt_data = pickle.load(f)
        e_count_dict = {}
        eventcode_idx = self.data_field.index("EventCode")
        date_idx = self.data_field.index("EventDate")
        for one_data in gdelt_data:
            event_date = one_data[date_idx]
            event_code_i = int(one_data[eventcode_idx][:2])
            if event_date in e_count_dict:
                e_count_dict[event_date][event_code_i-1] += 1
            else:
                temp_count = [0 for i in range(20)]
                temp_count[event_code_i-1] += 1
                e_count_dict[event_date] = temp_count
        logging.debug(e_count_dict)

        self.begin_date = gdelt_data[0][date_idx]
        self.end_date = gdelt_data[-1][date_idx]
        all_count_data = []
        iter_date = datetime.datetime.strptime(self.begin_date, "%Y-%m-%d").date()
        end_date = datetime.datetime.strptime(self.end_date, "%Y-%m-%d").date()
        
        while iter_date <= end_date:
            date_str = datetime.date.strftime(iter_date,"%Y-%m-%d")
            if date_str in e_count_dict:
                all_count_data.append(e_count_dict[date_str])
            else:
                # 补充没有数据的日期
                logging.info(f"data of {date_str} is empty.")
                all_count_data.append([0 for i in range(20)])
            iter_date += datetime.timedelta(days=1)
        
        # 所有数据的天数
        all_day_len = len(all_count_data)
        for i in range(all_day_len):
            # 单个x序列的最后一天的idx
            y_pos = i + self.seq_len + self.lead_len - 1
            if y_pos < all_day_len:
                x = all_count_data[i:i+self.seq_len]
                y = 0 if all_count_data[y_pos][self.target-1]==0 else 1
                self.pos_len += y
                self.data.append((x,y))
        self.neg_len = len(self.data)-self.pos_len

        logging.info(f"data info\ndate period: {self.begin_date}--{self.end_date}\n"\
                    f"data length: {len(self.data)}\npos rate: {self.pos_len/len(self.data):.2f}\n"\
                    f"neg rate: {self.neg_len/len(self.data):.2f}")
    
    
    def get_dataloader(self, train=True):
        # shuffle
        shuffle(self.data)
        # 切分为training he testing set
        carve_point = int(len(self.data)*self.training_rate)
        if train:
            train_data = self.data[:carve_point]
            X, Y = zip(*train_data)
            X, Y = torch.tensor(X), torch.tensor(Y)
            # normalization
            norm_deno = X.sum(dim=-1, keepdim=True)
            # avoid zero divided
            norm_deno += (norm_deno == 0).type(dtype=torch.int64)
            X = X/norm_deno
            train_data = torch.utils.data.TensorDataset(X, Y)
            return torch.utils.data.DataLoader(train_data, batch_size=self.batch_size, shuffle=train)
        else:
            test_data = self.data[carve_point:]
            X, Y = zip(*test_data)
            X, Y = torch.tensor(X), torch.tensor(Y)
            # normalization
            norm_deno = X.sum(dim=-1, keepdim=True)
            # avoid zero divided
            norm_deno += (norm_deno == 0).type(dtype=torch.int64)
            X = X/norm_deno
            test_data = torch.utils.data.TensorDataset(X, Y)
            return torch.utils.data.DataLoader(test_data, batch_size=self.batch_size, shuffle=train)

        
# 构建使用每日事件数量的模型
class SoftmaxRegression(torch.nn.Module):
    def __init__(self, num_input, num_output) -> None:
        super().__init__()
        self.net = torch.nn.Sequential(
            torch.nn.Flatten(),
            torch.nn.Linear(num_input, num_output)
        )
    
    def forward(self, X):
        return self.net(X)

# training settings
def loss_fn(Y_hat, Y, pos_rate=0.5, average=True):
    pos_weight, neg_weight = 1/(2*pos_rate), 1/(2*(1-pos_rate))
    weight = torch.tensor([pos_weight, neg_weight])
    return F.cross_entropy(Y_hat, Y, reduction="mean" if average==True else 'none', weight=weight)

def train(model, data, optimizer, loss_fn, epoch_idx, tb_writer):
    train_loader = data.get_dataloader(train=True)
    avg_epoch_loss, batch_num = 0, 0
    pos_rate = data.pos_len/(data.pos_len+data.neg_len)
    for batch_data in train_loader:
        X, Y = batch_data
        Y_hat = model(X)
        loss_batch = loss_fn(Y_hat, Y, pos_rate)
        optimizer.zero_grad()
        loss_batch.backward()
        optimizer.step()
        avg_epoch_loss += loss_batch
        batch_num += 1
    avg_epoch_loss /= batch_num
    tb_writer.add_scalar("Loss/train", avg_epoch_loss, epoch_idx)
    

def accuracy(Y_hat, Y, reduce = True):
    if reduce:
        return (Y_hat.type(Y.dtype)==Y).sum()
    else:
        return (Y_hat.type(Y.dtype)==Y)
    
def validate(model, data, epoch_idx, tb_writer):
    val_loader = data.get_dataloader(train=False)
    model.eval()
    all_compare = []
    avg_val_loss, batch_idx = 0, 0
    with torch.no_grad():
        for batch_data in val_loader:
            X, Y = batch_data
            Y_hat = model(X)
            # calculate val loss
            loss_batch = loss_fn(Y_hat, Y)
            avg_val_loss += loss_batch
            # calculate acc
            Y_hat = Y_hat.argmax(1)
            compare = accuracy(Y_hat, Y, reduce=False).tolist()
            all_compare.extend(compare)
            batch_idx += 1
    acc = sum(all_compare)/len(all_compare)
    avg_val_loss /= batch_idx
    tb_writer.add_scalar("Loss/val", avg_val_loss, epoch_idx)
    tb_writer.add_scalar("Acc/val", acc, epoch_idx)

def fit(model, max_epoches, optimizer, data, tb_writer):
    for epoch_idx in range(max_epoches):
        train(model, data, optimizer, loss_fn, epoch_idx, tb_writer)
        validate(model, data, epoch_idx, tb_writer)

if __name__ == "__main__":
    lr = 1e-2
    max_epoches = 50
    data1_path = 'code/dev/data/India/2018-2022/Chennai.pkl'
    data2_path = 'code/dev/data/India/2018-2022/New Delhi.pkl'
    # set record file
    tb_dir_name = datetime.datetime.now().strftime("%b%d_%H-%M_")
    tb_dir_name += "seq_len7"
    tb_writer = SummaryWriter(f'code/dev/runs/{tb_dir_name}')
    
    data = TargetNumberSequence(lead_len=1)
    # data.addData(data1_path)
    data.addData(data1_path)
    # ---DEBUG
    # train_loader = data.get_dataloader(train=True)
    # print(next(iter(train_loader)))
    # print('debug')
    # ---
    model = SoftmaxRegression(140, 2)
    optimizer = torch.optim.SGD(model.parameters(), lr)
    fit(model, max_epoches, optimizer, data, tb_writer)
    tb_writer.flush()
