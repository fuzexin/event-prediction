""" Constructing CompGCN"""
import dgl.data
import logging, pickle, dgl, torch
import pandas as pd
import numpy as np

logging.basicConfig(level=logging.DEBUG)

# handling one-day's GDELT data and formatting it as dgl graph


def getEntityIdx(data_field, data, depos_path):
    # aquiring all entities and its indices
    # and storing them in file.
    entity = set()
    for one_data in data:
        actor1name = one_data[data_field.index("Actor1Name")]
        actor2name = one_data[data_field.index("Actor2Name")]
        if len(actor1name)>0:
            entity.add(actor1name)
        if len(actor2name)>0:
            entity.add(actor2name)
    entity = list(entity)
    entity.sort()
    entity = pd.DataFrame(zip(range(len(entity)), entity))
    entity.to_csv(depos_path, sep='\t', index=False, header=False, doublequote=False)
    logging.info("save entity_set.csv OK!")

# for handling and storing event graph list
class EventGraph:
    def __init__(self, data_field, entity_path, ecode_path) -> None:
        self.data_field = data_field
        self.actor1_index = data_field.index('Actor1Name')
        self.actor2_index = data_field.index('Actor2Name')
        self.event_code_index = data_field.index('EventCode')
        self.event_date_index = data_field.index("EventDate")

        # load the entity and evencode dict
        entity_dict = pd.read_csv(entity_path, names=['id', 'entity_name'], sep='\t')
        self.entity_dict = dict(zip(entity_dict['entity_name'], entity_dict['id']))
        relation_dict = pd.read_csv(ecode_path, sep='\t', names=['code'], dtype=str, index_col=[1])
        self.realtion_dict = dict(zip(relation_dict['code'], range(len(relation_dict['code']))))
  
    def genGraphList(self, gdelt_data, deposit_path):

        # date_list for storing date, graph_list for storaging graph data
        date_list = []
        graph_list = []
        
        count_index = 0
        for one_date_data in gdelt_data:
            # get date
            one_date = one_date_data[0][-1]
            date_list.append(one_date)

            # get graph
            g = self._genDayGraph(one_date_data, range(count_index, count_index + len(one_date_data)))
            graph_list.append(g)

            count_index += len(one_date_data)

        # assign and save the date and graph data
        self.graph_list = dict(zip(date_list, graph_list))

        with open(deposit_path, 'wb') as f:
            pickle.dump(self.graph_list, f)
        
        logging.info('save graph_list OK')


    
    
    def _genDayGraph(self, data, edge_indices):
        # get index of actor1, eventcode, actor2
        index_data = []
        for i in range(len(data)):
            actor1 = self.entity_dict[data[i][self.actor1_index]]
            eventcode = self.realtion_dict[data[i][self.event_code_index]]
            actor2 = self.entity_dict[data[i][self.actor2_index]]
            index_data.append([actor1,eventcode,actor2])
        
        # use the index data to generate graph
        index_data = np.array(index_data)
        edge_indices = np.array(edge_indices)
        g = self._get_big_graph_w_idx(index_data, edge_indices)

        return g

    def _get_big_graph_w_idx(self, data, edge_indices):
        # from  GLEAN's 1_get_digraphs.py file
        # data:the data is one date's data, which composed as (subject, relation, object)
        # edge_indices: the every piece of 'data' in the total data's position(index).
        
        # separate the different composition of data
        src, rel, dst = data.transpose()
        # get unique entity, including subject and object, 
        # edges is the src and dst's position in the unique list. 
        # np.unique(***,return_inverse=True) :return the original data's index 
        # in the generated unique list if the return_inverse = True.
        uniq_v, edges = np.unique((src, dst), return_inverse=True)
        # now the 'src' and 'dst' is the index in the unique list.
        src, dst = np.reshape(edges, (2, -1))
        # use dgl for handling graph
        g = dgl.DGLGraph()
        g.add_nodes(len(uniq_v))
        g.add_edges(src, dst, {'eid': torch.from_numpy(edge_indices)}) # array list
        norm = self._comp_deg_norm(g)
        g.ndata.update({'id': torch.from_numpy(uniq_v).long().view(-1, 1), 'norm': norm.view(-1, 1)})
        g.edata['r'] = torch.LongTensor(rel)
        g.ids = {}
        idx = 0
        for id in uniq_v:
            g.ids[id] = idx
            idx += 1
        return g
    
    def _comp_deg_norm(self, g):
        in_deg = g.in_degrees(range(g.number_of_nodes())).float()
        # make in-degree 0 to 1 
        in_deg[torch.nonzero(in_deg == 0, as_tuple=False).view(-1)] = 1
        norm = 1.0 / in_deg
        return norm



if __name__ == "__main__":

    # resource
    
    # for retrieve index of fields
    data_field = ['EventID', 'Actor1Name', 'EventCode', 'Actor2Name', 'AllNames', 'NumArticles', 'IsRootEvent', 'EventDate']
    
    gdelt_data_path = "D:/Project/event_pred_research2024/code/dev/data/Thailand/2018-2022/Bangkok.pkl"
    graph_list_path = r"D:\Project\event_pred_research2024\code\dev\data\_dev\thailand\graph_list.pkl"

    entity_path = r"D:\Project\event_pred_research2024\code\dev\data\_dev\thailand\entity_set.csv"
    relation_path = r"D:\Project\event_pred_research2024\code\dev\support\cameo.txt"

    # Reading GDELT data
    with open(gdelt_data_path, 'rb') as f:
        gdelt_data = pickle.load(f)
    
    # Getting all entity and its indices
    # getEntityIdx(data_field, gdelt_data, "D:/Project/event_pred_research2024/code/dev/data/_dev/thailand/entity_set.csv")

    # Getting event graph list
    bangkok_event_graph = EventGraph(data_field, entity_path, relation_path)
    bangkok_event_graph.genGraphList(gdelt_data, graph_list_path)


    