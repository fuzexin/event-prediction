"""
    使用y=wx+N(0,sigma)来生成模型训练数据, 构造线性回归模型y=wx, 使用数据进行训练
    探索learning rate与epoch之间的关系, init parameters与epoch之间的关系.   
"""

import torch
import numpy, math, logging

# configure logging settings
tlogger = logging.Logger(__name__)
s_handler = logging.StreamHandler()
f_handler = logging.FileHandler("code/dev/data/initial-parameter_vs_epoch.txt")
fmt = logging.Formatter("%(asctime)s: %(message)s")
s_handler.setFormatter(fmt)
f_handler.setFormatter(fmt)
s_handler.setLevel(logging.DEBUG)


f_handler.setLevel(logging.DEBUG)
tlogger.addHandler(s_handler)
tlogger.addHandler(f_handler)


# define model  
class Linear_num(torch.nn.Module):
    def __init__(self, input_dim, output_dim) -> None:
        super().__init__()
        self.linear = torch.nn.Linear(input_dim, output_dim)

    def forward(self, input):
        output = self.linear(input)
        return output


# get data
# generate artificial data  
def generate_atf_data(n):
    # 1. initialize w and b
    w = numpy.array([3.5, 2.8]).reshape(-1,1)
    b = numpy.array(5)
    # print(f"actual w: {w}\n actual b: {b}")
    # 2. generate X
    X = numpy.random.random_sample((n,2))
    # 3. generate a little noise
    noise = numpy.random.randn(n,1) * 0.1
    # 3. calculate Y
    Y = X@w + b + noise
    # 4. return X and Y
    X = torch.Tensor(X)
    Y = torch.Tensor(Y)
    return X, Y

# assembling by batch  
def data_loader(train_data, batch_size, shuffle = True):
    X, Y = train_data
    
    # shuffle index but not data, it is important
    data_index = list(range(len(X)))
    numpy.random.shuffle(data_index)

    # separate by batch size
    b_num = math.ceil(len(X)/batch_size)
    for i in range(b_num):
        yield X[data_index[i*batch_size: (i+1)*batch_size]], Y[data_index[i*batch_size: (i+1)*batch_size]]
    
# train model
def train(model, train_loader, optimizer, loss_fn):
    # training by batches
    for batch in train_loader:
        X, Y = batch
        Y_pred = model(X)
        loss_batch = loss_fn(Y_pred, Y)
        optimizer.zero_grad()
        loss_batch.backward()
        optimizer.step()


# test
if __name__ == "__main__":
    # parameter setting
    learning_rate = 0.01
    batch_size = 32
    epoch = 50000
    # 获取数据
    X, Y = generate_atf_data(1000)
    # 获取模型
    model = Linear_num(2, 1)
    # 为探索initial parameter与epoch之间的关系, 需要设定模型的初始参数
    diff_value = 10
    for one_para in model.parameters():
        one_para.detach()[:] += diff_value
    # define optimizer and loss calculating function
    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)
    loss_fn = torch.nn.MSELoss()
    # train model  
    model.train()
    distance = math.inf
    epoch_count = 0
    fmt = logging.Formatter("%(message)s")
    s_handler.setFormatter(fmt)
    f_handler.setFormatter(fmt)
    tlogger.info(f"## initial parameter diff: {diff_value}")
    while(distance > 0.001 and epoch_count < epoch):
        temp_para_ls = []
        for one_para in model.parameters():
            temp_para_ls.extend(one_para.reshape(-1).tolist())
        
        distance = torch.tensor(temp_para_ls) - torch.tensor([3.5, 2.8, 5])
        distance = (distance**2).sum()
        
        fmt = logging.Formatter("%(asctime)s: %(message)s")
        s_handler.setFormatter(fmt)
        f_handler.setFormatter(fmt)
        tlogger.info(f'Epoch: {epoch_count} | Actual: {[3.5, 2.8, 5]} | Update: {temp_para_ls} | Distance: {distance.item()}')
        train_loader = data_loader((X, Y), batch_size)
        train(model, train_loader, optimizer, loss_fn)
        epoch_count += 1
