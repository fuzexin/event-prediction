""" 
    Athor:zxf
    Using for: training multi models simultanously.
"""
# -*- coding: utf-8 -*-
from __future__ import absolute_import  # 引用Python自带的库
from __future__ import unicode_literals #在python3中默认的编码采用了unicode
from __future__ import division #导入python未来支持的语言特征division(精确除法)
from __future__ import print_function #在python2.X，使用print就得像python3.X那样加括号使用

# import ptvsd
# ptvsd.enable_attach(address = ('172.20.201.90', 3000))
# ptvsd.wait_for_attach() 

import os, itertools, random, argparse, time, datetime

ap = argparse.ArgumentParser()
ap.add_argument('--dataset', type=str, default='THAD6h', help="dataset string")
ap.add_argument('--embedding', type=str, default='', help="word embedding string")
ap.add_argument('--tensorboard_log', type=str, default='', help="name of this run (use timestamp instead)")#运行的名称
ap.add_argument('--seed', type=int, default=42, help='random seed')#随机种子
ap.add_argument('--epochs', default=50, type=int, help='number of epochs to train')#迭代次数
ap.add_argument('--batch', type=int, default=32, help="batch size (due to sparse matrix operations)")#大小
ap.add_argument('--lr', type=float, default=1e-3, help='initial learning rate')#初始的学习率
ap.add_argument('--weight_decay', type=float, default=5e-4, help='weight decay (L2 loss on parameters)')#权值衰减
ap.add_argument('--dropout', type=float, default=0.2, help='dropout rate (1 - keep probability)')#丢失率
ap.add_argument('--f_dim', type=int, default=100, help="feature dimensions of graph nodes") #图节点的特征维数
ap.add_argument('--n_hidden', default=7, type=int, help='number of hidden layers')#隐藏层的层数
ap.add_argument('--n_class', type=int, default=1, help="number of class (default 1)") #类的数量
ap.add_argument('--check_point', type=int, default=1, help=" point")#检查点
ap.add_argument('--model', default='DynamicGCN', choices=['DynamicheckcGCN','GCN'], help='')#训练模型设置
ap.add_argument('--shuffle', action='store_false', default=True, help="Shuffle dataset 0/1")#数据洗牌，是否打乱顺序//modified!
ap.add_argument('--train', type=float, default=.825, help="training ratio (0, 1)")#训练率
ap.add_argument('--val', type=float, default=.175, help="validation ratio (0, 1)")#检验率
ap.add_argument('--test', type=float, default=.0, help="testing ratio (0, 1) test file is seperated")#测试率
ap.add_argument('--pos_rate', type=float, default=0.5, help='positive sample rate')
ap.add_argument('--fastmode', action='store_true', default=False, help='validate during training')#是否在训练过程中验证
ap.add_argument('--mylog', action='store_false', default=True,  help='tensorboad log')#tensorboad log，应该是日志记录
ap.add_argument('--patience', type=int, default=10,  help='patience for early stop')#patience for early stop ? 停止的等待最大时间？（未确定）
ap.add_argument('--cuda', action='store_false', default=True, help='use cuda')#是否使用cuda？
ap.add_argument('--gpu', type=str, default='0', help='used gpu')
args = ap.parse_args()

os.environ["CUDA_VISIBLE_DEVICES"]= args.gpu
#set program visible gpu
import torch
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import DataLoader
import numpy as np
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import roc_auc_score, accuracy_score
from sklearn.metrics import precision_recall_curve

import scipy.sparse as sp
#scipy: 包含致力于科学计算中常见问题的各个工具箱
from utils import *
#utils: 本地文件夹下的（自己编写的）常用函数 
from models import *
#models: 本地文件夹下的（自己编写的）模型 
import shutil
#shutil: 高级的 文件、文件夹、压缩包 处理模块
import logging
#日志输出

import glob
#文件名模式匹配
from tensorboardX import SummaryWriter
#Pytorch的可视化工具

logger = logging.getLogger(__name__)

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')  

np.random.seed(args.seed)

args.cuda = args.cuda and torch.cuda.is_available() 
logger.info('CUDA status %s', args.cuda)
if args.cuda:
    torch.cuda.manual_seed(args.seed)

def evaluate(epoch, model, val_dict, class_weights, log_desc='val_'):
    model.eval()
    total = 0.
    loss, prec, rec, f1, acc, auc = 0., 0., 0., 0., 0., 0.
    y_true, y_pred, y_score = [], [], []
    batch_size = 1
    x_val, y_val, idx_val = val_dict['x'], val_dict['y'], val_dict['idx']
    for i in range(len(x_val)):
        adj = x_val[i]
        y = y_val[i]
        idx = idx_val[i]
        

        if args.cuda:
            y = y.cuda()
            idx = idx.cuda()
            if args.model == 'DynamicGCN':
                for i in range(len(adj)):
                    adj[i] = adj[i].cuda()
            else:
                adj = adj.cuda()
        output,_ = model(adj, idx)

        loss_train = F.binary_cross_entropy(output, y, weight=class_weights[int(y.item())])
        loss += batch_size * loss_train.item()
        y_true += y.data.tolist()
        bi_val = np.where(output.data.cpu().numpy() > 0.5, 1, 0)
        y_pred += torch.from_numpy(bi_val).tolist()
        y_score += output.data.tolist()
        total += batch_size
 

    # print(y_pred,y_true);exit()

    prec, rec, f1, _ = precision_recall_fscore_support(y_true, y_pred, average="binary")  
    acc = accuracy_score(y_true, y_pred)
    auc = roc_auc_score(y_true, y_score)
    logger.info("%sloss: %.4f AUC: %.4f Prec: %.4f Rec: %.4f F1: %.4f Acc: %.4f", # 
            log_desc, loss / total, auc, prec, rec, f1, acc) 

    return prec, rec, f1, acc, auc


def train(epoch, train_dict, val_dict, test_dict, model, optimizer, class_weights):
    model.train()
    loss, total = 0., 0.
    batch_size = 1

    x_train, y_train, idx_train = train_dict['x'], train_dict['y'], train_dict['idx']

    if sys.version_info > (3, 0):
        combined = list(zip(x_train, y_train, idx_train))
        random.shuffle(combined)
        x_train[:], y_train[:], idx_train[:] = zip(*combined)
    else:
        z = zip(x_train, y_train, idx_train)
        random.shuffle(z)
        x_train, y_train, idx_train = zip(*z)

    for i in range(len(x_train)):
        adj = x_train[i]
        y = y_train[i]
        # feature = f_train[i]
        idx = idx_train[i]
        
        if args.cuda:
            y = y.cuda()
            idx = idx.cuda()
            if args.model == 'DynamicGCN':
                for i in range(len(adj)):
                    adj[i] = adj[i].cuda()
            else:
                adj = adj.cuda()
                
        optimizer.zero_grad()
        # print("i:---------------", i)
        # print("adj's shape: {}, index's shape{}".format(adj.shape, idx.shape))        

        output,_ = model(adj, idx)
        loss_train = F.binary_cross_entropy(output, y, weight=class_weights[int(y.item())])
        loss += batch_size * loss_train.item()
        total += batch_size
        loss_train.backward()
        optimizer.step()

    logger.info("train loss epoch %d %f", epoch, loss / total)

    if not args.fastmode:
        if (epoch + 0) % args.check_point == 0:
            logger.info("epoch %d, checkpoint!", epoch)
            if args.val > 0.:
                # evaluate(epoch, train_dict, log_desc='train_')
                prec, rec, f1, acc, auc = evaluate(epoch, model, val_dict, class_weights, log_desc='val_')
            else:
                evaluate(epoch, model, train_dict, class_weights, log_desc='train_')
                prec, rec, f1, acc, auc = evaluate(epoch, model, test_dict, class_weights, log_desc='test_')
    return acc


def trainProcess(dataset_name, ):
    
    args.dataset = dataset_name
    args.embedding = args.dataset
    if args.model == 'DynamicGCN':
        train_dict, val_dict, test_dict, pretrained_emb = load_sparse_temporal_data(args.dataset, args.embedding, args.f_dim, args.train, args.val, args.test)
        
    else:
        train_dict, val_dict, test_dict, pretrained_emb = load_dynamic_graph_data(args.dataset, args.embedding, args.f_dim, args.train, args.val, args.test)

    if args.cuda:
        pretrained_emb = pretrained_emb.cuda()
    logger.info('load dataset %s', args.dataset)

    if args.model == 'DynamicGCN':
        model = DynamicGCN(pretrained_emb=pretrained_emb,
                                n_output=args.n_class, #类的数量
                                n_hidden=args.n_hidden, #hidden layer 隐藏层
                                dropout=args.dropout) #丢失率
    else:
        model = GCN(pretrained_emb=pretrained_emb, 
                    n_output=args.n_class, 
                    dropout=args.dropout)

    logger.info('model %s', args.model)
    if args.cuda:
        model.cuda()
    # optimizer and loss
    optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr, weight_decay=args.weight_decay)
    pytorch_total_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    print(f"all total parameters of this model {pytorch_total_params}")

    # read class_weights from file
    with open(f"data/{dataset_name}/data_specification.txt", "r") as f:
        data_info = f.readlines()
    pos_rate = float(data_info[-1].split("=")[-1].split(':')[0])
    args.pos_rate = pos_rate

    class_weights = torch.FloatTensor([1/(2*(1-args.pos_rate)), 1/(2*args.pos_rate)])
    print("class weight: {}".format(list(class_weights)))
    if args.cuda:
        class_weights = class_weights.cuda()

    # Train model
    t_total = time.time()
    logger.info("training...")
    
    # model sub folder
    model_dir = 'model/%s' % (dataset_name)
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    bad_counter = 0
    best_epoch = 0
    best_acc = 0. 
    for epoch in range(args.epochs):
        cur_acc = train(epoch, train_dict, val_dict, test_dict, model, optimizer, class_weights)
        # if args.mylog:
        model_file = '%s/%s.pkl' % (model_dir, epoch)
        torch.save(model.state_dict(), model_file)

        if cur_acc > best_acc:
            best_acc = cur_acc
            best_epoch = epoch
            bad_counter = 0
        else:
            bad_counter += 1
        if bad_counter == args.patience:
            break

    # remove other models
    files = glob.glob(model_dir+'/*.pkl')
    for file in files:
        filebase = os.path.basename(file)
        epoch_nb = int(filebase.split('.')[0])
        if epoch_nb != best_epoch:
            os.remove(file)

    logger.info("Training Finished!")
    logger.info("optimization Finished!")
    logger.info("total time elapsed: {:.4f}s".format(time.time() - t_total))
    logger.info("Load best model and test......")
    logger.info("Best epoch {}".format(best_epoch))
    model.load_state_dict(torch.load(model_dir+'/{}.pkl'.format(best_epoch)))

    logger.info("testing...")
    prec, rec, f1, acc, auc = evaluate(epoch+1, model, test_dict, class_weights, log_desc = 'test_')

    # record model's testing performance
    if not os.path.exists("result/"):
        os.mkdir("result/")
    with open("result/test_result.txt", 'a') as f:
        now = datetime.datetime.now()
        f.write(f"{now}, {dataset_name}: precision: {prec:.2f}, rec: {rec:.2f}, F1: {f1:.2f}, \
                accuracy: {acc:.2f}, AUC: {auc:.2f}\n")

if __name__ == "__main__":
    country_ls = ["Taiwan", "Thailand"]
    date_period = '2018-2022'
    lead_day_ls = [1, 2, 3, 4, 5]
    for one_country in country_ls:
        for one_lead_day in lead_day_ls:
            dataset_name = one_country+date_period+'l'+str(one_lead_day)
            trainProcess(dataset_name)