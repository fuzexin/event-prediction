# -*- coding: utf-8 -*-
from __future__ import absolute_import  # 引用Python自带的库
from __future__ import unicode_literals #在python3中默认的编码采用了unicode
from __future__ import division #导入python未来支持的语言特征division(精确除法)
from __future__ import print_function #在python2.X，使用print就得像python3.X那样加括号使用

import ptvsd
ptvsd.enable_attach(address = ('172.20.201.90', 3000))
ptvsd.wait_for_attach() 

'''
以上应该是为兼容pyhon2.0，引用系统自带库等作用
'''

import os, itertools, random, argparse, time, datetime

'''
os: 操作系统相关
itertools: 内置库，迭代器相关
random: for random numbers
argparse: arguments parse
time : for time usageasd
datetime: datetime usage
'''


'''
this project needs three third party library
:
PyTorch >= 1.0: for neural networks

sklearn: marchine learning and data mining

pytorch_sparse: sparse matrix(稀疏矩阵) operation library

'''
os.environ["CUDA_VISIBLE_DEVICES"]='1'
#set program visible gpu
import torch
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import DataLoader
import numpy as np
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import roc_auc_score, accuracy_score
from sklearn.metrics import precision_recall_curve

import scipy.sparse as sp
#scipy: 包含致力于科学计算中常见问题的各个工具箱
from utils import *
#utils: 本地文件夹下的（自己编写的）常用函数 
from models import *
#models: 本地文件夹下的（自己编写的）模型 
import shutil
#shutil: 高级的 文件、文件夹、压缩包 处理模块

import glob
#文件名模式匹配
from tensorboardX import SummaryWriter
#Pytorch的可视化工具
import pickle

# Training settings
ap = argparse.ArgumentParser()
ap.add_argument('--dataset', type=str, default='THAD6h', help="dataset string")
#数据集
ap.add_argument('--embedding', type=str, default='thailand', help="word embedding string")
ap.add_argument('--tensorboard_log', type=str, default='', help="name of this run (use timestamp instead)")#运行的名称
ap.add_argument('--seed', type=int, default=42, help='random seed')#随机种子
ap.add_argument('--epochs', default=1000, type=int, help='number of epochs to train')#迭代次数
ap.add_argument('--batch', type=int, default=1, help="batch size (due to sparse matrix operations)")#并行大小
ap.add_argument('--lr', type=float, default=1e-3, help='initial learning rate')#初始的学习率
ap.add_argument('--weight_decay', type=float, default=5e-4, help='weight decay (L2 loss on parameters)')#权值衰减
ap.add_argument('--dropout', type=float, default=0.2, help='dropout rate (1 - keep probability)')#丢失率
ap.add_argument('--f_dim', type=int, default=100, help="feature dimensions of graph nodes") #图节点的特征维数
ap.add_argument('--n_hidden', default=7, type=int, help='number of hidden layers')#隐藏层的层数
ap.add_argument('--n_class', type=int, default=1, help="number of class (default 1)") #类的数量
ap.add_argument('--check_point', type=int, default=1, help="check point")#检查点
ap.add_argument('--model', default='DynamicGCN', choices=['DynamicGCN','GCN'], help='')#训练模型设置
ap.add_argument('--shuffle', action='store_false', default=True, help="Shuffle dataset 0/1")#数据洗牌，是否打乱顺序//modified!
ap.add_argument('--train', type=float, default=.825, help="training ratio (0, 1)")#训练率
ap.add_argument('--val', type=float, default=.175, help="validation ratio (0, 1)")#检验率
ap.add_argument('--test', type=float, default=.0, help="testing ratio (0, 1) test file is seperated")#测试率
ap.add_argument('--fastmode', action='store_true', default=False, help='validate during training')#是否在训练过程中验证
ap.add_argument('--mylog', action='store_false', default=True,  help='tensorboad log')#tensorboad log，应该是日志记录
ap.add_argument('--patience', type=int, default=10,  help='patience for early stop')#patience for early stop ? 停止的等待最大时间？（未确定）
ap.add_argument('--cuda', action='store_false', default=True, help='use cuda')#是否使用cuda？


args = ap.parse_args()
#以上应该为所有的参数设置
print('--------------Parameters--------------')
print(args)
print('--------------------------------------')
np.random.seed(args.seed)
args.cuda = args.cuda and torch.cuda.is_available() 
if args.cuda:
    torch.cuda.manual_seed(args.seed)

if args.model == 'DynamicGCN':
    train_dict, val_dict, test_dict, pretrained_emb = load_sparse_temporal_data(args.dataset, args.embedding, args.f_dim, args.train, args.val, args.test)
    #pretrained_emb:预训练的嵌入词
    
else:
    train_dict, val_dict, test_dict, pretrained_emb = load_dynamic_graph_data(args.dataset, args.embedding, args.f_dim, args.train, args.val, args.test)

if args.cuda:
    pretrained_emb = pretrained_emb.cuda()

if args.model == 'DynamicGCN':
    model = DynamicGCN(pretrained_emb=pretrained_emb,
                            n_output=args.n_class, #类的数量
                            n_hidden=args.n_hidden, #hidden layer 隐藏层
                            dropout=args.dropout) #丢失率
else:
    model = GCN(pretrained_emb=pretrained_emb, 
                n_output=args.n_class, 
                dropout=args.dropout)

if args.cuda:
    model.cuda()
# optimizer and loss
optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr, weight_decay=args.weight_decay)
pytorch_total_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
print('total number of parameters:',pytorch_total_params)

if 'THAI' in args.embedding:
    class_weights = torch.FloatTensor([0.5, 0.5])
    #class_weights = torch.FloatTensor([0.38, 0.62])# positive number vs negative numbers
elif 'HK' in args.embedding:
    class_weights = torch.FloatTensor([0.8, 0.2])
elif 'TW' in args.embedding:
    class_weights = torch.FloatTensor([0.8, 0.2])
elif 'CAMB' in args.embedding:
    class_weights = torch.FloatTensor([0.66, 0.34])
elif 'INDIA' in args.embedding:
    class_weights = torch.FloatTensor([0.80, 0.20])
else:
    class_weights = torch.FloatTensor([0.5, 0.5])
if args.cuda:
    class_weights = class_weights.cuda()

# Train model
model_dir = "/data/fuzexin/Program/DynamicGCN_server_"\
    "Version3/model/THAI13C7L_100_DynamicGCN_1604838453_THAI13C7L"
best_epoch = 4
model.load_state_dict(torch.load(model_dir+'/{}.pkl'.format(best_epoch)))

#----------------------------------handle data-------------------------------------------------------
weight_all_word = model.mask.weight.cpu().detach().numpy()
weight_all_word = list(weight_all_word)
print(weight_all_word)


#aquire key words 25% ahead weight
key_word = []
for i in range(int(len(weight_all_word)*0.25)):
    record_max_index = weight_all_word.index(max(weight_all_word))
    key_word.append(record_max_index)
    weight_all_word.pop(record_max_index)

print(key_word)
deposit_path = "/data/fuzexin/Program/DynamicGCN_server_Version3/export_data"
with open(os.path.join(deposit_path, "THAI13C7L_key_word.pkl"), 'wb') as f:
    pickle.dump(key_word, f)

print("handle successfully!")

#evaluate(epoch+1, test_dict, log_desc = 'test_')

