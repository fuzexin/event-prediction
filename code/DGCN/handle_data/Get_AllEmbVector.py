i = 0
limit = 2
""" 
    

    存入格式为字典 eg: "the": [0.345, 0.675, ... , 0.543]

    词嵌入存放的文件叫做：word.emb_100

"""

import numpy as np
import pickle
import logging
#获取整个 word-Embedding dictionary
logging.basicConfig(level = logging.DEBUG)
def getWordEmbedding(glove_file):  
  word_emb_dict = {}  
  with open(glove_file,'r', encoding = "utf-8") as f:  
      for line in f.readlines():  
          first_space_index = line.index(' ')  
          word = line[:first_space_index]  
          values = line[first_space_index + 1:]  
          vector = np.fromstring(values, sep=' ', dtype=np.float16).tolist()
          word_emb_dict[word] = vector  
  return word_emb_dict
def test(data_file):
    with open(data_file, "rb") as word_em_file:
        temp_data = pickle.load(word_em_file)
    print (temp_data)


all_word_emb = getWordEmbedding(\
    "E:\zxf\GdeltProject\DynamicGCNCode\Data\glove_6B\glove.6B.100d.txt")
# logging.debug(all_word_emb)
with open("E:\zxf\GdeltProject\DynamicGCNCode\HandleData_Experiment\process_data\word_emb_100.pkl", "wb") as data_file:
     pickle.dump(all_word_emb, data_file)

test("E:\zxf\GdeltProject\DynamicGCNCode\HandleData_Experiment\process_data\word_emb_100.pkl")


