import Get_ActorName1
import Get_y3
import SearchEmb2
import Get_index4
import Get_x5
import DeleteNullValue
import os,sys, pickle
from enum import Enum
import logging
import datetime

# import debugpy
# debugpy.listen(('172.20.201.133', 5678))
# debugpy.wait_for_client()

logging.basicConfig(level=logging.DEBUG)
class data_field(Enum):
    GlobalEventID = 0
    Actor1Name = 1
    EventCode = 2
    Actor2Name = 3 
    AllNames = 4
    NumArticles = 5 
    IsRootEvent= 6 
    EventDate = 7

def genTrainData(data_path, deposit_path, data_name, seq_len, lead_len):
    
    # get data path
    file_list = []
    for root, _, files in os.walk(data_path):
        for one_file in files:
            file_list.append(os.path.join(root, one_file))
    
    # get city data
    # We should garantee the file data is continueous in date
    all_data = []
    for one_file in file_list:
        with open(one_file, 'rb') as f:
            one_city_data = pickle.load(f)
        
        # 将数据按日期分入列表
        cls_date_list = []
        begin_idx = 0
        for end_idx in range(len(one_city_data)+1):
            if end_idx == len(one_city_data) or one_city_data[begin_idx][data_field.EventDate.value] != one_city_data[end_idx][data_field.EventDate.value]:
                cls_date_list.append(one_city_data[begin_idx: end_idx])
                begin_idx = end_idx
        
        # 插入为空数据日期的数据
        supply_empt_list = []
        for i in range(len(cls_date_list)-1):
            supply_empt_list.append(cls_date_list[i])
            # 判断两天的数据是否相差天数
            f_date = datetime.datetime.strptime(cls_date_list[i][0][data_field.EventDate.value],'%Y-%m-%d').date()
            n_date =  datetime.datetime.strptime(cls_date_list[i+1][0][data_field.EventDate.value],'%Y-%m-%d').date()
            intvl = (n_date - f_date).days - 1
            if intvl > 0 :
                for j in range(1, intvl+1):
                    supply_empt_list.append([])
        # 添加最后一天的数据
        supply_empt_list.append(cls_date_list[-1])

        # 按序列长度按天对数据进行组织
        fmted_city_data = []
        for i in range(len(supply_empt_list)-seq_len-lead_len+1):  
            fmted_city_data.append(supply_empt_list[i:i+seq_len+lead_len])
        
        # 删除掉标签天数据为空, 或者输入序列数据为空的样本
        flter_fmt_data = []
        for i in range(len(fmted_city_data)):
            # 计算输入序列数据量
            s_num = 0
            for j in range(seq_len):
                s_num += len(fmted_city_data[i][j])
            
            if len(fmted_city_data[i][-1]) == 0 or s_num == 0:
                continue
            flter_fmt_data.append(fmted_city_data[i])

        all_data.append(flter_fmt_data)
    logging.debug("check all_data: {}".format(all_data))
    Get_y3.getY(all_data, deposit_path, data_field)
    Get_ActorName1.getActorNameList(all_data, deposit_path, data_field)
    SearchEmb2.getANEmbedding(deposit_path, data_name)
    Get_index4.getindex(all_data, deposit_path, seq_len, data_field)
    Get_x5.getX(all_data, deposit_path, seq_len, data_field)
    DeleteNullValue.reformData(deposit_path, data_name)


def genPredictData(data_path, deposit_path, data_name, seq_len, lead_len):

    # get data path
    file_list = []
    for root, _, files in os.walk(data_path):
        for one_file in files:
            file_list.append(os.path.join(root, one_file))
    
    # get city data
    # We should garantee the file data is continueous in date
    all_data = []
    for one_file in file_list:
        with open(one_file, 'rb') as f:
            one_city_data = pickle.load(f)
        
        fmted_city_data = []
        for i in range(len(one_city_data)-seq_len-lead_len+1):  
            fmted_city_data.append(one_city_data[i:i+seq_len+lead_len])
        all_data.append(fmted_city_data)

    Get_y3.getY(all_data, deposit_path, data_field)
    Get_ActorName1.getActorNameList(all_data, deposit_path, data_field)
    SearchEmb2.getANEmbedding(deposit_path, data_name)
    Get_index4.getindex(all_data, deposit_path, seq_len, data_field)
    Get_x5.getX(all_data, deposit_path, seq_len, data_field)
    
if __name__ == "__main__":

    data_dir = '/data/fuzexin/project/event_pred_research2024/data/south_east_asia'
    deposit_dir = '/data/fuzexin/project/event_pred_research2024/code/DGCN/data'
    seq_len = 7
    lead_len_ls = [1, 2, 3, 4, 5]
    for lead_len in lead_len_ls:
        # dataset_list = ['Indonesia', 'Malaysia', 'Philippine', 'Singapore']
        dataset_list = ["Taiwan", "Thailand"]
        date_period = '2018-2022'
        for one_dataset in dataset_list:
            data_path = os.path.join(data_dir, one_dataset, date_period)
            data_name = one_dataset+ date_period + f'l{lead_len}'
            deposit_path = os.path.join(deposit_dir, data_name)
            if not os.path.exists(deposit_path):
                os.mkdir(deposit_path)
            genTrainData(data_path, deposit_path, data_name, seq_len, lead_len)


    # print("To use this program, please set some arguments first!\n1,original data path:")
    # data_path = input()
    # print("2,deposit the processed data path:")
    # deposit_path = input()
    # print("3,input the lead day:(leaving empty will set this argument 5 )")
    # lead_day = input()
    # if len(lead_day)==0:
    #     lead_day = 5
    # else:
    #     lead_day = int(lead_day)
    # print("4,input data name:(for example NewDelhi20C5L)")
    # data_name = input()
    # print("be sure for all argument:\n1,data_path:{},\n2,deposit_path:{},\n3,lead_day:{},\n4,data_name:{}\n"\
    #     .format(data_path, deposit_path, lead_day, data_name))
    
    # print("for sure, please input keys y/Y, not please input other keys:")
    # enter_key = input()
    # if enter_key in ('y','Y'):
    #     genTrainData(data_path, deposit_path, lead_day, data_name)
    # else:
    #     sys.exit()

    