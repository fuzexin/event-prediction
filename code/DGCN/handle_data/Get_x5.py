"""
    this code is for getting x, idx, y data
    x: adjacency matrixes
    idx: word idx
    y: ground truth
"""

import pickle
import numpy as np
from scipy.sparse import csr_matrix
import os


def getX(all_data, deposit_path, seq_len, data_field):

    #aquire all Actorname data
    actorname_file_path = os.path.join(deposit_path, "word_list.pkl")
    with open(actorname_file_path, 'rb') as all_actorname_file:
        actorname_file_data = pickle.load(all_actorname_file)

    #aquire chain word index data
    index_file_path = os.path.join(deposit_path, "index.pkl")
    with open(index_file_path, 'rb') as index_file:
        word_index_data = pickle.load(index_file)


    chain_count = 0
    x = []

    for one_city_data in all_data: 
        for one_chain_data in one_city_data: # one chain
            index_list = word_index_data[chain_count].tolist()
            edge_weight_list = []
            for one_day_data in one_chain_data[:seq_len]: # one day
                edge_weight_oneday = []
                for one_data in one_day_data: # one data
                    Actor1Name = one_data[data_field.Actor1Name.value]
                    Actor2Name = one_data[data_field.Actor2Name.value]
                    NumArticles = one_data[data_field.NumArticles.value]
                    if(  (len(Actor1Name)>0) and (len(Actor2Name)>0) ):
                        Actor1Name_index = index_list.index(actorname_file_data[Actor1Name])
                        Actor2Name_index = index_list.index(actorname_file_data[Actor2Name])
                        if(NumArticles > 0):
                            edge_weight_oneday.append([Actor1Name_index, Actor2Name_index, NumArticles])
                            edge_weight_oneday.append([Actor2Name_index, Actor1Name_index, NumArticles])
                        else:
                            edge_weight_oneday.append([Actor1Name_index, Actor2Name_index, 1])
                            edge_weight_oneday.append([Actor2Name_index, Actor1Name_index, 1])
                edge_weight_list.append(edge_weight_oneday)
                
            #calculate d(i), d(j), D
            d_ij = {}
            D = 0
            for one_day_data in edge_weight_list:

                for one_piece_data in one_day_data:

                    if(one_piece_data[0] not in d_ij.keys()):
                        d_ij[one_piece_data[0]] = one_piece_data[2]
                    else:
                        d_ij[one_piece_data[0]] += one_piece_data[2]
                    
                    if(one_piece_data[1] not in d_ij.keys()):
                        d_ij[one_piece_data[1]] = one_piece_data[2]
                    else:
                        d_ij[one_piece_data[1]] += one_piece_data[2]
                    D += one_piece_data[2]
                
            one_chain_matrixes = []
            for one_day_data2 in edge_weight_list:
                
                dict_data = {}
                for one_piece_data in one_day_data2:
                    if( (one_piece_data[0], one_piece_data[1]) not in dict_data):
                        dict_data[(one_piece_data[0], one_piece_data[1])] = one_piece_data[2]
                    else:
                        dict_data[(one_piece_data[0], one_piece_data[1])] += one_piece_data[2]
                data = []
                row = []
                col = []
                for piece_dict_data in dict_data:
                    row.append(piece_dict_data[0])
                    col.append(piece_dict_data[1])
                    value = ( dict_data[piece_dict_data] * D)/(d_ij[piece_dict_data[0]] * d_ij[piece_dict_data[1]])
                    data.append(value)
                
                one_day_csrmat = csr_matrix((data, (row, col)), shape=(len(index_list), len(index_list)), dtype = np.float64)
                one_chain_matrixes.append(one_day_csrmat)
            if(len(one_chain_matrixes) < 7 and len(one_chain_matrixes) > 0):
                matrix_row_num = matrix_column_num = len(index_list)
                zero_matrix = csr_matrix(([0], ([0], [0])), shape=(matrix_row_num, matrix_column_num), dtype = np.float64)
                left_num = 7 - len(one_chain_matrixes)
                for i in range(left_num):
                    one_chain_matrixes.append(zero_matrix)
                print("too little matrixes! mark{}".format(chain_count))
                
            elif(len(one_chain_matrixes) == 7):
                pass
            else:
                print("too many matrixes! mark{}".format(chain_count))
            
            #validate
            # if(one_chain_matrixes[0].shape[0] != len(index_list)):
            #     print("Wrong dimension, {}".format(chain_count))
            #     print(index_list)
            x.append(one_chain_matrixes)
            chain_count += 1


    print("total chain number is: {}".format(chain_count))
    x = np.array(x)
    # print(x)
    x_file_path = os.path.join(deposit_path, "x.pkl")
    with open(x_file_path, "wb") as x_file:
        pickle.dump(x, x_file)
    
    print("Aquire x file process successfully finished!")
    


if __name__ == "__main__":
    file_list =[
        "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contras"
            "tExperiment\Thain_PartClass\chain_neg_Bangkok_1.pkl",
        "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contr"
            "astExperiment\Thain_PartClass\chain_neg_Chiang_1.pkl",
        "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contrast"
            "Experiment\Thain_PartClass\chain_pos_Bangkok_1.pkl",
        "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contrast"
            "Experiment\Thain_PartClass\chain_pos_Chiang_1.pkl"
    ]

    deposit_path = "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contrast"+\
        "Experiment\Thain_PartClass\ProcessedData_l3"
    
    getX(file_list, deposit_path, 3)