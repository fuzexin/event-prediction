import pickle
import random
import os
import shutil

def reformData(deposit_path, data_name):
    #getData
    all_index_file_path = os.path.join(deposit_path, "index.pkl")
    with open(all_index_file_path, "rb") as index_file:
        index_data1 = pickle.load(index_file)

    all_x_file_path = os.path.join(deposit_path, "x.pkl")
    with open(all_x_file_path, "rb") as x_file:
        x_data1 = pickle.load(x_file)

    all_y_file_path = os.path.join(deposit_path, "y.pkl")
    with open(all_y_file_path, "rb") as y_file:
        y_data1 =pickle.load(y_file)
    print(len(y_data1))
    condition = lambda t: len(t[0]) > 1
    z = list(zip(index_data1, x_data1, y_data1))
    z = list(filter(condition, z))
    random.shuffle(z)

    index_data, x_data, y_data = zip(*z)
    print(len(y_data))
    data_length = len(index_data)
    cut_point = int(data_length * 0.2)

    test_index_data = index_data[:cut_point]
    test_x_data = x_data[:cut_point]
    test_y_data = y_data[:cut_point]

    train_index_data = index_data[cut_point:]
    train_x_data = x_data[cut_point:]
    train_y_data = y_data[cut_point:]

    # storage index data
    train_index_path = os.path.join(deposit_path, "ind."+ data_name +".idx")
    test_index_path = os.path.join(deposit_path, "ind."+ data_name +".tidx")
    with open(train_index_path, "wb") as train_idx_file:
        pickle.dump(train_index_data, train_idx_file)
    with open(test_index_path, "wb") as test_idx_file:
        pickle.dump(test_index_data, test_idx_file)

    #storage x data
    train_x_path = os.path.join(deposit_path, "ind."+ data_name +".x")
    test_x_path = os.path.join(deposit_path, "ind."+ data_name +".tx")
    with open(train_x_path, "wb") as train_x_file:
        pickle.dump(train_x_data, train_x_file)
    with open(test_x_path, "wb") as test_x_file:
        pickle.dump(test_x_data, test_x_file)

    #storage y data
    train_y_path = os.path.join(deposit_path, "ind."+ data_name +".y")
    test_y_path = os.path.join(deposit_path, "ind."+ data_name +".ty")
    with open(train_y_path, "wb") as train_y_file:
        pickle.dump(train_y_data, train_y_file)
    with open(test_y_path, "wb") as test_y_file:
        pickle.dump(test_y_data, test_y_file)

    print("reform all data successfully!")
    print("{:*^20}".format("end"))


if __name__ == "__main__":
    deposit_path = "E:\zxf\GdeltProject\DynamicGCNCode\FromDN"+\
        "_data\ContrastExperiment\Thain_PartClass\ProcessedData_l3"
    data_name ="THAI13C3L"
    reformData(deposit_path, data_name)



