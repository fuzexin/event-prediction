import numpy as np
import pickle
import random

#test file data proportion is 20%
#about three file 

#read data
with open(r"E:\zxf\GdeltProject\DynamicGCNCode\PreprocessedData\index.pkl", "rb") as index_file:
    index_data = pickle.load(index_file)

with open(r"E:\zxf\GdeltProject\DynamicGCNCode\PreprocessedData\x.pkl","rb") as x_file:
    x_data = pickle.load(x_file)

with open(r"E:\zxf\GdeltProject\DynamicGCNCode\PreprocessedData\y.pkl", "rb") as y_file:
    y_data =pickle.load(y_file)

z = list(zip(index_data, x_data, y_data))
random.shuffle(z)
index_data[:], x_data[:], y_data[:] = zip(*z)

data_length = len(index_data)
cut_point = int(data_length * 0.2)

test_index_data = index_data[:cut_point]
test_x_data = x_data[:cut_point]
test_y_data = y_data[:cut_point]

train_index_data = index_data[cut_point:]
train_x_data = x_data[cut_point:]
train_y_data = y_data[cut_point:]

# storage index data
with open(r"E:\zxf\GdeltProject\DynamicGCNCode\PreprocessedData\train_data\ind.THAD6h.idx", "wb") as train_idx_file:
    pickle.dump(train_index_data, train_idx_file)
with open(r"E:\zxf\GdeltProject\DynamicGCNCode\PreprocessedData\train_data\ind.THAD6h.tidx", "wb") as test_idx_file:
    pickle.dump(test_index_data, test_idx_file)

#storage x data
with open(r"E:\zxf\GdeltProject\DynamicGCNCode\PreprocessedData\train_data\ind.THAD6h.x", "wb") as train_x_file:
    pickle.dump(train_x_data, train_x_file)
with open(r"E:\zxf\GdeltProject\DynamicGCNCode\PreprocessedData\train_data\ind.THAD6h.tx", "wb") as test_x_file:
    pickle.dump(test_x_data, test_x_file)

#storage y data
with open(r"E:\zxf\GdeltProject\DynamicGCNCode\PreprocessedData\train_data\ind.THAD6h.y", "wb") as train_y_file:
    pickle.dump(train_y_data, train_y_file)
with open(r"E:\zxf\GdeltProject\DynamicGCNCode\PreprocessedData\train_data\ind.THAD6h.ty", "wb") as test_y_file:
    pickle.dump(test_y_data, test_y_file)

print("{:*^20}".format("end"))



