"""

获取关键事件数据给王寅森

"""
import pickle
import json
import os

file_path_neg = r"E:\zxf\GdeltProject\DynamicGCNCode\FromDN"\
    r"_data\DGCN—dataset-11.6\Thailand\20Class\chain_neg_Bangkok.pkl"
# 泰国20类负例数据
with open(file_path_neg, 'rb') as f:
    all_neg_chain_data = pickle.load(f)
file_path_pos = r"E:\zxf\GdeltProject\DynamicGCNCode\FromDN_"\
    r"data\DGCN—dataset-11.6\Thailand\20Class\chain_pos_Bangkok.pkl"
# 泰国20类正例数据
with open(file_path_pos, 'rb') as f:
    all_pos_chain_data = pickle.load(f)
# 获取关键节点数据
file_path_kw = "E:\zxf\GdeltProject\DynamicGCNCode\HandleData_"\
    "Experiment\process_data\THAI20C5L_kw0_1\key_word_detail.txt"
with open(file_path_kw, 'r') as f:
    kw_data = f.read()
kw_data = eval(kw_data)

# 获取关键正例数据
all_pos_chain_data = all_pos_chain_data[0:50]
key_pos_chain_data = []
for one_chain_data in all_pos_chain_data:
    one_chain_data = one_chain_data[2:9]
    key_one_chain_data = []
    for one_day_data in one_chain_data:
        key_one_day_data = []
        for one_data in one_day_data:
            actor1name = one_data["Actor1Name"]
            actor2name = one_data["Actor2Name"]
            if( (actor1name in kw_data.values()) or \
                (actor2name in kw_data.values())
                ):
                key_one_day_data.append(one_data)
        key_one_chain_data.append(key_one_day_data)
    key_pos_chain_data.append(key_one_chain_data)
# print(key_pos_chain_data)  # test ok

# 获取关键正例数据
all_neg_chain_data = all_neg_chain_data[0:50]
key_neg_chain_data = []
for one_chain_data in all_neg_chain_data:
    one_chain_data = one_chain_data[2:9]
    key_one_chain_data = []
    for one_day_data in one_chain_data:
        key_one_day_data = []
        for one_data in one_day_data:
            actor1name = one_data["Actor1Name"]
            actor2name = one_data["Actor2Name"]
            if( (actor1name in kw_data.values()) or \
                (actor2name in kw_data.values())
                ):
                key_one_day_data.append(one_data)
        key_one_chain_data.append(key_one_day_data)
    key_neg_chain_data.append(key_one_chain_data)

# print(key_neg_chain_data)  # test ok
# key_neg_chain_data = json.dumps(key_neg_chain_data)
# print(key_neg_chain_data)

event_dir = r"E:\zxf\GdeltProject\DynamicGCNCode\HandleData"\
    r"_Experiment\process_data\THAI20C5L_kevent50"
pos_event_path = os.path.join(event_dir, "thai50_pos.pkl")
with open(pos_event_path, 'wb') as f:
    pickle.dump(key_pos_chain_data, f)
neg_event_path = os.path.join(event_dir, "thai50_neg.pkl")
with open(neg_event_path, 'wb') as f:
    pickle.dump(key_neg_chain_data, f)
print("process successfully!")








