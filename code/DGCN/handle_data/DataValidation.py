import pickle
import os
import numpy as np
from scipy.sparse import csr_matrix

base_dir = r"E:\zxf\GdeltProject\DynamicGCNCode\PreprocessedData"
x_filename = r"x.pkl"
y_filename = r"y.pkl"
idx_filename = r"index.pkl"

with open(os.path.join(base_dir, x_filename), "rb") as x_file:
    x_data = pickle.load(x_file)

with open(os.path.join(base_dir, y_filename), "rb") as y_file:
    y_data = pickle.load(y_file)

with open(os.path.join(base_dir, idx_filename), "rb") as idx_file:
    idx_data = pickle.load(idx_file)

for i in range(len(x_data)):
    if(len(x_data[i]) != 7):
        print("{}th number of matrix list is: {}".format(i, len(x_data[i])))

    for j in range(7):
        #check every matrix's dimension
        if(x_data[i][j].shape != x_data[i][0].shape):
            print("i: {}, j: {}, dimension not correct!".format(i, j))   

#validate length of x, y, idx data
print("x data length: {}, y data length: {}, idx data length: {}".format(len(x_data), len(y_data), len(idx_data) ))
if(len(x_data) == len(y_data) == len(idx_data)):
    print("length correct!")

#validate matrix's dimension is equal to idx's length
count_unmatch_num = 0
count_match_num = 0
for i in range(len(x_data)):
    if(x_data[i][0].shape[0] != len(idx_data[i])):
        print("{}th length not matching!".format(i))
        count_unmatch_num += 1
    else:
        # print("{}th length matching!".format(i))
        count_match_num += 1
print("unmatching number is {}".format(count_unmatch_num))  
print("matching number is {}".format(count_match_num))   
