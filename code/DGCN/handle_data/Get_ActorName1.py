""" 
    This code's main purpose is to get all different ActorName in Event chain data
    
    every event chain data contains 7 GDELT struct data

    the 7 GDELT data may be NONE or a intact GDELT record

    1 GDELT record has two field need to acquire: Actor1Name, Actor2Name

"""

import pickle
import os

def getActorNameList(all_data, deposit_path, data_field):
    word_dict = {} #storage word and its index, like "government": 1
    #aquire actor name data
    for one_pos_data in all_data:
        # one position data = many chains data
        for one_chain_data in one_pos_data:
            # one chain's data  = seq_len + lead_len day data
            for one_day_data in one_chain_data:
                for one_event in one_day_data:
                            Actor1Name = one_event[data_field.Actor1Name.value]
                            Actor2Name = one_event[data_field.Actor2Name.value]
                            if( (len(Actor1Name)>0) and (Actor1Name not in word_dict.keys()) ):
                                word_dict[Actor1Name] = len(word_dict)
                            if( (len(Actor2Name)>0) and (Actor2Name not in word_dict.keys()) ):
                                word_dict[Actor2Name] = len(word_dict)
    # print(word_dict)
    # storage in disk
    storage_file = os.path.join(deposit_path, "word_list.pkl")
    with open(storage_file, "wb") as word_dict_file:
        pickle.dump(word_dict, word_dict_file)
    print("Aquire Actorname process successfully finished!")

if __name__ == "__main__":
    #----for different data set, need modify--- 
    file_list =[
        r"E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contras"
            r"tExperiment\Thain_PartClass\chain_neg_Bangkok_1.pkl",
        r"E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contr"
            r"astExperiment\Thain_PartClass\chain_neg_Chiang_1.pkl",
        r"E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contrast"
            r"Experiment\Thain_PartClass\chain_pos_Bangkok_1.pkl",
        r"E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contrast"
            r"Experiment\Thain_PartClass\chain_pos_Chiang_1.pkl"
    ]

    deposit_path = r"E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contrast"+\
        r"Experiment\Thain_PartClass\ProcessedData_l3"
    
    getActorNameList(file_list, deposit_path, 3)
    