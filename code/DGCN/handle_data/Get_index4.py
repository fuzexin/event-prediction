"""
    this code is for getting x, idx, y data
    x: adjacency matrixes
    idx: word idx
    y: ground truth
"""

import pickle
import numpy as np
import os

def getindex(all_data, deposit_path, seq_len, data_field):
    #aquire all Actorname data
    actorname_file_path = os.path.join(deposit_path, "word_list.pkl")
    with open(actorname_file_path, 'rb') as all_actorname_file:
        actorname_file_data = pickle.load(all_actorname_file)

    index_list = []
    # all_data  = many cities data
    for one_pos_data in all_data:
        # one position data = many chains data
        for one_chain_data in one_pos_data:
            chain_word_set = set()
            # one chain's data  = seq_len + lead_len day data
            for one_day_data in one_chain_data[:seq_len]:
                for one_event in one_day_data:
                    Actor1Name = one_event[data_field.Actor1Name.value]
                    Actor2Name = one_event[data_field.Actor2Name.value]
                    if(  (len(Actor1Name)>0) and (Actor1Name in actorname_file_data.keys())  ):
                        chain_word_set.add(actorname_file_data[Actor1Name])
                    if( (len(Actor2Name)>0) and (Actor2Name in actorname_file_data.keys()) ):
                        chain_word_set.add(actorname_file_data[Actor2Name])
            index_list.append(np.array(list(chain_word_set)))

    #print(index_list)

    index_list = np.array(index_list)
    print(index_list)
    idx_file_path = os.path.join(deposit_path, "index.pkl")
    with open(idx_file_path, "wb") as index_file:
        pickle.dump(index_list, index_file)
    print("Aquire index file process successfully finished!")


if __name__ == "__main__":
    file_list =[
        
        "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contras"
            "tExperiment\Thain_PartClass\chain_neg_Bangkok_1.pkl",
        "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contr"
            "astExperiment\Thain_PartClass\chain_neg_Chiang_1.pkl",
        "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contrast"
            "Experiment\Thain_PartClass\chain_pos_Bangkok_1.pkl",
        "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contrast"
            "Experiment\Thain_PartClass\chain_pos_Chiang_1.pkl"
    ]
    deposit_path = \
        "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\ContrastExperiment\Thain_PartClass\ProcessedData_l3"
    getindex(file_list, deposit_path, 3)




