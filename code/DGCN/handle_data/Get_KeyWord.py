import pickle, os
import torch

def genKeyWord(model_path, wdict_path, key_rate, kw_depos_path):
    assert key_rate > 0 and key_rate < 1
    # read model's mask.weight parameter
    state_data = torch.load(model_path, map_location = torch.device('cpu'))  # ok
    #print(type(state_data))
    #print(state_data['mask.weight'])  # load successfully!
    all_weight = state_data['mask.weight']
    all_weight = all_weight.cpu().detach().numpy()
    all_weight = list(all_weight)
    # print(all_weight)
    # aquire key words 25% ahead weight
    key_word = []
    for _ in range(int(len(all_weight) * key_rate)):
        record_max_index = all_weight.index(max(all_weight))
        key_word.append(record_max_index)
        all_weight.pop(record_max_index)
        if len(key_word) > 50:
            break
    with open(wdict_path, 'rb') as f:
        all_word_dict = pickle.load(f)
    # print("the type of word_dict: ", type(all_word_dict))
    key_word_detail = {}
    for single_word in all_word_dict:
        if all_word_dict[single_word] in key_word:
            key_word_detail[all_word_dict[single_word]] = single_word
    # print(key_word)    
    with open(kw_depos_path, 'w') as f:
        # pickle.dump(key_word, f)
        f.write(str(key_word_detail))
    print("get {:.0%} key word successfully!".format(key_rate))

def genKeySubgraph(kw_path, wdict_path, data_list, store_dir):
    with open(kw_path, 'rb') as f:
        key_word = pickle.load(f)
    with open(wdict_path, 'rb') as f:
        all_word_dict = pickle.load(f)
    print("the type of word_dict: ", type(all_word_dict))
    print("the type of key_word: ", type(key_word))
    key_word_detail = {}
    for single_word in all_word_dict:
        if all_word_dict[single_word] in key_word:
            key_word_detail[all_word_dict[single_word]] = single_word
            #key_word_detail.append(single_word)
            #print(single_word)
    #print(key_word_detail)
    # for single_key_word in key_word[0:10]:
    #     print(key_word_detail[single_key_word]

    #get key_word for every chain data
    #read data
    test_index_path = data_list[0]
    with open(test_index_path, "rb") as test_index_file:
        index_data = pickle.load(test_index_file)

    test_x_file_path = data_list[1]
    with open(test_x_file_path, "rb") as test_x_file:
        x_data = pickle.load(test_x_file)

    test_y_file_path = data_list[2]
    with open(test_y_file_path, "rb") as test_y_file:
        y_data = pickle.load(test_y_file)
    print(len(y_data))
    condition = lambda t: t[2][0] == 1
    z = list(zip(index_data, x_data, y_data))
    z = list(filter(condition, z))
    index_data, x_data, y_data = zip(*z)
    # print(len(y_data))
    # print(y_data)
    
    # get key word for every day
    key_index_data = []
    for one_chain_index in index_data:
        one_chain_key_index = []
        for one_index in one_chain_index:
            if one_index in key_word:
                one_chain_key_index.append(one_index)
        key_index_data.append(one_chain_key_index)

    index_flag = 0
    key_edge = []
    for one_chain_x in x_data:
        single_index = index_data[index_flag]
        single_key_index = key_index_data[index_flag]
        key_edge_one_chian = []
        for one_day_x in one_chain_x:
            # print(one_day_x)
            dict_one_day_x = one_day_x.todok()      # sparse matrix to dict
            key_edge_one_day = {}
            for pos in dict_one_day_x.keys():
                if((single_index[pos[0]] in single_key_index)
                    and (single_index[pos[1]] in single_key_index)):
                    key_edge_one_day[(single_index[pos[0]],
                    single_index[pos[1]])] = dict_one_day_x[pos]
            key_edge_one_chian.append(key_edge_one_day)
        key_edge.append(key_edge_one_chian)
        index_flag += 1

    # print(key_edge)
    
    # storage data for visualization
    # key_word_detail {12: "Trump"}, 
    # key_index_data, [[32, 12, 71, 45], ... , [12, 3, 5, 90]]
    # key_edge, 
    # [ [{(12, 47): 1.234, ... , (68, 43): 0.234}, ... ,{}], [{}, ... ,{}] ]
    key_word_detail_path = os.path.join(store_dir, "key_word_detail.txt")
    key_index_data_path = os.path.join(store_dir, "key_index_data.txt")
    key_edge_path = os.path.join(store_dir, "key_sub_edge.txt")

    with open(key_word_detail_path, 'w') as f:
        # json.dump(key_word_detail, f, cls=MyEncoder)
        f.write(str(key_word_detail))
    with open(key_index_data_path, 'w') as f:
        # json.dump(key_index_data, f, cls=MyEncoder)
        f.write(str(key_index_data))
    with open(key_edge_path, 'w') as f:
        # json.dump(key_edge, f, cls=MyEncoder)
        f.write(str(key_edge))       

def genKeySubgraph2(kw_detail_path, chain_data_path, deposit_filepath, lead_day):
    with open(kw_detail_path, 'r') as f:
        kw_detail = f.read()
    kw_detail = eval(kw_detail)
    with open(chain_data_path, 'rb') as f:
        chain_data = pickle.load(f)
    subgraph_data = []
    for one_chain_data in chain_data:
        one_chain_subgraph = []
        for one_day_data in one_chain_data[(7-lead_day) : (14-lead_day)]:
            one_day_subgraph = {}
            for one_data in one_day_data:
                if(one_data):
                    Actor1Name = one_data["Actor1Name"]
                    Actor2Name = one_data["Actor2Name"]
                    EventCode = one_data["EventCode"]
                    node1 = node2 =""
                    if(  (len(Actor1Name)>0) and (Actor1Name in kw_detail.values())):
                        node1 = Actor1Name
                        node2 = Actor2Name #modified!
                    elif( (len(Actor2Name)>0) and (Actor2Name in kw_detail.values()) ):
                        node1 = Actor1Name # modified!
                        node2 = Actor2Name
                    if( (len(node1) != 0) or (len(node2) != 0) ):
                        one_day_subgraph[(node1, node2)] = EventCode
            one_chain_subgraph.append(one_day_subgraph)
        subgraph_data.append(one_chain_subgraph)
    print(subgraph_data)
    with open(deposit_filepath, 'w') as f:
        f.write(str(subgraph_data))
    print("----end-----")

if __name__ == "__main__":

    # model_path = r"/data/zxf/MultiModelPredict/data/RecordAndConfig/DGCN/New Delhi/model_para.pkl"
    # kw_path = r"/data/zxf/MultiModelPredict/data/RecordAndConfig/DGCN/New Delhi/kw_detail.txt"
    # wdict_path = r"/data/zxf/MultiModelPredict/data/RecordAndConfig/DGCN/New Delhi/word_list.pkl"
    # genKeyWord(model_path, wdict_path, 0.1, kw_path)

    prefix_dir = '/data/fuzexin/project/event_pred_research2024/code/DGCN'
    date_period = '2018-2022'
    lead_day = 1
    country_ls = ["Thailand", "Taiwan"]
    for one_country in country_ls:
        model_base_dir = prefix_dir+f"/model/{one_country}{date_period}l{lead_day}"
        data_base_dir = prefix_dir+f"/data/{one_country}{date_period}l{lead_day}"
        model_path = model_base_dir +"/model_para1.pkl"
        kw_path = data_base_dir + '/kw_detail.txt'
        wdict_path = data_base_dir + '/word_list.pkl'
        genKeyWord(model_path, wdict_path, 0.1, kw_path)





