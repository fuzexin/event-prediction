"""
    this code is for getting x, idx, y data
    x: adjacency matrixes
    idx: word idx
    y: ground truth
"""

import pickle
import numpy as np
import os


def getY(all_data, deposit_path, data_field):
    y = []
    count_neg = count_pos = 0
    # all_data = many cities data
    for one_pos_data in all_data:
        # one position data = many chains data
        for one_chain_data in one_pos_data:
            label = False
            # the last day event is the event happened day
            for one_event in one_chain_data[-1]:
                if one_event[data_field.EventCode.value][:2] == '14':
                    label = True
                    break
            if label == False:
                y.append([0])
                count_neg += 1
            else:
                y.append([1])
                count_pos += 1
    
    y = np.array(y)
    y_file_path = os.path.join(deposit_path, "y.pkl")
    with open(y_file_path, "wb") as y_file:
        pickle.dump(y, y_file)
    str1 = "positive: {}, negative: {}, all = {}\n".\
        format(count_pos, count_neg, (count_pos+count_neg))
    #print(str1)
    str2 = "positive : negtive = {} : {}\n".\
        format(count_pos/(count_pos + count_neg), count_neg/(count_pos + count_neg))
    with open(os.path.join(deposit_path, "data_specification.txt"), "a") as f:
        f.write(str1)
        f.write(str2)
    #print("format is : {}".format(str2))
    print("Aquire y file process successfully finished!")

if __name__ == "__main__":
    file_list =[
    "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contras"
        "tExperiment\Thain_PartClass\chain_neg_Bangkok_1.pkl",
    "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contr"
        "astExperiment\Thain_PartClass\chain_neg_Chiang_1.pkl",
    "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contrast"
        "Experiment\Thain_PartClass\chain_pos_Bangkok_1.pkl",
    "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contrast"
        "Experiment\Thain_PartClass\chain_pos_Chiang_1.pkl"
    ]
    deposit_path = "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\ContrastExperiment\Thain_PartClass\ProcessedData_l3"
    getY(file_list, deposit_path)