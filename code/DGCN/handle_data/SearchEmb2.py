""" 
    this code's purpose is to find all words' ebedding in ActorName dict

    storage in list, its shape is 
    [length of ActorName dict, 100(the length of single embeding)]

    storage in file ActorName_embeding.pkl
"""

import pickle
import logging
import pickle
import numpy as np
import os

logging.basicConfig(level = logging.INFO)

#get all words embedding from data
# emb_file_path = "E:\zxf\GdeltProject\DynamicGCNCode\PreprocessedData\word_emb_100.pkl"
# TODO check the file exist
emb_file_path = "./support/word_emb_100.pkl"

with open(emb_file_path, 'rb') as all_emb_file:
    emb_file_data = pickle.load(all_emb_file)

def getANEmbedding(deposit_path, data_name):
    #aquire all Actorname data
    #modified!-----------------------------------------------------------------
    actorname_file_path = os.path.join(deposit_path, "word_list.pkl")
    with open(actorname_file_path, 'rb') as all_actorname_file:
        actorname_file_data = pickle.load(all_actorname_file)
    logging.info("number of all words is {}".format(len(actorname_file_data)))
    #search all embedding for creating ActorName embeding file
    ActorName_embeding = list(range(len(actorname_file_data)))
    not_get_num = 0
    for single_actorname in actorname_file_data:
        name_parts = single_actorname.split()
        parts_emb_vector = [0.0 for i in range(100)]
        for part in name_parts:
            part = part.lower()
            if(part in emb_file_data.keys()):
                parts_emb_vector = [i+j for i, j in zip(parts_emb_vector, emb_file_data[part])]
            else:
                logging.info(part)           

        if(sum(parts_emb_vector) != 0):
            parts_emb_vector = [i/len(name_parts) for i in parts_emb_vector]
        else:
            # record the number of not searched words
            not_get_num += 1
        #ActorName_embeding.append(parts_emb_vector)
        ActorName_embeding[actorname_file_data[single_actorname]] = parts_emb_vector
    logging.debug("all actor name are:\n{}\nend-".format(ActorName_embeding))
    logging.info("\n unget embedding word are:{}".format(not_get_num))
    ActorName_embeding = np.array(ActorName_embeding, dtype = np.float32)
    # storage actor name embedding data in file
    an_emb_file = os.path.join(deposit_path,  data_name +".emb_100")
    with open(an_emb_file, "wb") as Name_emb_file:
        pickle.dump(ActorName_embeding, Name_emb_file)

    logging.info("Aquire ActorName Embedding successfully finished!")

if __name__ == "__main__":
    deposit_path = "E:\zxf\GdeltProject\DynamicGCNCode\FromDN_data\Contras" +\
            "tExperiment\Thain_PartClass\ProcessedData_l3"
    getANEmbedding(deposit_path)



    


