import debugpy
# Allow other computers to attach to debugpy at this IP address and port.
debugpy.listen(('172.20.201.133', 5678))
# Pause the program until a remote debugger is attached
debugpy.wait_for_client()
print("test1\n"*10)
print("test2\n"*10)
print("test3\n"*10)
print("test4\n"*10)
print("test5\n"*10)
print("test6\n"*10)