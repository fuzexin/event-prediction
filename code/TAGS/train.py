from __future__ import division
from __future__ import print_function

import argparse
import os
import time
import torch.optim as optim
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support
from models import *

import debugpy
# Allow other computers to attach to debugpy at this IP address and port.
debugpy.listen(('172.20.201.137', 5678))
# Pause the program until a remote debugger is attached
debugpy.wait_for_client()

def train(epoch, batch_size):
    loss, count = 0., 0
    model.train()
    iteration = int(len(y_list) * 0.9) // batch_size
    t1 = time.time()
    for it in tqdm(range(iteration), desc='train iteration'):
        loss_train = 0
        optimizer.zero_grad()
        for i in range(it * batch_size, (it + 1) * batch_size):
            output = model(edge_index_data[i], edge_type_data[i],
                           node_idx_data[i], edge_idx_data[i])
            loss_train += F.binary_cross_entropy(output, y_list[i], weight=class_weights[int(y_list[i].item())])
            count += 1
        loss_train.backward()
        optimizer.step()
        loss += loss_train
    acc, prec, rec, f1 = evaluate()
    print('Epoch: {:03d}'.format(epoch + 1),
          'time: {:.4f}s'.format(time.time() - t1),
          'loss_train: {:.4f}'.format(loss / count),
          'precision_test: {:.4f}'.format(prec),
          'recall_test: {:.4f}'.format(rec),
          'f1_test: {:.4f}'.format(f1))

    return acc, prec, rec, f1


def evaluate():
    model.eval()
    y_true, y_pred = [], []
    for i in range(int(len(y_list) * 0.9), len(y_list)):
        output = model(edge_index_data[i], edge_type_data[i],
                       node_idx_data[i], edge_idx_data[i])
        y_true += y_list[i].data.tolist()
        bi_val = np.where(output.data.cpu().numpy() > 0.5, 1, 0)
        y_pred += torch.from_numpy(bi_val).tolist()
    prec, rec, f1, _ = precision_recall_fscore_support(y_true, y_pred, average="binary")
    acc = accuracy_score(y_true, y_pred)
    return acc, prec, rec, f1


parser = argparse.ArgumentParser()
parser.add_argument('--gpu_idx', type=int, default=0)
parser.add_argument('--seed', type=int, default=42)
parser.add_argument('--n_feature', type=int, default=100)
parser.add_argument('--n_convs', type=int, default=1, choices=[1, 2, 3])
parser.add_argument('--n_encoders', type=int, default=1, choices=[1, 2, 3, 6])
parser.add_argument('--len_seq', type=int, default=7)
parser.add_argument('--n_heads', type=int, default=8, choices=[1, 2, 4, 8, 16])
parser.add_argument('--n_feedforward', type=int, default=64)
parser.add_argument('--batch_size', type=int, default=8)
parser.add_argument('--epochs', type=int, default=200)
parser.add_argument('--dropout', type=float, default=0.5)
parser.add_argument('--n_output', type=int, default=1)
parser.add_argument('--lr', type=float, default=5e-4, choices=[1e-3, 5e-4, 1e-4])
parser.add_argument('--weight_decay', type=float, default=5e-3, choices=[5e-3, 5e-4])
args = parser.parse_args()

os.environ["CUDA_VISIBLE_DEVICES"] = str(args.gpu_idx)

args.cuda = torch.cuda.is_available()


file_list = [
             ['India', '2019-2022']]

data_dir = "/nfs/home/fzx/project/EPredict2023/data"
deposit_train_data = '/nfs/home/fzx/project/EPredict2023/data'

for [country, year] in file_list:
    result_list = []
    for i in range(5):

        # ---handle data
        print(args)
        # conjecture edge_index_data is (enode1, enode2),  edge_type_data is 141 etc., node_idx_data is ?, y_list is 0 or 1. 
        entity_size, relation_size, edge_index_data, edge_type_data, node_idx_data, y_list = preprocess(country, year, args.len_seq, data_dir, deposit_train_data)
        # ---- DEBUG
        # for i in range(len(edge_index_data)):
        #     if len(edge_index_data[i])==0:
        #         print(i)
        edge_index_data, edge_type_data, node_idx_data, y_list = shuffle(args.seed, edge_index_data, edge_type_data, node_idx_data, y_list)
        edge_idx_data = get_edge_idx_data(edge_type_data)
        pos, neg = count(y_list)
        class_weights = torch.FloatTensor([pos/(pos+neg), neg/(pos+neg)])
        print('Class Weights: {}'.format(class_weights))
        np.random.seed(args.seed)
        torch.manual_seed(args.seed)
        if args.cuda:
            print('GPU: {} {}'.format(torch.cuda.get_device_name(), args.gpu_idx))
            torch.cuda.manual_seed(args.seed)
            for sample in range(len(y_list)):
                y_list[sample] = y_list[sample].cuda()
                for day in range(args.len_seq):
                    try:
                        edge_index_data[sample][day] = edge_index_data[sample][day].cuda()
                        edge_type_data[sample][day] = edge_type_data[sample][day].cuda()
                        node_idx_data[sample][day] = node_idx_data[sample][day].cuda()
                        edge_idx_data[sample][day] = edge_idx_data[sample][day].cuda()
                    except Exception:
                        pass
        
        # train model
        model = TAGS_online(entity_size=entity_size,
                            relation_size=relation_size,
                            n_feature=args.n_feature,
                            n_convs=args.n_convs,
                            n_encoders=args.n_encoders,
                            n_heads=args.n_heads,
                            n_feedforward=args.n_feedforward,
                            n_output=args.n_output,
                            dropout=args.dropout)
        optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
        parameters_num = sum(p.numel() for p in model.parameters() if p.requires_grad)
        if args.cuda:
            model.cuda()
            class_weights = class_weights.cuda()

        print('Location/Samples: {}/{}'.format(country, len(y_list)))
        print('Model Parameters: {:.4f}M'.format(parameters_num / 1000000.0))
        if not os.path.exists('model/{}/'.format(country)):
            os.makedirs('model/{}/'.format(country))
        print("----------------Training-----------------")
        max_f1, max_epoch, bad_count = 0., 0, 0
        t1 = time.time()
        for epoch in range(args.epochs):
            acc, prec, rec, f1 = train(epoch, args.batch_size)
            if f1 >= max_f1:
                max_f1, max_epoch, bad_count = f1, epoch, 0
                indicators = [acc, prec, rec, f1]
                torch.save(model.state_dict(), 'model/{}/{}_{}_{}.pkl'.format(country, country, args.len_seq, i))
            else:
                bad_count += 1
            if (epoch + 1) % 10 == 0 and epoch > 0:
                print('Best epoch: {:03d}'.format(max_epoch + 1),
                      'Best F1: {:.4f}'.format(max_f1))
            if epoch >= 100 and bad_count >= 50:
                print("------------Early Stop------------")
                break
        print("Training time: {:.4f}s".format(time.time()-t1))
        print("Best epoch: {:03d}".format(max_epoch + 1),
              "Best F1: {:.4f}".format(max_f1))
        result_list.append(indicators)
        

        if not os.path.exists('results/'):
            os.makedirs('results/')
        with open('results/{}_{}.pkl'.format(country, args.len_seq), 'wb') as f:
            pkl.dump(result_list, f)