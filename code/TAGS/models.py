from layers import *
import torch.nn as nn


class TAGS_modularize(nn.Module):
    def __init__(self, entity_emb, relation_emb, n_convs, n_encoders, n_heads, n_feedforward, n_output, dropout, explain=False):
        super(TAGS_modularize, self).__init__()
        self.num_relations = get_minimum_multiples(relation_emb.size(0), n_heads)
        self.num_nodes = get_minimum_multiples(entity_emb.size(0), n_heads)
        self.n_input = entity_emb.size(1)

        self.graph_processor = Graph_Processor(entity_emb, relation_emb, n_output, n_convs, n_heads, dropout)
        self.encoder1_list, self.encoder2_list = nn.ModuleList(), nn.ModuleList()
        for i in range(n_encoders):
            self.encoder1_list.append(Encoder_Layer(self.num_nodes, n_heads, n_feedforward, dropout))
            self.encoder2_list.append(Encoder_Layer(self.num_relations, n_heads, n_feedforward, dropout))
        self.predict = Predictor(self.num_nodes, self.num_relations, self.num_nodes+self.num_relations, n_output, dropout)
        self.explain = explain

    def forward(self, edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
        seq_emb1, seq_emb2 = self.graph_processor(edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq)
        for encoder1, encoder2 in zip(self.encoder1_list, self.encoder2_list):
            enc_output1, a1 = encoder1(seq_emb1)
            enc_output2, a2 = encoder2(seq_emb2)
        pred_emb = torch.cat([enc_output1, enc_output2], dim=1)
        output = self.predict(pred_emb)
        if self.explain:
            actor_contribution = self.predict.explain(pred_emb, node_idx_seq, edge_idx_seq)
            return output, actor_contribution
        else:
            return output


class TAGS_online(nn.Module):
    def __init__(self, entity_size, relation_size, n_feature, n_convs, n_encoders, n_heads, n_feedforward, n_output, dropout):
        super(TAGS_online, self).__init__()
        self.num_nodes = get_minimum_multiples(entity_size, n_heads)
        self.num_relations = get_minimum_multiples(relation_size, n_heads)

        self.graph_processor = Graph_Processor_online(entity_size, relation_size, n_feature, n_output, n_convs, n_heads, dropout)
        self.encoder1_list, self.encoder2_list = nn.ModuleList(), nn.ModuleList()
        for i in range(n_encoders):
            self.encoder1_list.append(Encoder_Layer(self.num_nodes, n_heads, n_feedforward, dropout))
            self.encoder2_list.append(Encoder_Layer(self.num_relations, n_heads, n_feedforward, dropout))
        self.predict = Predictor(self.num_nodes, self.num_relations, self.num_nodes+self.num_relations, n_output, dropout)

    def forward(self, edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq):
        seq_emb1, seq_emb2 = self.graph_processor(edge_index_seq, edge_type_seq, node_idx_seq, edge_idx_seq)
        for encoder1, encoder2 in zip(self.encoder1_list, self.encoder2_list):
            enc_output1, a1 = encoder1(seq_emb1)
            enc_output2, a2 = encoder2(seq_emb2)
        output = self.predict(torch.cat([enc_output1, enc_output2], dim=1))

        return output