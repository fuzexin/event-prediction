import numpy as np
import pickle as pkl
import os
import random
from tqdm import tqdm

from torch.nn import Parameter
from torch.nn.init import *


def process_list(lis):
    return sorted(list(set(lis)))


def get_minimum_multiples(n_nodes, n_heads):
    while n_nodes % n_heads != 0:
        n_nodes += 1
    return n_nodes


def shuffle(rand_seed, edge_index_data, edge_type_data, node_idx_data, y_list):
    random.seed(rand_seed)
    zip_list = list(zip(edge_index_data, edge_type_data, node_idx_data, y_list))
    random.shuffle(zip_list)
    edge_index_data, edge_type_data, node_idx_data, y_list = zip(*zip_list)
    edge_index_data, edge_type_data, node_idx_data, y_list = list(edge_index_data), list(edge_type_data), list(
        node_idx_data), list(y_list)

    return edge_index_data, edge_type_data, node_idx_data, y_list


def cut_dataset(k, old_list):
    length = len(old_list)
    test_list = old_list[int(0.1 * k * length): int(0.1 * (k + 1) * length)]
    del old_list[int(0.1 * k * length): int(0.1 * (k + 1) * length)]
    new_list = old_list + test_list

    return new_list


def cross_cut(k, edge_index_data, edge_type_data, node_idx_data, y_list):
    edge_index_data = cut_dataset(k, edge_index_data)
    edge_type_data = cut_dataset(k, edge_type_data)
    node_idx_data = cut_dataset(k, node_idx_data)
    y_list = cut_dataset(k, y_list)

    return edge_index_data, edge_type_data, node_idx_data, y_list

###############
def get_edge_idx_data(edge_type_data):
    # deduplicate and sort day's event-type  
    edge_idx_data = []
    for seq_edge_type in edge_type_data:
        seq_edge_idx = []
        for edge_type in seq_edge_type:
            seq_edge_idx.append(get_edge_idx(edge_type))
        edge_idx_data.append(seq_edge_idx)
    return edge_idx_data


def get_param(shape):
    param = Parameter(torch.Tensor(*shape))
    xavier_normal_(param.data)  # 正态分布
    # xavier_uniform_(param.data)  # 均匀分布
    # kaiming_uniform_(param.data, a=math.sqrt(5))
    return param


def get_positional_encoding(len_seq, embed_dim):
    positional_encoding = np.array([
        [pos / np.power(10000, 2 * i / embed_dim) for i in range(embed_dim)]
        if pos != 0 else np.zeros(embed_dim) for pos in range(len_seq)])
    positional_encoding[1:, 0::2] = np.sin(positional_encoding[1:, 0::2])
    positional_encoding[1:, 1::2] = np.cos(positional_encoding[1:, 1::2])
    return torch.tensor(positional_encoding)


def com_mult(a, b):
    r1, i1 = a[..., 0], a[..., 1]
    r2, i2 = b[..., 0], b[..., 1]
    return torch.stack([r1 * r2 - i1 * i2, r1 * i2 + i1 * r2], dim=-1)


def conj(a):
    a[..., 1] = -a[..., 1]
    return a


def cconv(a, b):
    return torch.irfft(com_mult(torch.rfft(a, 1), torch.rfft(b, 1)), 1, signal_sizes=(a.shape[-1],))


def ccorr(a, b):
    return torch.irfft(com_mult(conj(torch.rfft(a, 1)), torch.rfft(b, 1)), 1, signal_sizes=(a.shape[-1],))


def find_file(filepath, match_string):
    file_list = []
    names = os.listdir(filepath)
    for name in names:
        if match_string in name:
            file_list.append(name)
    file_list = process_list(file_list)
    return file_list


def Get_EventMapping():
    event_mapping = {}
    with open('support/CAMEO.eventcodes.txt', 'r', encoding='utf-8') as f:
        lines = f.readlines()
    for i, line in enumerate(lines):
        if i > 0:
            split_line = line.strip().split('\t')
            event_code = split_line[0]
            event_type = split_line[1]
            event_mapping[event_code] = event_type

    return event_mapping


def process_original_data(country, year, data_dir):
    """ read data file and store it in [[pos1], [pos2], ...], pos1 = [[day0's data], [day1's data], ...] """
    all_data = []
    file_list = find_file('{}/{}/{}/'.format(data_dir, country, year), '.pkl')
    for file in file_list:
        data, date_list = [], []
        with open('{}/{}/{}/{}'.format(data_dir, country, year, file), 'rb') as f:
            orginal_data = pkl.load(f)
        for event in orginal_data:
            date_list.append(event[-1])
        date_list = process_list(date_list)
        for date in date_list:
            day_list = []
            for event in orginal_data:
                if event[-1] == date:
                    day_list.append(event)
            data.append(day_list)
        all_data.append(data)
    return all_data


def get_actor_event(data):
    """ get actor name, event code and event description text. """
    actor_list, eventcode_list, event_list = [], [], []
    event_mapping = Get_EventMapping()
    for idx, day_list in enumerate(data):
        for event in day_list:
            if event[2] != '---':
                actor_list.append(event[1])
                actor_list.append(event[3])
                eventcode_list.append(event[2])
    eventcode_list = process_list(eventcode_list)
    for eventcode in eventcode_list:
        event_list.append(event_mapping[eventcode])
    return process_list(actor_list), eventcode_list, event_list


def get_labels(data, len_seq):
    label_list = []
    for idx, day_list in enumerate(data):
        if idx >= len_seq:
            protest_count = 0
            for event in day_list:
                if event[2].startswith('14'):
                    protest_count += 1
                    break
            if protest_count > 0:
                label_list.append(torch.tensor([1.]))
            else:
                label_list.append(torch.tensor([0.]))
    return label_list


def get_node_idx(edge_index):
    idx = []
    for node_list in edge_index:
        for node in node_list:
            idx.append(node.item())
    return torch.tensor(process_list(idx))


def get_edge_idx(edge_type):
    idx = []
    for edge in edge_type:
        idx.append(edge.item())
    return torch.tensor(process_list(idx))


def get_graphs(data, actor_list, eventcode_list, label_list, len_seq):
    # edge_index_data is list of many len_seq length day edge_indexs [e0, e1, ..., ], one is like (1, 2) , 
    # edge_type_data ie like up,  one edge is like 141, 
    # node_idx_data is also like up, one is like 1.   
    edge_index_data, edge_type_data, node_idx_data = [], [], []
    edge_indexs, edge_types, node_idxs = [], [], []
    for i in tqdm(range(len(data)), desc='processing graphs...'):
        edge_index, edge_type = [], []
        for event in data[i]:
            if (actor_list.index(event[1]), actor_list.index(event[3])) not in edge_index and event[2] != '---':
                edge_index.append((actor_list.index(event[1]), actor_list.index(event[3])))
                edge_type.append(eventcode_list.index(event[2]))
        edge_index = torch.LongTensor(edge_index).t()
        edge_type = torch.LongTensor(edge_type)
        node_idx = get_node_idx(edge_index)
        edge_indexs.append(edge_index)
        edge_types.append(edge_type)
        node_idxs.append(node_idx)
    for i in range(len(label_list)):
        edge_index_data.append(edge_indexs[i:i + len_seq])
        edge_type_data.append(edge_types[i:i + len_seq])
        node_idx_data.append(node_idxs[i:i + len_seq])

    return len(actor_list), len(eventcode_list), edge_index_data, edge_type_data, node_idx_data


def preprocess(country, year, len_seq, data_dir, deposit_dir):
    all_data = process_original_data(country, year, data_dir)
    all_actor_list, all_eventcode_list, all_event_list, all_label_list = [], [], [], []
    for data in all_data:
        actor_list, eventcode_list, event_list = get_actor_event(data)
        label_list = get_labels(data, len_seq)
        all_actor_list += actor_list
        all_eventcode_list += eventcode_list
        all_event_list += event_list
        all_label_list += label_list
    all_actor_list, all_eventcode_list, all_event_list = process_list(all_actor_list), process_list(all_eventcode_list), process_list(all_event_list)
    all_edge_index_data, all_edge_type_data, all_node_idx_data = [], [], []
    for data in all_data:
        label_list = get_labels(data, len_seq)
        entity_size, relation_size, edge_index_data, edge_type_data, node_idx_data = get_graphs(data, all_actor_list, all_eventcode_list, label_list, len_seq)
        all_edge_index_data += edge_index_data
        all_edge_type_data += edge_type_data
        all_node_idx_data += node_idx_data
    with open('{0}/{1}/actor_event.pkl'.format(deposit_dir, country), 'wb') as f:
        pkl.dump([all_actor_list, all_eventcode_list], f)
    return len(all_actor_list), len(all_event_list), all_edge_index_data, all_edge_type_data, all_node_idx_data, all_label_list


def count(y_list):
    pos_count, neg_count = 0, 0
    for y in y_list:
        if y.item() > 0.:
            pos_count += 1
        else:
            neg_count += 1
    return pos_count, neg_count