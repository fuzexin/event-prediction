from __future__ import division
from __future__ import print_function

import argparse
import os


### interaction
def getTAGSPredictData(country, gdelt_path, depos_path):
    with open('data/EventData/{}/actor_event.pkl'.format(country), 'rb') as f:
        actor_list, eventcode_list = pkl.load(f)
    with open(gdelt_path, 'rb') as f:
        data = pkl.load(f)
    edge_indexs, edge_types, node_idxs = [], [], []
    for day_event in data:
        edge_index, edge_type = [], []
        for event in day_event:
            if event[2] in eventcode_list:
                edge_type.append(eventcode_list.index(event[2]))
                if event[1] in actor_list and event[3] in actor_list:
                    edge_index.append((actor_list.index(event[1]), actor_list.index(event[3])))
                elif event[1] in actor_list and event[3] not in actor_list:
                    edge_index.append((actor_list.index(event[1]), 0))
                elif event[1] not in actor_list and event[3] in actor_list:
                    edge_index.append((0, actor_list.index(event[3])))
        edge_index = torch.LongTensor(edge_index).t()
        edge_type = torch.LongTensor(edge_type)
        node_idx = get_node_idx(edge_index)
        edge_indexs.append(edge_index)
        edge_types.append(edge_type)
        node_idxs.append(node_idx)
    edge_idxs = get_edge_idx_data([edge_types])[0]
    with open(depos_path, 'wb') as f:
        pkl.dump((edge_indexs, edge_types, node_idxs, edge_idxs), f)
    return len(actor_list), len(eventcode_list)


def predTAGSData(model, model_path, pred_path):
    model.load_state_dict(torch.load(model_path, map_location=torch.device('cpu')))
    model.eval()
    with open(pred_path, 'rb') as f:
        edge_indexs, edge_types, node_idxs, edge_idxs = pkl.load(f)
    pred_p = model(edge_indexs, edge_types, node_idxs, edge_idxs)
    return pred_p.item()


parser = argparse.ArgumentParser()
parser.add_argument('--gpu_idx', type=int, default=0)
parser.add_argument('--seed', type=int, default=42)
parser.add_argument('--n_feature', type=int, default=100)
parser.add_argument('--n_convs', type=int, default=1, choices=[1, 2])
parser.add_argument('--n_encoders', type=int, default=1, choices=[1, 2, 3, 6])
parser.add_argument('--len_seq', type=int, default=14)
parser.add_argument('--n_heads', type=int, default=8)
parser.add_argument('--n_feedforward', type=int, default=64)
parser.add_argument('--dropout', type=float, default=0.5)
parser.add_argument('--n_output', type=int, default=1)
args = parser.parse_args()

os.environ["CUDA_VISIBLE_DEVICES"] = str(args.gpu_idx)
from models import *
args.cuda = torch.cuda.is_available()


entity_size, relation_size = getTAGSPredictData('America', 'data/OriginalChainData/TAGS/Hong Kong/2023/04/Hong Kong20230413.pkl', 'data/Hong Kong20230413_test.pkl')

model = TAGS_online(entity_size=entity_size,
                    relation_size=relation_size,
                    n_feature=args.n_feature,
                    n_convs=args.n_convs,
                    n_encoders=args.n_encoders,
                    n_heads=args.n_heads,
                    n_feedforward=args.n_feedforward,
                    n_output=args.n_output,
                    dropout=args.dropout)

y = predTAGSData(model, 'model/America/America_7_0.pkl', 'data/Hong Kong20230413_test.pkl')
print(y)