# record

Wangyinsen的数据集合构建

时间: 从2019年9月24日至2022年9月24日.  

地点:  

- 美国（US）：考虑到美国不同地区间的地域文化差异较大，构建数据集时将采集数据的州按地理位置分布划分为东部和西部两个数据集，其中东部（USE）包括从纽约及其周边的宾夕法尼亚、康涅狄格和马萨诸塞共四个州采集的数据，西部（USW）包括从加利福尼亚及其周边的内华达和亚利桑那共三个州采集的数据。
- 英国（UK）：数据集采自于伦敦、爱丁堡、剑桥、曼彻斯特、贝尔法斯特、格拉斯哥等英国的主要城市或郡。
- 俄罗斯（RU）：数据集采自于莫斯科、圣彼得堡等主要州，考虑到俄罗斯国内的人口分布相对较为集中，因此将上述采集的数据合并为一个数据集。
- 印度（IND）：同样考虑到印度地区之间的文化差异较大，因此将采集的数据集按邦所处的地理位置分布划分为北部和南部两个数据集，其中北部（INDN）数据集主要采自于新德里及其周边地区，南部（INDS）数据集主要采自于孟买及其周边地区。

## early experiment

SQL statement of extract dataset from GDELT:  

```sql
    SELECT GlobalEventID, Actor1Name, EventCode, Actor2Name, EventDate
    FROM gdelt.event 
    where EventDate = '{get_date}' AND  ActionGeo_ADM1Code = '{ADM1Code}' AND ActionGeo_FullName ilike '%{city_name}%' AND IsRootEvent=1 AND Actor1Name != '' AND EventCode != '' AND Actor2Name != '' AND SOURCEURL != ''
```

used countries and cities:

```python
country_city = {
    'America':[('Chicago', 'USIL'), ('Los Angeles', 'USCA'), ('New York', 'USNY'), ('San Francisco', 'USCA'), ('Washington', 'USDC')],
    'UK':[('London', 'UKH9'), ('Edinburgh','UKU8'), ('Manchester', 'UKI2')],
    "Russia": [('Moscow', 'RS48'), ('Sankt-Petersburg', 'RS66')],
    'India': [('Bombay', 'IN16'), ('New Delhi', 'IN07'), ('Calcutta', 'IN28'), ('Chennai', 'IN25'), ('Bangalore', 'IN19')],
}
```

experiment result(F1):  
| country | Wang's data | this data |
| - | - | - |  
| America  | 0.77  | 0.85 |  
| Russia  | 0.85  | 0.96 |  
| India  | 0.74  | 0.66  |  
| UK  | 0.79  | 0.83 |  
| Philippines  | -  | 0.48 |  

## HAFL model performence comparing  

### HAFL model architechture  

#### find the most suit model and training code  

train.py v.s. train_items.py  

the major difference between these two file is that, train.py uses `TAGS_modularize` model, but train_items.py uses `TAGS_online` model.  

And `TAGS_modularize` has implemented prediction explain, and in which, entity and relation embeddings are generated from given embedding. In `TAGS_online`, entity and relation embeddings are generated from random function.  

train_online.py vs train_online2.py  

In these two files, all models are `TAGS_online`, except this, they have not much differences.  

So, summarize before, we used train_items.py code files to analyse the model architecture.  

#### TAGS_online architechture

## Models Performance Comparing  

The PURPOSE: comprehensive compare model HAFL, CMF, DGCN.  

### Dataset construction

Region Include:  

```python
{
    'Egypt':[('Abuja', 'NI11'), ('Alexandria', 'EG06'), ('Buhari', 'NI29'), ('Cairo', 'EG11'), ('Lagos', 'NI05')],
    'Thailand': [('Bangkok', 'TH40'), ('ChiangMai', 'TH02'), ('ChiangRai', 'TH03'), ('Pattaya', 'TH46'), ('Phuket', 'TH62')],
    "Russia": [('Moscow', 'RS48'), ('Sankt-Petersburg', 'RS66')],
    'India': [('Bombay', 'IN16'), ('New Delhi', 'IN07'), ('Calcutta', 'IN28'), ('Chennai', 'IN25'), ('Bangalore', 'IN19')],
    'Japan':[('Tokyo', 'JA40'), ('Yokohama', 'JA19'), ('Osaka', 'JA32'), ('Nagoya', 'JA01'), ('Kobe', 'JA13')],
    'America':[('Chicago', 'USIL'), ('Los Angeles', 'USCA'), ('New York', 'USNY'), ('San Francisco', 'USCA'), ('Washington', 'USDC')],
    'UK':[('London', 'UKH9'), ('Edinburgh','UKU8'), ('Manchester', 'UKI2')]
}
```

After data aquired, Finding that some cities' data are a few, so substracting some cities. The final data region are below:  

```python
{
    'Egypt':[('Abuja', 'NI11'), ('Alexandria', 'EG06'), ('Cairo', 'EG11'), ('Lagos', 'NI05')],
    'Thailand': [('Bangkok', 'TH40')],
    "Russia": [('Moscow', 'RS48')],
    'India': [('New Delhi', 'IN07'),('Chennai', 'IN25')],
    'Japan':[('Tokyo', 'JA40')],
    'America':[('Chicago', 'USIL'), ('Los Angeles', 'USCA'), ('New York', 'USNY'), ('San Francisco', 'USCA'), ('Washington', 'USDC')],
    'UK':[('London', 'UKH9'), ('Edinburgh','UKU8'), ('Manchester', 'UKI2')]
}
```

sample fields:  
GlobalEventID, Actor1Name, EventCode, Actor2Name, AllNames, NumArticles, RootEvent, EventDate  

storage data of **a city** as a file.  

date period: 20180101--20221231

### train condition

1. positive and negtive setting rule: 1, if oneday has a target event which also is RootEvent, beside that, is negtive; 2, oneday having a target event will be seen as positive, besides as negtive.  
2. date span: three or five years.  
3. train order: 1, train data using data date before, and test using data time after. 2, disorder date order, using five-fold validation.  
4. train citys: use one city or more than one city. (haven't implement)  

### experiment record

| country | city | date period | model name | sample quantity | positive rate | positive setting rule | model parameter quantity | loop | batch size | epoch | patience | class_weight | train order |
| - | - | - | - | - | - | - | - | - | - | - | - | - | - |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |

#### CMF

test training command: `python train_pred.py -d Egypt -l 1 --epochs 1`  
experiment training command: `python train_pred.py -d Egypt --gpu 0`
first step:  
basic trainning has accomplished: Thailand, Japan, India, Egypt;  
use `--chs_root` to set sample label considering IsRootEvent also has accomplished: Thailand, Japan, India, Egypt.  

next step: we will consider training order.  

```shell
# reconstruct sen2vec model  
python getTextToken.py -d /nfs/home/fzx/project/EPredict2023/data/model_comparing/America/2018-2022 -p /nfs/home/fzx/project/EPredict2023/text/code/CMF/data/America

./fasttext sent2vec -input /nfs/home/fzx/project/EPredict2023/text/code/CMF/data/support/America/text_token.txt -lr 0.2 -lrUpdateRate 100 -dim 300 -epoch 5 -minCount 8 -minCountLabel 0 -neg 10 -wordNgrams 3 -loss ns -bucket 2000000 -thread 2  -t 0.0001 -dropoutK  4 -verbose 2 -numCheckPoints 1 -output  /nfs/home/fzx/project/EPredict2023/text/code/CMF/data/support/America/s2v_300  

```

#### HAFL

debug training command: `python train_items.py -d Thailand -l 1 --epochs 1`
experiemnt training command: `python train_items.py -d Thailand --gpu 0`  

### 讨论

包含CMF数据集, 包含WYS的数据集. 为什么是哪些城市. 构建模式是否要改变.  

### Findings

In CMF, after every training epoch, CMF use validate loss to choose epoch model, HAFL use validate F1 to choose epoch model. From analyse the model's performance, we find HAFL's performance F1 score can reach to 0.77, recall reach to 0.93, but negtive recall just reach to 0.25 in 0.59 positive class weight dataset.  

## Literature Reading  

## ITEMS SUPPORT

### 增加预测城市- 2024年4月16日

| 国家 | 城市 | ActionGeo_ADM1Code |
| - | - | - |  
| Philippine | Manila | RPD9 |  
| Philippine | Cebu | RP21|  
| Philippine | Clark | |  
| Philippine | Baguio | RPA4|  
| Malaysia | Kuala Lumpur | MY14 |  
| Malaysia | Pulau Pinang | MY09 |  
| Malaysia | Ipoh | MY07 |  
| Indonesia | Jakarta | ID04 |  
| Indonesia | Surabaya | ID08|  
| Indonesia | Bandung | ID30 |  
| Singapore | Singapore | SN |  
